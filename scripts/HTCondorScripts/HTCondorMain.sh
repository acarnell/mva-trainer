#!/bin/bash

cd $MVA_TRAINER_BASE_DIR
echo "=== Executing setup ==="
source setup.sh
if [ $1 == "Converter" ]; then
    echo "=== Executing Converter ==="
    python3 scripts/Converter.py -c $2
fi
if [ $1 == "Trainer" ]; then
    echo "=== Executing Trainer ==="
    python3 scripts/Trainer.py -c $2
fi
if [ $1 == "Evaluater" ]; then
    echo "=== Executing Evaluater ==="
    python3 scripts/Evaluate.py -c $2
fi
if [ $1 == "All" ]; then
    echo "=== Executing Converter ==="
    python3 scripts/Converter.py -c $2
    echo "=== Executing Trainer ==="
    python3 scripts/Trainer.py -c $2
    echo "=== Executing Evaluater ==="
    python3 scripts/Evaluate.py -c $2
fi
echo "=== End ==="

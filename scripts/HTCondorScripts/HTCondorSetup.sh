#!/bin/bash
echo "=== Executing Singularity ==="

singularity exec -H $(pwd) -B /cvmfs -B /afs -B /eos -B /tmp \
  $1 \
  bash $MVA_TRAINER_BASE_DIR/scripts/HTCondorScripts/HTCondorMain.sh $2 $3

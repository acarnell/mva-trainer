from HelperModules.MessageHandler import ErrorMessage, WarningMessage
from HelperModules.HelperFunctions import ReadOption
from HelperModules.OptionChecker import OptionChecker

class Sample:
    """
    Class to describe a MC,data or systematic sample.
    """
    def __init__(self, group):
        self.__Name           = None # Name of the sample (e.g. ttZ)
        self.__Type           = None # Type of the sample ("Signal", "Background", "Systematic" or "Data")
        self.__NtupleFiles    = None # Paths to Ntuples
        self.__TreeName       = None # Treename within ROOT file
        self.__Selection      = ""   # Selection string (ROOT/c++ style)
        self.__MCWeight       = ""   # MCWeight string (ROOT/c++ style)
        self.__TrainLabel     = None # Training label used during training
        self.__PenaltyFactor  = 1.   # Penalty to be applied in the loss function
        self.__FillColor      = None # Fillcolor for histogram plotting
        self.__Group          = None # To declare whether the sample is member of a group
        self.__ScaleToBkg     = False

        #####################################################################################
        ################################ Reading Settings ##################################
        #####################################################################################
        Sample_OS = OptionChecker()
        for item in group:
            Sample_OS.CheckSampleOption(item)
            if "Name" in item and not "TreeName" in item:
                self.__Name = ReadOption(item, "Name", istype="str", islist=False)
            if "Type" in item:
                self.__Type = ReadOption(item, "Type", istype="str", islist=False)
            if "TrainLabel" in item:
                self.__TrainLabel = ReadOption(item, "TrainLabel", istype="int", islist=False)
            if "NtupleFiles" in item:
                self.__NtupleFiles = ReadOption(item, "NtupleFiles", istype="str", islist=True)
            if "TreeName" in item:
                self.__TreeName = ReadOption(item, "TreeName", istype="str", islist=False)
            if "Selection" in item:
                self.__Selection = ReadOption(item, "Selection", istype="str", islist=False)
            if "MCWeight" in item:
                self.__MCWeight = ReadOption(item, "MCWeight", istype="str", islist=False)
            if "PenaltyFactor" in item:
                self.__PenaltyFactor = ReadOption(item, "PenaltyFactor", istype="float", islist=False)
            if "FillColor" in item:
                self.__FillColor = ReadOption(item, "FillColor", istype="int", islist=False, msg="Could not load 'FillColor'. Make sure you pass an integer.")
            if "Group" in item:
                self.__Group = ReadOption(item, "Group", istype="str", islist=False)
            if "ScaleToBkg" in item:
                self.__ScaleToBkg = ReadOption(item, "ScaleToBkg", istype="bool", islist=False)

        #####################################################################################
        ############################### Errors and Warnings #################################
        #####################################################################################
        if self.__Name==None:
            ErrorMessage("Name attribute not set for SAMPLE.")
        if self.__Type==None:
            ErrorMessage("SAMPLE " + self.__Name + " has no 'Type' set.")
        elif self.__Type not in ["Background","Signal","Signal_Scaled","Fake","Systematic", "Data"]:
            ErrorMessage("SAMPLE " + self.__Name + " has unknown type " + self.__Type +". Can be Background, Signal, Fake, Systematic, or Data.")
        if self.__NtupleFiles==None:
            ErrorMessage("SAMPLE " + self.__Name + " has no input files set. You need to define at least one path using the 'NtupleFiles' option.")
        if self.__Type=="Data" and self.__TrainLabel == None:
            self.__TrainLabel = -1
        if self.__Type!="Data" and self.__TrainLabel == None:
            ErrorMessage("SAMPLE " + self.__Name + " has no 'Trainlabel' set. If you only want to plot this sample, then set 'TrainLabel' to -1.")
        if self.__PenaltyFactor < 0:
            ErrorMessage("SAMPLE " + self.__Name + " has negative 'PenaltyFactor' set. This does not make sense.")
        if self.__FillColor==None and not self.__Type=="Data" and not self.__Type=="Systematic":
            ErrorMessage("SAMPLE " + self.__Name + " has no 'FillColor' set.")

    ##############################################################################
    ############################### Misc methods #################################
    ##############################################################################
    def get_Name(self, returnGroupName=False):
        if returnGroupName==True and self.get_Group()!=None:
            return self.get_Group()
        else:
            return self.__Name

    def get_Type(self):
        return self.__Type

    def get_TreeName(self):
        return self.__TreeName

    def get_TrainLabel(self):
        return self.__TrainLabel
    
    def get_Selection(self):
        return self.__Selection

    def get_MCWeight(self):
        return self.__MCWeight

    def get_NtupleFiles(self):
        return self.__NtupleFiles

    def get_PenaltyFactor(self):
        return self.__PenaltyFactor

    def get_FillColor(self):
        return self.__FillColor

    def get_Group(self):
        return self.__Group

    def ScaleToBkg(self):
        return self.__ScaleToBkg
        
    def __str__(self):
        
        SampleString = "\n\t\t".join(self.__NtupleFiles)
        return ("SAMPLEINFO:\tName: "+self.__Name
                +"\nSAMPLEINFO:\tType: "+self.__Type
                +"\nSAMPLEINFO:\tTrainingLabel: "+str(self.__TrainLabel)
                +"\nSAMPLEINFO:\tSelection: "+self.__Selection
                +"\nSAMPLEINFO:\tSamples: "+SampleString)

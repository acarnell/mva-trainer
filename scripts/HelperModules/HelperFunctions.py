"""
Module containing a variety of helperfunctions
that fit nowhere else.
"""

import os, re
import numpy as np
from HelperModules.Transfo import TransfoBinning
from HelperModules.MessageHandler import ErrorMessage, WarningMessage
from ROOT import TH1D, TH2D, THStack, gROOT

def createRatio(h1, h2):
    """Function to determine the ratio between to histograms

    Keyword arguments:
    h1 --- Nominator histogram
    h2 --- Denominator histogram.
    """
    h3 = h1.Clone("h3")
    h3.SetTitle("")
    h3.Divide(h2)
    # Adjust y-axis settings
    y = h3.GetYaxis()
    y.SetTitle("Data/Pred. ")
    y.SetNdivisions(505)
    y.SetTitleSize(20)
    y.SetTitleFont(43)
    y.SetTitleOffset(1.55)
    y.SetLabelFont(43)
    y.SetLabelSize(15)
    # Adjust x-axis settings
    x = h3.GetXaxis()
    x.SetTitleSize(20)
    x.SetTitleFont(43)
    x.SetTitleOffset(4.0)
    x.SetLabelFont(43)
    x.SetLabelSize(15)
    x.SetTitle("DNN Output")
    return h3

def DefineAndFill(Binning, Samples, xvals, xval_identifiers, weights, ClassID=1, SamplesAreClasses = False, binningoptimisation="None"):
    """Function to create unique histograms and fill them.

    Keyword arguments:
    Binning         --- Default binning for all histograms
    xvals           --- List of values to be filled into histograms
    xval_identifier --- List of float/strings/booleans to separate the values into separate catagories
    weights         --- List of floats containing weights. Needs to be same dimension as 'xvals'.
    ClassID         --- Label associated to a requested class. Default is 1 (representing signal)
    binningoptimisation  --- Defines the binning optimisation algorithm
    """
    hists = {}
    unique_identifiers = np.unique(xval_identifiers)
    if SamplesAreClasses:
        colors = dict(zip(unique_identifiers,list(range(41,101,int(59/len(unique_identifiers))))))
        ID_dict = {unique_identifier: {"Type": None,
                                       "TrainingLabel": unique_identifier,
                                       "FillColor": colors[unique_identifier],
                                       "Name": Samples[unique_identifier]} for unique_identifier in unique_identifiers}
    else:
        ID_dict = {Sample.get_Name(returnGroupName=True): {"Type": Sample.get_Type(),
                                                           "TrainingLabel": Sample.get_TrainLabel(),
                                                           "FillColor": Sample.get_FillColor(),
                                                           "Name": Sample.get_Name(),
                                                           "ScaleToBkg": Sample.ScaleToBkg()} for Sample in Samples}
    # Defining the histograms
    for unique_identifier in unique_identifiers:
        if binningoptimisation != "None":
            hist = TH1D("hist_"+str(unique_identifier),"",1000,Binning[0],Binning[-1])
        else:
            hist = TH1D("hist_"+str(unique_identifier),"",int(len(Binning)-1),Binning)
        FillHist(hist,
                 xvals[xval_identifiers==unique_identifier],
                 weights[xval_identifiers==unique_identifier])
        hists["hist_"+str(unique_identifier)]=(hist,ID_dict[unique_identifier])
    if binningoptimisation != "None":
        temp_s_stack = THStack()
        temp_b_stack = THStack()
        for value in hists.values():
            if value[1]["Type"]=="Data":
                continue
            if value[1]["TrainingLabel"]==ClassID:
                temp_s_stack.Add(value[0])
            if value[1]["TrainingLabel"]!=ClassID:
                temp_b_stack.Add(value[0])
        hist_s = temp_s_stack.GetStack().Last().Clone("hist_s")
        hist_b = temp_b_stack.GetStack().Last().Clone("hist_b")
        if binningoptimisation == "TransfoD_symmetric":
            Transfo = TransfoBinning(hist_b=hist_b, hist_s=hist_s, zb=int(len(Binning)/2), zs=int(len(Binning)/2))
        elif binningoptimisation == "TransfoD_flatS":
            Transfo = TransfoBinning(hist_b=hist_b, hist_s=hist_s, zb=int(0), zs=int(len(Binning)))
        elif binningoptimisation == "TransfoD_flatB":
            Transfo = TransfoBinning(hist_b=hist_b, hist_s=hist_s, zb=int(len(Binning)), zs=0)
        optimised_binning = Transfo.TransfoD()
        optimised_hists = {"hist_"+str(k)+"_rebinned": (v[0].Rebin(len(optimised_binning)-1,"hist_"+str(k)+"_rebinned",optimised_binning),v[1]) for k,v in hists.items()}
        for key, hist in hists.items():
            gROOT.FindObject(str(key)).Delete()
        return optimised_hists
    return hists

def DefineAndFill2D(Binning_1, Binning_2, Samples, Predictions_1, Predictions_2, N, W, SamplesAreClasses = False):
    """Function to create unique histograms with nbins and fill it

    Keyword arguments:
    Binning     --- Binning for the histogram
    Predictions --- Predictions to be filled into histograms.
    N           --- List of strings containing sample name. Needs to be same dimension as 'Predictions'.
    W           --- List of floats containing sample weight. Needs to be same dimension as 'Predictions'.
    """
    hists = {}
    if SamplesAreClasses:
        N_T_dict = {n: ("Background", Samples[n]) for n in np.unique(N) if n != -2}
        N_T_dict[-2] = ("Background", "None")
    else:
        N_T_dict = {Sample.get_Name(returnGroupName=True): (Sample.get_Type(), Sample.get_TrainLabel()) for Sample in Samples}
    for n in np.unique(N):
        if len(Binning_1)==2 and len(Binning_2)==2:
            hist = TH2D("hist"+str(n),"",int(Binning_1[0]),Binning_1[1],int(Binning_2[0]),Binning_2[1])
        elif len(Binning_1)==3 and len(Binning_2)==3:
            hist = TH2D("hist"+str(n),"",int(Binning_1[0]),Binning_1[1],Binning_1[2],int(Binning_2[0]),Binning_2[1],Binning_2[2])
        FillHist2D(hist, Predictions_1[N==n],Predictions_2[N==n], W[N==n])
        hists[n]=(hist,N_T_dict[n])
    return hists

def FillHist(hist,values,weights):
    """Helper function to fill a histogram with weighted values

    Keyword arguments:
    hist    --- TH1 object to be filled.
    values  --- Values that are filled into hist.
    weights --- Weights that are used for weighting values.
    """
    for v,w in zip(values,weights):
        hist.Fill(v,w)

def FillHist2D(hist,values_1,values_2,weights):
    """Helper function to fill a histogram with weighted values

    Keyword arguments:
    hist    --- TH1 object to be filled.
    values  --- Values that are filled into hist.
    weights --- Weights that are used for weighting values.
    """
    for v1,v2,w in zip(values_1,values_2,weights):
        hist.Fill(v1,v2,w)

def FillYieldHist(Hist_Yields, yield_dict, total_number_events = 1, color = 2):
    for binID, Class in zip(range(1, len(yield_dict)+1), yield_dict.keys()):
        Hist_Yields.SetBinContent(binID, yield_dict[Class]/total_number_events)
    Hist_Yields.SetFillColor(color)
    Hist_Yields.SetMaximum(Hist_Yields.GetMaximum()*1.3)
    for binID, Class in zip(range(1,Hist_Yields.GetNbinsX()+1),yield_dict.keys()):
        Hist_Yields.GetXaxis().SetBinLabel(binID, Class)

def Separation(hist_a, hist_b):
    """Calculating the separation between two distributions (hist_a, hist_b)

    Keywords arguments:
    hist_a --- First histogram
    hist_b --- Second Histogram
    """
    s = 0
    hist_a_bins = hist_a.GetNbinsX()+1
    hist_b_bins = hist_b.GetNbinsX()+1
    if hist_a_bins != hist_b_bins:
        ErrorMessage("Unequal number of bins. Can not calculate separation power!")
    else:
        for i in range(1,hist_a_bins):
            if hist_a.GetBinContent(i)+hist_b.GetBinContent(i) > 0:
                s+=1/2.*(hist_a.GetBinContent(i)-hist_b.GetBinContent(i))**2/(hist_a.GetBinContent(i)+hist_b.GetBinContent(i))
            if hist_a.GetBinContent(i) < 0:
                WarningMessage("Bin content in bin "+str(i)+" of "+hist_a.GetName()+" is smaller than 0.")
            if hist_b.GetBinContent(i) < 0:
                WarningMessage("Bin content in bin "+str(i)+" of "+hist_b.GetName()+" is smaller than 0.")
    return s

def chunks(lst, n):
    """
    Yield successive n-sized chunks from list.
    """
    c = []
    for i in range(0, len(lst), n):
        c.append(lst[i:i + n])
    return c

def ensure_trailing_slash(path):
    """
    Enusure a given path endes with a '/'.

    Parameters:
    path -- path to be checked

    returns path with '/' at the end.
    """
    if path[-1]=="/":
        return path
    return path+"/"

def delete_trailing_slash(path):
    """
    Delete traling slash of a given path.

    Parameters:
    path -- path to be checked

    returns path without '/' at the end.
    """
    if path[-1]=="/":
        return path[:-1]
    return path

def create_output_dir(path):
    """
    Create an output directory (if it does not exit yet) using the path argument.

    Parameters:
    path -- path to the directory
    """
    if os.path.isdir(delete_trailing_slash(path)):
        return
    os.makedirs(delete_trailing_slash(path))

def clear_config(FileName):
    """
    Clears a config file of blank lines and commented lines (lines starting with#).

    Parameters:
    FileName -- path to the config file

    returns list of lines (list of strings).
    """
    with open(FileName) as f_in:
        lines = (line.rstrip() for line in f_in)
        lines = list(line for line in lines if line) # Non-blank lines
        lines = list(line.replace("\t","") for line in lines) # strip tabs
        lines = list(line.split("#",1)[0] for line in lines) #ignore comments
        # lines = list(line.replace("\\","#") for line in lines) # for LaTeX labels
        return lines

def get_groups(seq):
    """
    Determine whether keywords exist in given sequence of strings and grouping following words together.

    Parameters:
    seq -- sequence of strings

    returns group of strings lead by individual keywords.
    """
    # using list comprehension + zip() + slicing + enumerate()
    # Split list into lists by particular value
    size = len(seq)
    idx_list = [idx for idx, val in enumerate(seq) if val in
                ["GENERAL",
                 "DNNMODEL",
                 "BDTMODEL",
                 "WGANMODEL",
                 "SAMPLE",
                 "VARIABLE"]]
    res = [seq[i: j] for i, j in zip(idx_list, idx_list[1:] + [size])]
    return res

def check_groups(groups, is_Train=False):
    """
    Determine whether obligatory keywords exist in a list of grouped strings.

    Parameters:
    groups -- list of groups
    """
    model_counter = 0
    sample_counter = 0
    variable_counter = 0
    general_counter = 0
    for group in groups:
        if group[0] in ["DNNMODEL","BDTMODEL","WGANMODEL"]:
            model_counter+=1
        if group[0] == "SAMPLE":
            sample_counter+=1
        if group[0] == "VARIABLE":
            variable_counter+=1
        if group[0] == "GENERAL":
            general_counter+=1
    if model_counter == 0:
        ErrorMessage("No suitable model section was found in config file!")
    elif model_counter > 1:
        ErrorMessage("More than one model section was found in config file!")

    if sample_counter == 0:
        ErrorMessage("No 'SAMPLE' section was found in config file!")
    if variable_counter == 0:
        ErrorMessage("No 'VARIABLE' section was found in config file!")
    elif sample_counter == 1 and is_Train:
        WarningMessage("Found only one 'SAMPLE' section in config file! If you do supervised learning a second 'SAMPLE' section is expected.")
    if general_counter == 0:
        ErrorMessage("No 'GENERAL' section was found in config file!")
    elif general_counter != 1:
        ErrorMessage("More than one 'GENERAL' section was found in config file!")

def ReadOption(string, option, istype="str", islist=False, isnestedlist=False, msg=""):
    """
    Read single options from a config file.

    Parameters:
    string --- Individual line (string) from a config file.
    option --- String representing the option against which the input line is compared to.
    istype --- String to determine whether the option expects a 'str', 'int', 'float' or 'bool'.
    islist --- Boolean to determine whether the option expects a list.
    msg --- Custom error message.

    returns chosen option from the config file.
    """
    string = " ".join(string.split()) # Removing blocks of whitespaces
    if msg=="":
        msg="Something went wrong with option "+option+"!"
    if istype=="str" and islist==False and isnestedlist==False :
        String_indece_1 = [_.start() for _ in re.finditer("\"", string)] # strings with "
        String_indece_2 = [_.start() for _ in re.finditer("'", string)] # strings with '
        if len(String_indece_1)==0 and len(String_indece_2)==0:
            return string.replace("\t","").replace(option+" = ","").replace(option+" =","").replace(option+"= ","").replace(option+"=","").replace(" ","")
        elif len(String_indece_1)==2:
            return string[String_indece_1[0]+1:String_indece_1[1]]
        elif len(String_indece_2)==2:
            return string[String_indece_2[0]+1:String_indece_2[1]]
        ErrorMessage("Detected uneven number of string indicators for option "+option+".")
    if istype=="str" and islist==True and isnestedlist==False:
        String_indece_1 = [_.start() for _ in re.finditer("\"", string)] # strings with "
        String_indece_2 = [_.start() for _ in re.finditer("'", string)] # strings with '
        if len(String_indece_1)==0 and len(String_indece_2)==0:
            return string.replace("\t","").replace(option+" = ","").replace(option+" =","").replace(option+"= ","").replace(option+"=","").replace(" ","").split(",")
        elif len(String_indece_1)%2==0:
            return [string[a+1:b] for a,b in chunks(String_indece_1,2)]
        elif len(String_indece_2)%2==0:
            return [string[a+1:b] for a,b in chunks(String_indece_2,2)]
        else:
            ErrorMessage("Detected uneven number of string indicators for option "+option+".")
    if istype=="int" and islist==False and isnestedlist==False:
        try:
            return int(string.replace("\t","").replace(" ","").replace(option+"=",""))
        except Exception as e:
            ErrorMessage(msg + e)
    if istype=="float" and islist==False and isnestedlist==False:
        try:
            return float(string.replace("\t","").replace(" ","").replace(option+"=",""))
        except Exception as e:
            ErrorMessage(msg + e)
    if istype=="bool" and islist==False and isnestedlist==False:
        try:
            s = string.replace("\t","").replace(" ","").replace(option+"=","").lower()
            return s in ["true", "1"]
        except Exception as e:
            ErrorMessage(msg + e)
    if istype=="int" and islist==True and isnestedlist==False:
        try:
            return [int(word) for word in string.replace("\t","").replace(" ","").replace(option+"=","").split(',')]
        except Exception as e:
            ErrorMessage(msg + e)
    if istype=="float" and islist==True and isnestedlist==False:
        try:
            return [float(word) for word in string.replace("\t","").replace(" ","").replace(option+"=","").split(',')]
        except Exception as e:
            ErrorMessage(msg + e)
    if istype=="bool" and islist==True and isnestedlist==False:
        try:
            return [word in ["true", "1"] for word in string.replace("\t","").replace(" ","").replace(option+"=","").split(',')]
        except Exception as e:
            ErrorMessage(msg + e)
    if istype=="str" and islist==True and isnestedlist==True:
        try:
            sub_lists = string.replace("\t","").replace(" ","").replace(option+"=","").split('|')
            ref_lists = [sub_list.split(",") for sub_list in sub_lists]
            return ref_lists
        except Exception as e:
            ErrorMessage(msg + e)
    if istype=="int" and islist==True and isnestedlist==True:
        try:
            sub_lists = string.replace("\t","").replace(" ","").replace(option+"=","").split('|')
            ref_lists = [sub_list.split(",") for sub_list in sub_lists]
            return [[int(word) for word in ref_list if word.isdigit()] for ref_list in ref_lists]
        except Exception as e:
            ErrorMessage(msg + e)
    if istype=="float" and islist==True and isnestedlist==True:
        try:
            sub_lists = string.replace("\t","").replace(" ","").replace(option+"=","").split('|')
            ref_lists = [sub_list.split(",") for sub_list in sub_lists]
            return [[float(word) for word in ref_list if word.isdigit()] for ref_list in ref_lists]
        except Exception as e:
            ErrorMessage(msg + e)
    if istype=="bool" and islist==True and isnestedlist==True:
        try:
            sub_lists = string.replace("\t","").replace(" ","").replace(option+"=","").split('|')
            ref_lists = [sub_list.split(",") for sub_list in sub_lists]
            return [[word in ["true", "1"] for word in ref_list if word.isdigit()] for ref_list in ref_lists]
        except Exception as e:
            ErrorMessage(msg + e)

def equalObs(P, W, nbin):
    """
    Determine whether obligatory keywords exist in a list of grouped strings.

    Parameters:
    P -- List of predictions
    W -- List of weights
    nbin -- Number of bins

    returns list of bin edges for which prediction is flat
    """
    bin_edges = [0]
    mean = sum(W)/nbin
    indece = np.argsort(P)
    W_sorted = W[indece]
    P_sorted = P[indece]
    s = 0
    for i in range(len(indece)):
        s+= W_sorted[i]
        if s>mean:
            bin_edges.append(P_sorted[i])
            s=0
        bin_edges.append(1)
    return bin_edges

def OneHot(Y, classlen):
    """
    Transform a label vector into a one-hot encoded vector

    Parameters:
    Y --- List of labels.

    returns one hot encoded labels.
    """
    unique = list(set(Y))
    minimum, maximum = min(Y),max(Y)
    if [i for i in range(minimum,maximum) if i not in range(classlen)]:
        ErrorMessage("Label array is not ascending.")
    elif 0 not in unique:
        ErrorMessage("Label array does not include 0.")
    else:
        if len(unique) != classlen:
            WarningMessage("{} classes defined but only {} classes used".format(classlen, len(unique)))
        encoded = []
        for y in Y:
            new = [0]*classlen
            new[y] = 1
            encoded.append(new)
        return encoded


def sanitize_root(varlist, fill_value=np.nan):
    #Need to check whether this is still needed when using uproot
    """
    Transforms vector variables into a tuple of variable name and fill value.

    This is required for `root2array`, since vectorized variables otherwise
    would result in a numpy array with `dtype=object`, which lacks certain
    properties required for some numpy functions (e.g. it does not have a
    shape).

    For this, every variable containing `[n]`, where `n` is an integer will be
    transformed into a tuple `(varname, fill_value)`, while other variables are
    left unchanged. Refer to http://scikit-hep.org/root_numpy/reference/generated/root_numpy.root2array.html
    for details.

    Parameters
    ----------
    varlist : list of str
        List of variable names to be retrived by `root2array` later on.
    fill_value : numeric, list of numeric, default = numpy.nan
        Value to fill retrieved vector variables with if they exceed the index
        in the ntuple. If this is a list, `len(varlist) == len(fill_value)`
        is required.

    Returns
    -------
    list of str
        List of sanitized variable names as specified above.
    """

    islist_fill_value = not isinstance(fill_value, (int, float))
    varresults = []

    if islist_fill_value and not len(varlist) == len(fill_value):
        ErrorMessage("sanitize_root: If fill-value is a list, it must have "
                     + "the same length as varlist!")

    for i, var in enumerate(varlist):
        varparts = var.split('[')
        # We need an opening bracket (but might have a vector of vectors)
        if not len(varparts) > 1:
            varresults.append(var)
            continue
        # Check if the last bracket actually closes again exactly once
        varparts = varparts[-1].split(']')
        if not len(varparts) == 2:
            varresults.append(var)
            continue
        # Check if there is only an index between the brackets
        if not varparts[0].isdigit():
            varresults.append(var)
            continue

        # Variable seems to be an array element, let's generate a tuple!
        if islist_fill_value:
            varresults.append((var, fill_value[i]))
        else:
            varresults.append((var, fill_value))

    return varresults

def filter_VarPathName(var):
    """Filters out illegal chars from variable names for filename usage

    For variable Ctrl-plots, some characters are illegal in the filename.
    This function replaces the illegal chars in `__pathCharReplDict` by
    the strings specified there and removes all other chars not allowed
    in `__char_isAllowed`.

    Parameters
    ----------
    var : string
        Variable name to be filtered by the rules outlined above.

    Returns
    -------
    string
        Filtered variable name suitable for filenames.
    """
    pathCharReplDict = {
        "/"        : "_DIV_",
        "*"        : "_TIMES_"
    }
    varpath = var
    # Replacing chars by their replacement for readability
    for char, repl in pathCharReplDict.items():
        varpath = varpath.replace(char, repl)

    # Removing other special characters
    varpath = ''.join([c for c in varpath if char_isAllowed(c)])

    # TODO: Enable InfoMessage again once verbosity option added
    # if not var == varpath:
    #     InfoMessage("Filename '{:s}' used for variable '{:s}'".format(
    #             varpath, var))

    return varpath

def char_isAllowed(c):
    '''Returns True if char c is allowed, False otherwise'''
    return c.isalnum() or c in " _-.,[]()"

def matched(string):
        """
        Method to check whether parenthesis are matched in a string.
        """
        count = 0
        for i in string:
            if i == "(":
                count += 1
            elif i == ")":
                count -= 1
            if count < 0:
                return False
        return count == 0

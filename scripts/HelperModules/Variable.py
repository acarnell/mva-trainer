from HelperModules.MessageHandler import ErrorMessage, WarningMessage
from HelperModules.HelperFunctions import ReadOption
from HelperModules.OptionChecker import OptionChecker
from array import array
import numpy as np

class Variable:
    """
    Class to describe an input variable.
    """
    def __init__(self,group=None, name=None, label=None, binning=None, custombinning=None):
        self.__Name          = name # Name of the variable (e.g. pT_1jet)
        self.__Label         = label # Label of the variable (e.g. Leading jet p_{T})
        self.__Binning       = binning # Binning for the variable
        self.__CustomBinning = custombinning # Custom binning for the variable
        self.__VarBinning    = None

        if group != None: # this is the case where we read from config file, otherwise we initialise using init function
            ####################################################################################
            ################################ Reading Settings ##################################
            ####################################################################################
            Variable_OS = OptionChecker()
            for item in group:
                Variable_OS.CheckVariableOption(item)
                if "Name" in item:
                    self.__Name = ReadOption(item, "Name", istype="str", islist=False)
                if "Label" in item:
                    self.__Label = ReadOption(item, "Label", istype="str", islist=False)
                if "Binning" in item and not "CustomBinning" in item:
                    self.__Binning = ReadOption(item, "Binning", istype="float", islist=True)
                if "CustomBinning" in item:
                    self.__CustomBinning = ReadOption(item, "CustomBinning", istype="float", islist=True)

                #####################################################################################
                ############################### Errors and Warnings #################################
                #####################################################################################
            if self.__Name == None:
                ErrorMessage("Name attribute not set for VARIABLE.")
            if self.__Label == None:
                ErrorMessage("Label attribute not set for VARIABLE %s."%self.__Name)
            if self.__Binning == None and self.__CustomBinning == None:
                ErrorMessage("VARIABLE %s has no 'Binning' or 'CustomBinning' defined! Please pass either a valid 'Binning' or 'CustomBinning'."%self.__Name)
            if len(self.__Binning)!=3:
                ErrorMessage("VARIABLE %s has invalid 'Binning' set. You need to pass a binning in this Format: nbins,xlow,xhigh."%self.__Name)
            if self.__CustomBinning != None and len(self.__CustomBinning) < 2:
                ErrorMessage("VARIABLE %s has invalid 'CustomBinning' set. You need to pass at least to values!."%self.__Name)

        # Get actual binning in array like format
        if self.__CustomBinning != None:
            self.__VarBinning = array("d",self.__CustomBinning)
        else:
            self.__VarBinning = array("d",np.linspace(start=self.__Binning[1],
                                                      stop=self.__Binning[2],
                                                      num=int(self.__Binning[0]+1)))

    def get_Name(self):
        return self.__Name

    def get_Label(self, LaTeXsafe=True):
        if LaTeXsafe:
            return self.__Label.replace("/","#")
        return self.__Label

    def get_VarBinning(self):
        return self.__VarBinning

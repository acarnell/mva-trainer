from HelperModules.MessageHandler import ErrorMessage

class OptionChecker:
    # This class serves as a helper class to check whether passed options are valid
    def __init__(self):
        # Allowed options for the GENERAL section
        self.__AllowedGeneralSettings = ["GENERAL", # this is read first so we also have to include it
                                         "Job",
                                         "Selection",
                                         "MCWeight",
                                         "Treename",
                                         "InputScaling",
                                         "RecalcScaler",
                                         "OutputScaling",
                                         "WeightScaling",
                                         "TreatNegWeights",
                                         "Folds",
                                         "TrainMethod",
                                         "ATLASLabel",
                                         "CMLabel",
                                         "CustomLabel",
                                         "DoCtrlPlots",
                                         "DoYields",
                                         "PlotFormat",
                                         "StackPlotScale",
                                         "SeparationPlotScale",
                                         "ConfusionPlotScale",
                                         "KSPlotScale",
                                         "LargeCorrelation",
                                         "RatioMax",
                                         "RatioMin",
                                         "Blinding"]
        # Allowed options for the SAMPLE section
        self.__AllowedSampleSettings = ["SAMPLE", # this is read first so we also have to include it
                                        "Name",
                                        "Type",
                                        "TrainLabel",
                                        "NtupleFiles",
                                        "Treename",
                                        "Selection",
                                        "MCWeight",
                                        "PenaltyFactor",
                                        "FillColor",
                                        "Group",
                                        "ScaleToBkg"]
         # Allowed options for the VARIABLE section
        self.__AllowedVariableSettings = ["VARIABLE", # this is read first so we also have to include it
                                        "Name",
                                        "Label",
                                        "Binning",
                                        "CustomBinning"]
        # Allowed basic options for all models
        self.__AllowedBasicModelSettings = ["DNNMODEL", # this is read first so we also have to include it
                                            "BDTMODEL"  # this is read first so we also have to include it
                                            "WGANMODEL",# this is read first so we also have to include it
                                            "Name",
                                            "Type",
                                            "Epochs",
                                            "BatchSize",
                                            "ValidationSize",
                                            "Metrics",
                                            "ClassLabels",
                                            "ModelBinning",
                                            "BinningOptimisation",
                                            "ClassColors"]
        # Allowed specific options for DNN models
        self.__AllowedDNNModelSettings = ["DNNMODEL", # this is read first so we also have to include it
                                          "Nodes",
                                          "MaxNodes",
                                          "MinNodes",
                                          "StepNodes",
                                          "MaxLayers",
                                          "MinLayers",
                                          "PredesignedModel",
                                          "LearningRate",
                                          "Patience",
                                          "MinDelta",
                                          "DropoutIndece",
                                          "DropoutProb",
                                          "BatchNormIndece",
                                          "ActivationFunctions",
                                          "SetActivationFunctions",
                                          "KernelInitialiser",
                                          "KernelRegulariser",
                                          "KernelRegulariserl1",
                                          "KernelRegulariserl2",
                                          "BiasRegulariser",
                                          "BiasRegulariserl1",
                                          "BiasRegulariserl2",
                                          "ActivityRegulariser",
                                          "ActivityRegulariserl1",
                                          "ActivityRegulariserl2",
                                          "OutputSize",
                                          "OutputActivation",
                                          "Loss",
                                          "Optimiser",
                                          "SmoothingAlpha",
                                          "UseTensorBoard",
                                          "RunONNXConversion",
                                          "RegressionTarget",
                                          "RegressionTargetLabel",
                                          "RegressionScale",
                                          "ReweightTargetDist",
                                          "HuberDelta",
                                          "Verbosity"]
        # Allowed specific options for BDT models
        self.__AllowedBDTModelSettings = ["BDTMODEL", # this is read first so we also have to include it
                                          "nEstimators",
                                          "MaxDepth",
                                          "LearningRate",
                                          "Patience",
                                          "MinDelta",
                                          "Criterion",
                                          "Subsample", 
                                          "Loss",
                                          "SmoothingAlpha",
                                          "Verbosity",
                                          "RegressionTarget",
                                          "RegressionTargetLabel"]
        
    ##############################################################################
    ############################# Checking methods ###############################
    ##############################################################################
    def CheckSampleOption(self,Option):
        if not any([s in Option for s in self.__AllowedSampleSettings]) and not Option.isspace() and not Option=="":
            ErrorMessage(Option+" is not an allowed SAMPLE option!")

    def CheckVariableOption(self,Option):
        if not any([s in Option for s in self.__AllowedVariableSettings]) and not Option.isspace() and not Option=="":
            ErrorMessage(Option+" is not an allowed VARIABLE option!")

    def CheckGeneralOption(self,Option):
        if not any([s in Option for s in self.__AllowedGeneralSettings]) and not Option.isspace() and not Option=="":
            ErrorMessage(Option+" is not an allowed GENERAL option!")

    def CheckBasicModelOption(self,Option):
        if not(any([s in Option for s in self.__AllowedDNNModelSettings]) or
               any([s in Option for s in self.__AllowedBDTModelSettings])):
            if not any([s in Option for s in self.__AllowedBasicModelSettings]) and not Option.isspace() and not Option=="":
                ErrorMessage(Option+" is not an allowed option for a model!")

    def CheckDNNModelOption(self,Option):
        if not(any([s in Option for s in self.__AllowedBasicModelSettings])):
            if not any([s in Option for s in self.__AllowedDNNModelSettings]) and not Option.isspace() and not Option=="":
               ErrorMessage(Option+" is not an allowed option for a DNN classification model!")

    def CheckBDTModelOption(self,Option):
        if not(any([s in Option for s in self.__AllowedBasicModelSettings])):
               if not any([s in Option for s in self.__AllowedBDTModelSettings]) and not Option.isspace() and not Option=="":
                   ErrorMessage(Option+" is not an allowed option for a BDT classification model!")

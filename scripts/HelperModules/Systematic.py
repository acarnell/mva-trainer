from HelperModules.MessageHandler import ErrorMessage, WarningMessage
from HelperModules.HelperFunctions import ReadOption

class Systematic:
    """
    Class to describe a MC Systematic.
    """
    def __init__(self, group):
        self.__Name           = None  # Name of the systematic (e.g. JES_up)
        self.__NtupleFiles    = None  # Paths to Ntuples
        self.__TreeName       = None  # Treename within ROOT file
        self.__Selection      = ""    # Selection string (ROOT/c++ style)
        self.__Samples        = "All" # Samples this systematic is applied on
        self.__Exclude        = None  # Samples this systematic is explicitly not applied on
        self.__PenaltyFactor  = 1.    # Penalty to be applied in the loss function

        #####################################################################################
        ################################ Reading Settings ##################################
        #####################################################################################
        for item in group:
            if "Name" in item and not "TreeName" in item:
                self.__Name = ReadOption(item, "Name", istype="str", islist=False)
            if "NtupleFiles" in item:
                self.__NtupleFiles = ReadOption(item, "NtupleFiles", istype="str", islist=True)
            if "TreeName" in item:
                self.__TreeName = ReadOption(item, "TreeName", istype="str", islist=False)
            if "Selection" in item:
                self.__Selection = ReadOption(item, "Selection", istype="str", islist=False)
            if "Samples" in item:
                self.__Samples = ReadOption(item, "Samples", istype="str", islist=True)
            if "Exclude" in item:
                self.__Exclude = ReadOption(item, "Exclude", istype="str", islist=True)
            if "PenaltyFactor" in item:
                self.__PenaltyFactor = ReadOption(item, "PenaltyFactor", istype="float", islist=False)

        #####################################################################################
        ############################### Errors and Warnings #################################
        #####################################################################################
        if self.__Name==None:
            ErrorMessage("Name attribute not set for SYSTEMATIC.")
        if self.__NtupleFiles==None and self.__TreeName==None:
            ErrorMessage("SAMPLE " + self.__Name + " has no input files and no tree name set. You need to define at least one path using the 'NtupleFiles' option or a tree name using the 'TreeName' option.")
        if self.__PenaltyFactor < 0:
            ErrorMessage("SAMPLE " + self.__Name + " has negative 'PenaltyFactor' set. This does not make sense.")
        if self.__FillColor==None and not self.__Type=="Data":
            ErrorMessage("SAMPLE " + self.__Name + " has no 'FillColor' set.")

    ##############################################################################
    ############################### Misc methods #################################
    ##############################################################################
    def get_Name(self):
        return self.__Name

    def get_TreeName(self):
        return self.__TreeName

    def get_Selection(self):
        return self.__Selection

    def get_NtupleFiles(self):
        return self.__NtupleFiles

    def get_Samples(self):
        return self.__Samples

    def get_ExcludedSamples(self):
        return self.__Exclude

    def get_PenaltyFactor(self):
        return self.__PenaltyFactor

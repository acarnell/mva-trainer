"""
Module handling the injection of predictions
back into root ntuples.
"""

import collections, os, uproot, ROOT
import tensorflow as tf
import pandas as pd
import HelperModules.HelperFunctions as hf
from functools import reduce
from pickle import load
from array import array
from HelperModules.Settings import Settings
from HelperModules.MessageHandler import ErrorMessage

class Injector:
    """
    Class handling the injection of ntuples into root files.
    Takes args from argparse as parameter.
    """
    def __init__(self, args):
        self.__InputPath    = args.input_path
        self.__OutputPath   = args.output_path
        self.__ConfigPaths  = args.configfile
        self.__ModelPaths   = args.model_path
        self.__TreeWildcard = args.tree_wildcard
        self.__CfgSettings  = [Settings(ConfigPath, option="all", is_Train=False) for ConfigPath in self.__ConfigPaths]
        self.__Models       = [s.get_Model() for s in self.__CfgSettings]

        # First we check whether all models have a unique name:
        ModelNames  = [Model.get_Name() for Model in self.__Models]
        if len(ModelNames)!=len(set(ModelNames)):
            Duplicates = [item for item, count in collections.Counter(ModelNames).items() if count > 1]
            ErrorMessage("You are using models that have identical `Model Names`. The following names occur multiple times %s."%Duplicates)
        # If all models have a unique name we define a dictionary holding parameters
        self.__InjectionDict = {ModelName: {} for ModelName in ModelNames}
        for S,ModelPath in zip(self.__CfgSettings,self.__ModelPaths):
            Model  = S.get_Model()
            nFolds = S.get_General().get_Folds()
            ModelInputs = S.get_VarNames().copy()
            ModelInputs += ["eventNumber"]
            if nFolds!=1:
                Folds = ["_Fold"+str(i) for i in range(nFolds)]
            else:
                Folds = [""]
            # Getting the paths of the models and the objects themselves
            if Model.isClassificationBDT():
                self.__InjectionDict[Model.get_Name()]["ModelPaths"]=[hf.ensure_trailing_slash(ModelPath)+Model.get_Name()+"_"+Model.get_Type()+Fold+".pkl" for Fold in Folds]
                ModelObjects = []
                for path in self.__InjectionDict[Model.get_Name()]["ModelPaths"]:
                    with open(path, 'rb') as f:
                        ModelObjects.append(load(f))
                self.__InjectionDict[Model.get_Name()]["ModelObjects"]=ModelObjects
            elif Model.isClassificationDNN() or Model.isReconstruction() or Model.isRegression():
                self.__InjectionDict[Model.get_Name()]["ModelPaths"] = [hf.ensure_trailing_slash(ModelPath)+Model.get_Name()+"_"+Model.get_Type()+Fold+".h5" for Fold in Folds]
                ModelObjects = [tf.keras.models.load_model(path) for path in self.__InjectionDict[Model.get_Name()]["ModelPaths"]]
                self.__InjectionDict[Model.get_Name()]["ModelObjects"]=ModelObjects
            # Getting additional parameters
            self.__InjectionDict[Model.get_Name()]["Scaler"] = load(open(hf.ensure_trailing_slash(ModelPath)+"scaler.pkl", 'rb'))
            self.__InjectionDict[Model.get_Name()]["ClassLabels"] = [classlabel.replace(" ","") for classlabel in Model.get_ClassLabels()]
            self.__InjectionDict[Model.get_Name()]["ModelInputs"] = ModelInputs
            self.__InjectionDict[Model.get_Name()]["ModelFolds"] = nFolds
            self.__InjectionDict[Model.get_Name()]["Model"] = Model

    def do_PreCalculation(self,fname,treename):
        """
        Function performing the pre-calculation, i.e. calculating
        the predictions given by the model
        keyword arguments:
        fname --- file name (string)
        treename --- treename (string)
        """
        DF_List = []
        for ParameterDict in self.__InjectionDict.values():
            FoldDataFrames = []
            nFolds = ParameterDict["ModelFolds"]
            Model  = ParameterDict["Model"]
            ModelObjects = ParameterDict["ModelObjects"]
            scaler = ParameterDict["Scaler"]
            Events = uproot.open(fname+":"+treename)
            df_X = Events.arrays(ParameterDict["ModelInputs"],library="pd")
            df_X_Folds = [df_X[df_X["eventNumber"]%nFolds==i] for i in range(nFolds)]
            # Let's reorder the event numbers and concatenate them again. We need this for the keys of our prediction dictionary
            EventNumbers = [df_X[df_X["eventNumber"]%nFolds==i]["eventNumber"].values for i in range(nFolds)]
            df_X_Folds = [X.drop(["eventNumber"], axis=1) for X in df_X_Folds]
            # We need to make sure we actually have values to pass to the scaler. Otherwise this will crash
            X_Scaled = [scaler.transform(X.values) if (len(X.values)!=0) else [] for X in df_X_Folds]
            ################################### BDTs ###################################
            if Model.isClassificationBDT():
                for i,X,E in zip(range(nFolds),X_Scaled,EventNumbers):
                    if len(X)==0:
                        continue
                    if len(ParameterDict["ClassLabels"])==1: # binary scikit-learn case returns [1-P,P]
                        columns = [Model.get_Name()]
                        tmp_df = pd.DataFrame(data=ModelObjects[i].predict_proba(X)[:,1],columns=columns)
                    else:
                        columns = [Model.get_Name()+"_"+cl for cl in ParameterDict["ClassLabels"]]
                        tmp_df = pd.DataFrame(data=ModelObjects[i].predict_proba(X),columns=columns)
                    tmp_df["eventNumber"] = E
                    FoldDataFrames.append(tmp_df)
            ################################### DNNs ###################################
            elif Model.isClassificationDNN() or Model.isReconstruction() or Model.isRegressionDNN():
                for i,X,E in zip(range(nFolds),X_Scaled,EventNumbers):
                    if len(X)==0:
                        continue
                    if len(ParameterDict["ClassLabels"])==1:
                        columns = [Model.get_Name()]
                    else:
                        columns = [Model.get_Name()+"_"+cl for cl in ParameterDict["ClassLabels"]]
                    tmp_df = pd.DataFrame(data=ModelObjects[i].predict(X), columns=columns)
                    tmp_df["eventNumber"] = E
                    FoldDataFrames.append(tmp_df)
                DF_List.append(pd.concat(FoldDataFrames))
            DF_Merged = reduce(lambda  left,right: pd.merge(left,right,on=['eventNumber'],how='outer'), DF_List)
        return DF_Merged

    def do_Injection(self,fname):
        """
        Function to perform the injection of ntuples into the actual root file.

        Keyordword arguments:
        fname --- file name (string)
        """
        # Create a new root file
        File = ROOT.TFile(fname)
        NewFile = ROOT.TFile(fname.replace(os.path.abspath(self.__InputPath), os.path.abspath(self.__OutputPath)),"recreate")
        # Loop over trees in root file
        keys = File.GetListOfKeys()
        Treenames = []
        for key in keys:
            if key.GetClassName() != "TTree": # ignore TObjects that are not TTrees
                continue
            if key.ReadObj().GetName() == "": # ignore TTrees that do not have a name
                continue
            Treenames.append(key.ReadObj().GetName())
        for tName in Treenames:
            Tree = File.Get(tName)
            if tName not in self.__TreeWildcard and self.__TreeWildcard != "":
                continue
             # black listing a few trees which are simply copied
            if tName in ["", "truth", "particleLevel", "PDFsumWeights", "AnalysisTracking", "AnalysisTrackingTree"]:
                NewTree = Tree.CloneTree(-1, "fast")
                NewFile.Write()
                continue
            NewTree = Tree.CloneTree(0)
            NewBranches = {}
            for ModelName, ParameterDict in self.__InjectionDict.items():
                # 1st case, we only have one output from our Model
                if len(ParameterDict["ClassLabels"]) == 1:
                    NewBranches[ModelName] = array('f', [0])
                # 2nd case, we have multiple outputs from our Model
                else:
                    # We add separate branches for each output
                    for Label in ParameterDict["ClassLabels"]:
                        NewBranches[ModelName+"_"+Label] = array('f', [0])
            for BranchName, NewBranch in NewBranches.items():
                NewTree.Branch(BranchName, NewBranch, BranchName+"/F")
            if Tree.GetEntries() == 0:
                NewFile.Write()
                continue
            DF = self.do_PreCalculation(fname=fname,treename=tName)
            eventNumbers = DF["eventNumber"].values
            ModelNames = [key for key in DF.keys() if key!="eventNumber"]
            tmp_dicts = DF[ModelNames].to_dict("records")
            DF_dict = dict(zip(eventNumbers,tmp_dicts))
            BranchNames = [N for N in DF.keys() if N!="eventNumber"]
            for entry_number in range(0,Tree.GetEntries()):
                Tree.GetEntry(entry_number)
                for Name in BranchNames:
                    NewBranches[Name][0] = DF_dict[Tree.eventNumber][Name]
                NewTree.Fill()
            NewFile.Write()
        NewFile.Close()

"""
Module for a BDT model
"""
import numpy as np
import HelperModules.HelperFunctions as hf
from HelperModules.MessageHandler import ErrorMessage, WarningMessage, TrainerMessage, TrainerDoneMessage
from HelperModules.BasicModel import BasicModel
from HelperModules.OptionChecker import OptionChecker
from sklearn.ensemble import GradientBoostingClassifier, GradientBoostingRegressor
from xgboost import XGBClassifier
from pickle import dump

class BDTModel(BasicModel):
    """
    Class for implementing boosted decision trees.
    """
    def __init__(self, group, input_dim, is_Train=True):
        super().__init__(group)
        self.__InputDim              = input_dim # Input dimensionality of the data
        self.__scikitmodel           = None      # Keras model object
        self.__PredesignedModel      = None      # For usage of pre-designed models
        self.__nEstimators           = 100       # Number of boosting stages to perform
        self.__MaxDepth              = 3         # Maximum depth of the individual estimators
        self.__LearningRate          = 0.1       # Learning rate
        self.__Patience              = None      # Number of epochs in ES to wait for improvement
        self.__MinDelta              = None      # Minimum improvement for ES
        self.__Subsample             = None      # sample used in boosted phase
        self.__Criterion             = None      # Function to measure the quality of a split
        self.__Loss                  = None      # Loss function to be used
        self.__SmoothingAlpha        = None      # Parameter to be used for label smoothing
        self.__RegressionTarget      = None      # Name of truth variable for regression
        self.__RegressionTargetLabel = None      # Label of the variable for regression for plots
        self.__Verbosity             = 1         # Verbosity level for scikit-learn (Default 1)

        self.__AllowedBDTTypes     = ["Classification-BDT",
                                      "Regression-BDT",
                                      "Classification-Xg-BDT"]

        #######################################################################
        ############################ Input Options ############################
        #######################################################################
        BDTModel_OS = OptionChecker()
        for item in self._BasicModel__Group:
            BDTModel_OS.CheckBDTModelOption(item)
            if "nEstimators" in item:
                self.__nEstimators = hf.ReadOption(item, "nEstimators", istype="int", islist=False, msg="Could not load 'nEstimators'. Make sure you pass an integer greater than 0.")
            if "MaxDepth" in item:
                self.__MaxDepth = hf.ReadOption(item, "MaxDepth", istype="int", islist=False, msg="Could not load 'MaxDepth'. Make sure you pass an integer greater than 0.")
            if "LearningRate" in item:
                self.__LearningRate = hf.ReadOption(item, "LearningRate", istype="float", islist=False, msg="Could not load 'LearningRate'. Make sure you pass a float.")
            if "Patience" in item:
                self.__Patience = hf.ReadOption(item, "Patience", istype="int", islist=False, msg="Could not load 'Patience'. Make sure you pass an integer.")
            if "MinDelta" in item:
                self.__MinDelta = hf.ReadOption(item, "MinDelta", istype="float", islist=False, msg="Could not load 'MinDelta'. Make sure you pass a float.")
            if "Criterion" in item:
                self.__Criterion = hf.ReadOption(item, "Criterion", istype="str", islist=False)
            if "Subsample" in item:
                self.__Subsample = hf.ReadOption(item, "Subsample", istype="float", islist=False)
            if "Loss" in item:
                self.__Loss = hf.ReadOption(item, "Loss", istype="str", islist=False)
            if "SmoothingAlpha" in item:
                self.__SmoothingAlpha = hf.ReadOption(item, "SmoothingAlpha", istype="float", islist=False)
            if "RegressionTarget" in item and not "RegressionTargetLabel" in item:
                self.__RegressionTarget = hf.ReadOption(item, "RegressionTarget", istype="str", islist=False)
            if "RegressionTargetLabel" in item:
                self.__RegressionTargetLabel = hf.ReadOption(item, "RegressionTargetLabel", istype="str", islist=False)
            if "Verbosity" in item:
                self.__Verbosity = hf.ReadOption(item, "Verbosity", istype="int", islist=False)
            if self.__Verbosity not in [0,1,2]:
                ErrorMessage("MODEL " + self._BasicModel__Name + " has invalid 'Verbosity' set! Possible options are 0 (silent), 1 (prints progress and performance once in a while) and 2 (prints progress and performance for every tree).")

        #############################################################################
        ############################ Check Input Options ############################
        #############################################################################
        if self._BasicModel__Type not in self.__AllowedBDTTypes:
            ErrorMessage("MODEL " + self._BasicModel__Name + " has invalid type set for BDTMODEL!")
        if self.__MinDelta is not None:
            if self.__MinDelta>=1:
                ErrorMessage("MODEL " + self._BasicModel__Name + " has 'MinDelta' set to "+str(self.__MinDelta)+". 'MinDelta' must be <1.")
        if self.__Loss is None:
            ErrorMessage("MODEL " + self._BasicModel__Name + " has no 'Loss' set.")
        else:
            if self.isClassificationBDT():
                if self.__Loss not in ["deviance", "exponential"]:
                    ErrorMessage("MODEL " + self._BasicModel__Name + " has unknown 'Loss' set: "+self.__Loss)
            if self.isClassificationXgBDT():
                if self.__Loss not in ["rmse", "rmsle","mae", "mape","logloss"]:
                    ErrorMessage("MODEL " + self._BasicModel__Name + " has unknown 'Loss' set: "+self.__Loss)
            if self.isRegressionBDT():
                if self.__Loss not in ["square_error", "ls","absoulte_error","lad","huber","quantile"]:
                    ErrorMessage("MODEL " + self._BasicModel__Name + " has unknown 'Loss' set: "+self.__Loss)
        if self.__Subsample is None:
            self.__Subsample = 1.0
        if self.__Criterion is None: 
            if self.isClassificationBDT() or self.isRegressionBDT():
                self.__Criterion = "friedman_mse"
                if is_Train:
                    WarningMessage("MODEL " + self._BasicModel__Name + " has no 'Criterion' set. Using 'friedman_mse' now.")    
        if self.isClassificationBDT() or self.isRegressionBDT():
            if self.__Criterion not in ["friedman_mse", "mse"]:
                ErrorMessage("MODEL " + self._BasicModel__Name + " has unsupported'Criterion' set. Only 'friedman_mse' and 'mse' are supported.")
        if self._BasicModel__ClassLabels is None:
            self._BasicModel__ClassLabels = ["Binary"]
        if self.__SmoothingAlpha is not None and (self.__SmoothingAlpha > 0.5 or self.__SmoothingAlpha<0):
            ErrorMessage("MODEL " + self._BasicModel__Name + " has invalid SmoothingAlpha value! Must be between >0 and <0.5!")
        if not self.isRegressionBDT() and self.__RegressionTarget is not None:
            ErrorMessage("MODEL " + self._BasicModel__Name + " has 'RegressionTarget' set! This is only allowed for NN with type 'Regression-BDT'!")
        if self.isRegressionBDT() and self.__RegressionTarget is None:
            ErrorMessage("MODEL " + self._BasicModel__Name + " has type 'Regression-BDT' set but no 'RegressionTarget' was defined.")
        if not self.isRegressionBDT() and self.__RegressionTargetLabel is not None:
            ErrorMessage("MODEL " + self._BasicModel__Name + " has 'RegressionTargetLabel' set! This is only allowed for NN with type 'Regression-BDT'!")
        if self.isRegressionBDT() and self.__RegressionTargetLabel is None:
            WarningMessage("MODEL " + self._BasicModel__Name + " has type 'Regression-BDT' set but no 'RegressionTargetLabel' was defined. Using 'RegressionTarget' for the label instead.")
            self.__RegressionTargetLabel = self.__RegressionTarget

    def BDTCompile(self):
        """
        Compile a simple BDT model
        """
        if self.isRegressionBDT():
            self.__scikitmodel = GradientBoostingRegressor(n_estimators = self.__nEstimators,
                                                           learning_rate = self.__LearningRate,
                                                           loss = self.__Loss,
                                                           criterion  = self.__Criterion,
                                                           max_depth = self.__MaxDepth,
                                                           n_iter_no_change = self.__Patience,
                                                           validation_fraction = self._BasicModel__ValidationSize,
                                                           tol = self.__MinDelta,
                                                           verbose = self.__Verbosity)
        if self.isClassificationBDT():
            self.__scikitmodel = GradientBoostingClassifier(n_estimators = self.__nEstimators,
                                                            learning_rate = self.__LearningRate,
                                                            loss = self.__Loss,
                                                            subsample = self.__Subsample,
                                                            criterion  = self.__Criterion,
                                                            max_depth = self.__MaxDepth,
                                                            n_iter_no_change = self.__Patience,
                                                            validation_fraction = self._BasicModel__ValidationSize,
                                                            tol = self.__MinDelta,
                                                            verbose = self.__Verbosity)
        if self.isClassificationXgBDT():
                        self.__scikitmodel = XGBClassifier( n_estimators= self.__nEstimators, 
                                                            learning_rate= self.__LearningRate,
                                                            objective= 'binary:logistic',
                                                            subsample= self.__Subsample,
                                                            tree_method = 'hist',
                                                            max_depth= self.__MaxDepth, 
                                                            colsample_bytree= 0.4, 
                                                            early_stopping_rounds = self.__Patience, #Not used but...
                                                            eval_metric=self.__Loss,
                                                            verbosity= self.__Verbosity,
                                                            use_label_encoder=False) #Avoid annoying warnings

    def CompileAndTrain(self, X, Y, W, output_path, X_test=None, Y_test=None, W_test=None, Fold=""):
        """
        Training a simple GradientBoosting classifier.

        Keyword Arguments:
        X           --- Array of data to be used for training
        Y           --- Labels
        W           --- Weights
        output_path --- Output path where the model .joblib files and the history is to be saved
        Fold        --- String declaring the fold name. This will be appended to the modelname upon saving it.
        """
        if Fold!="":
            Fold = "_Fold"+Fold
        if self.__SmoothingAlpha is not None:
            Y=self.smooth_labels(Y, K=len(np.unique(Y)), alpha=self.__SmoothingAlpha)
            Y_test=self.smooth_labels(Y_test, K=len(np.unique(Y)), alpha=self.__SmoothingAlpha)
        self.BDTCompile()
        TrainerMessage("Starting Training...")
        if self.isClassificationXgBDT(): 
            if(X_test != None)  and (Y_test != None) and (W_test != None):
                self.__scikitmodel.fit(X, Y, sample_weight=W, eval_set=[(X_test, Y_test)],sample_weight_eval_set=[W_test])
            else:
                self.__scikitmodel.fit(X, Y, sample_weight=W, eval_set=[(X, Y)],sample_weight_eval_set=[W])
        else:
            self.__scikitmodel.fit(X, Y, sample_weight=W)    
        TrainerDoneMessage("Training done! Writing model to "+hf.ensure_trailing_slash(output_path)+self._BasicModel__Name+"_"+self._BasicModel__Type+Fold+".pkl")
        with open(hf.ensure_trailing_slash(output_path)+self._BasicModel__Name+"_"+self._BasicModel__Type+Fold+".pkl", 'wb') as f:
            dump(self.__scikitmodel, f)

    def get_RegressionTarget(self):
        """
        Getter method to return the name of the variable
        in the ntuples used as the regression target
        """
        return self.__RegressionTarget

    def get_RegressionTargetLabel(self):
        """
        Getter method to return the label of the variable
        in the ntuples used as the regression target
        """
        return self.__RegressionTargetLabel

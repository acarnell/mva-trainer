"""
This module hosts custom functions to be used in keras models.
"""

from tensorflow.keras.backend import sigmoid
from tensorflow.keras.utils import get_custom_objects

def swish(x, beta=1):
    """
    See https://arxiv.org/abs/1710.05941v1 for more information
    """
    return x * sigmoid(beta * x)

get_custom_objects().update({'swish': swish})

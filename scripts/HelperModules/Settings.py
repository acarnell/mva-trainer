from HelperModules.MessageHandler import ErrorMessage, InfoDelimiter
from HelperModules.DNNModel import DNNModel
from HelperModules.BDTModel import BDTModel
from HelperModules.WGANModel import WGANModel
from HelperModules.Sample import Sample
from HelperModules.Variable import Variable
from HelperModules.SystTrees import SystTrees
from HelperModules.HelperFunctions import clear_config, get_groups, check_groups, ReadOption
from HelperModules.OptionChecker import OptionChecker
from numpy import unique

class Settings:
    """ This class serves as a storage class for all settings that are read from the config file.

    Instance variables:
    option   --- Option to determine which settings are read and evaluated. Possible are 'all', 'samples' or 'model'.
    FileName --- File name (string) of the config file.
    model    --- String describing the type of model that shall be used later in the code.
    samples  --- List of samples (list of strings) that are picked up by the code and passed to the converter.
    general  --- Additional general settings which are defined in the General_Settings class.
    """
    def __init__(self, FileName, option="all", is_Train=True):
        self.__option   = option.lower()
        self.__FileName = FileName
        self.__groups   = get_groups(clear_config(FileName))
        check_groups(self.__groups, is_Train)
        self.__model   = None
        self.__samples = []
        self.__variables = []
        self.__general = None
        
        #####################################################################################
        ################################ Reading Settings ###################################
        #####################################################################################
        # Iterating through groups of settings (i.e. GENERAL, DNNMODEL/WGANMODEL and SAMPLE)
        for group in self.__groups:
            # Detection of the GENERAL block
            if group[0]=="GENERAL" and (self.__option == "all" or self.__option == "samples"):
                self.__general = General_Settings(group)
            # Detection of each SAMPLE block
            if group[0]=="SAMPLE" and (self.__option == "all" or self.__option == "samples"):
                self.__samples.append(Sample(group))
            # Detection of each VARIABLE block
            if group[0]=="VARIABLE" and (self.__option == "all" or self.__option == "samples"):
                self.__variables.append(Variable(group))
            # Detection of the DNNModel block
            if group[0]=="DNNMODEL" and (self.__option == "all" or self.__option == "model"):
                self.__model = DNNModel(group,len(self.get_Variables()), is_Train)
            # Detection of the BDTModel block
            if group[0]=="BDTMODEL" and (self.__option == "all" or self.__option == "model"):
                self.__model = BDTModel(group,len(self.get_Variables()), is_Train)
            # Detection of the WGANModel block
            if group[0]=="WGANMODEL" and (self.__option == "all" or self.__option == "model"):
                self.__model = WGANModel(group,len(self.get_Variables()), self.get_Variables(), is_Train)

        #####################################################################################
        ############################### Errors and Warnings #################################
        #####################################################################################
        if len(unique([Sample.get_TrainLabel() for Sample in self.__samples if Sample.get_Group()!=None]))!=len(unique([Sample.get_Group() for Sample in self.__samples if Sample.get_Group()!=None])):
            ErrorMessage("Detected Groups with non-identical training labels. You need to check this!")
        if self.__model.isClassification() and len(unique([Sample.get_TrainLabel() for Sample in self.__samples if Sample.get_TrainLabel()>=0]))<=1:
            ErrorMessage("You need at least 2 samples with different training labels!")

    ##############################################################################
    ############################### Misc methods #################################
    ##############################################################################
    def get_Model(self):
        return self.__model

    def get_Samples(self):
        return self.__samples

    def get_Variables(self):
        return self.__variables

    def get_VarNames(self):
        return [var.get_Name() for var in self.get_Variables()]

    def get_VarLabels(self):
        return [var.get_Label() for var in self.get_Variables()]

    def get_NominalSamples(self):
        return [Sample for Sample in self.__samples if Sample.get_Type()!="Systematic"] # only return non-systematic samples

    def get_SystSamples(self):
        return [Sample for Sample in self.__samples if Sample.get_Type()=="Systematic"] # only return systematic samples

    def get_General(self):
        return self.__general

    def get_Groups(self):
        return self.__groups

    def Print(self):
        if("all" == self.__option or "model" == self.__option):
            print(self.__model)
            InfoDelimiter()
        if("all" == self.__option or "samples" == self.__option):
            print(self.__general)
            InfoDelimiter()
        if("all" == self.__option or "samples" == self.__option):
            for sample in self.__samples:
                print(sample)
                print()
                InfoDelimiter()
                    
class General_Settings:
    """ This class serves as a storage class for general settings that are read from the config file.

    Instance variables:
    Job             --- Job name. This name will be used to create a corresponding directory
    Selection       --- Selection (string) which is applied to all samples when the converter is called.
    MCWeight        --- MC weight (string) which is applied to all samples when the converter is called.
    Treename        --- Name of the input tree from which the Variables are extracted.
    InputScaling    --- Type of scaling that is applied to the inputs. Possible options: 'None', 'MinMax', 'MinMax_Symmetric' and 'Standard'.
    RecalcScaler    --- Boolean. Determines whether the scaler is re-run upon executin of conversion/training/evaluation or not.
    OutputScaling   --- Type of scaling that is applied to the inputs. Possible options: 'None', 'MinMax', 'MinMax_Symmetric' and 'Standard'.
    WeightScaling   --- Boolean. If set to 'True' weights are scaled such that the sum of weights of individual samples is unity. Weights are then devided by the overall mean.
    TreatNegWeights --- Boolean. If set to 'True' negative weights are set to zero.
    Folds           --- Number (integer) of folds that are used during K-Fold validation and in training. 
    ALabel          --- ATLAS Label.
    CMLabel         --- Centre of Mass label.
    MyLabel         --- Additional label to be printed on plots.
    Blinding        --- Float number above which data bins are blinded.
    PlotFormat      --- List of string specifing the output format of the plots
    StackPlotScale  --- String determining whether to use a log/linear scale for stack plots
    SeparationPlotScale  --- String determining whether to use a log/linear scale for (1D) separation plots
    ConfusionPlotScale   --- String determining whether to use a log/linear scale for (binary) confusion plots
    KSPlotScale          --- String determining whether to use a log/linear scale for TrainTest plots
    LargeCorrelation     --- Float number above which a correlation is considered to be large
    RatioMax        --- Value defining the maximum in the lower pad for ratio plots
    RatioMax        --- Value defining the minimum in the lower pad for ratio plots
    """
    def __init__(self,group):
        self.__Job             = None
        self.__Selection       = ""
        self.__MCWeight        = ""
        self.__Treename        = None
        self.__InputScaling    = None
        self.__RecalcScaler    = True
        self.__OutputScaling   = None
        self.__WeightScaling   = True
        self.__TreatNegWeights = True
        self.__Folds           = 1
        self.__TrainMethod     = "Default" # To use in case of a single dataset to be split for example for odd-even evaluation
        self.__ATLASLabel      = ""
        self.__CMLabel         = ""
        self.__CustomLabel     = ""
        self.__DoCtrlPlots     = True
        self.__DoYields        = False
        self.__Blinding        = 999999 # All Histogram values beyond this point are blinded (default shouldn't blind)
        ### Additional Plot Settings ###
        self.__PlotFormat          = ["pdf","png","eps"]
        self.__StackPlotScale      = "Linear"
        self.__SeparationPlotScale = "Linear"
        self.__ConfusionPlotScale  = "Linear"
        self.__KSPlotScale         = "Linear"
        self.__LargeCorrelation    = 0.5
        self.__RatioMax            = 1.55
        self.__RatioMin            = 0.45

        #####################################################################################
        ################################ Reading Settings ###################################
        #####################################################################################
        General_OS = OptionChecker()
        for item in group:
            General_OS.CheckGeneralOption(item)
            if "Job" in item:
                self.__Job = ReadOption(item, "Job", istype="str", islist=False)
            if "Selection" in item:
                self.__Selection = ReadOption(item, "Selection", istype="str", islist=False)
            if "MCWeight" in item:
                self.__MCWeight = ReadOption(item, "MCWeight", istype="str", islist=False)
            if "Treename" in item:
                self.__Treename = ReadOption(item, "Treename", istype="str", islist=False)
            if "InputScaling" in item:
                self.__InputScaling = ReadOption(item, "InputScaling", istype="str", islist=False)
            if "RecalcScaler" in item:
                self.__RecalcScaler = ReadOption(item, "RecalcScaler", istype="bool", islist=False)
            if "OutputScaling" in item:
                self.__OutputScaling = ReadOption(item, "OutputScaling", istype="str", islist=False)
            if "WeightScaling" in item:
                self.__WeightScaling = ReadOption(item, "WeightScaling", istype="bool", islist=False, msg="Could not convert 'WeightScaling' into boolean. Make sure to pass either True or False.")
            if "TreatNegWeights" in item:
                self.__TreatNegWeights = ReadOption(item, "TreatNegWeights", istype="bool", islist=False, msg="Could not convert 'TreatNegWeights' into boolean. Make sure to pass either True or False.")
            if "Folds" in item:
                self.__Folds = ReadOption(item, "Folds", istype="int", islist=False, msg="Could not convert 'Folds' into integer.")
            if "TrainMethod" in item:
                self.__TrainMethod = ReadOption(item, "TrainMethod", istype="str", islist=False, msg="Could not convert 'Trainig method' into string.") # Still to understand if this is the best position
            if "ATLASLabel" in item:
                self.__ATLASLabel = ReadOption(item, "ATLASLabel", istype="str", islist=False)
            if "CMLabel" in item:
                Label = ReadOption(item, "CMLabel", istype="str", islist=False) # This is a little special because we can not parse '#' from the config file
                if(Label!=""):
                    self.__CMLabel = "#sqrt{s}="+Label
            if "CustomLabel" in item:
                self.__CustomLabel = ReadOption(item, "CustomLabel", istype="str", islist=False)
            if "DoCtrlPlots" in item:
                self.__DoCtrlPlots = ReadOption(item, "DoCtrlPlots", istype="bool", islist=False)
            if "DoYields" in item:
                self.__DoYields = ReadOption(item, "DoYields", istype="bool", islist=False)
            if "PlotFormat" in item:
                self.__PlotFormat = ReadOption(item, "PlotFormat", istype="str", islist=True)
            if "StackPlotScale" in item:
                self.__StackPlotScale = ReadOption(item, "StackPlotScale", istype="str", islist=False)
            if "SeparationPlotScale" in item:
                self.__SeparationPlotScale = ReadOption(item, "SeparationPlotScale", istype="str", islist=False)
            if "ConfusionPlotScale" in item:
                self.__ConfusionPlotScale = ReadOption(item, "ConfusionPlotScale", istype="str", islist=False)
            if "KSPlotScale" in item:
                self.__KSPlotScale = ReadOption(item, "KSPlotScale", istype="str", islist=False)
            if "LargeCorrelation" in item:
                self.__LargeCorrelation = ReadOption(item, "LargeCorrelation", istype="float", islist=False)
            if "RatioMin" in item:
                self.__RatioMin = ReadOption(item, "RatioMin", istype="float", islist=False)
            if "RatioMax" in item:
                self.__RatioMax = ReadOption(item, "RatioMax", istype="float", islist=False)
            if "Blinding" in item:
                self.__Blinding = ReadOption(item, "Blinding", istype="float", islist=False)

        #####################################################################################
        ############################### Errors and Warnings #################################
        #####################################################################################
        AllSystTrees = SystTrees() # Object holding all allowed syst trees
        if self.__Job == None:
            ErrorMessage("Could not find 'Job' option in general settings.")
        if self.__Treename == None:
            ErrorMessage("Could not find 'Treename' option in general settings.")
        if self.__Treename != "nominal" and self.__Treename != "nominal_Loose" and AllSystTrees.check_validity(self.__Treename):
            ErrorMessage("Given 'Treename' "+self.__Treename+" does not exist.")
        ###### Checking the given Training Method ######
        if self.__TrainMethod not in ["Default", "BDT-Split", "BDT-4t"]:
            ErrorMessage("Given 'TrainMethod' "+self.__TrainMethod +" not supported. Only allowed options are 'Default', 'BDT-Split', 'BDT-4t'!")
        ###### Checking the given plotting options ######
        if self.__InputScaling not in ["none","minmax","minmax_symmetric","standard"]:
            ErrorMessage("Unknown 'InputScaling' option. Only allowed options are 'none', 'minmax', 'minmax_symmetric' and 'standard'!")
        if not set(self.__PlotFormat).issubset(["pdf","png","eps","jpeg","gif","svg"]):
            ErrorMessage("Unknown 'PlotFormat' provided. The allowed formats are 'pdf', 'png', 'eps', 'jpeg', 'gif' and 'svg'")
        if self.__StackPlotScale not in ["Linear", "Log"]:
            ErrorMessage("Unknown option for 'StackPlotScale'. Allowed options are 'Log' and 'Linear' (Default).")
        if self.__SeparationPlotScale not in ["Linear", "Log"]:
            ErrorMessage("Unknown option for 'SeparationPlotScale'. Allowed options are 'Log' and 'Linear' (Default).")
        if self.__ConfusionPlotScale not in ["Linear", "Log"]:
            ErrorMessage("Unknown option for 'ConfusionPlotScale'. Allowed options are 'Log' and 'Linear' (Default).")
        if self.__KSPlotScale not in ["Linear", "Log"]:
            ErrorMessage("Unknown option for 'KSPlotScale'. Allowed options are 'Log' and 'Linear' (Default).")
        if self.__LargeCorrelation >=1.0 or self.__LargeCorrelation <0.0:
            ErrorMessage("'LargeCorrelation' has to be larger than zero and smaller than 1!")
        if self.__RatioMin < 0:
            ErrorMessage("'RatioMin' can not be smaller than 0!")
        if self.__RatioMax < 0:
            ErrorMessage("'RatioMax' can not be smaller than 0!")
        if self.__RatioMax < self.__RatioMin:
            ErrorMessage("'RatioMax' can not be smaller than 'RatioMin'!")

    ##############################################################################
    ############################### Misc methods #################################
    ##############################################################################
    def get_Job(self):
        return self.__Job

    def get_Selection(self):
        return self.__Selection

    def get_MCWeight(self):
        return self.__MCWeight

    def get_Treename(self):
        return self.__Treename

    def get_InputScaling(self):
        return self.__InputScaling

    def get_RecalcScaler(self):
        return self.__RecalcScaler

    def get_OutputScaling(self):
        return self.__OutputScaling

    def get_WeightScaling(self):
        return self.__WeightScaling

    def get_TreatNegWeights(self):
        return self.__TreatNegWeights

    def get_Folds(self):
        return self.__Folds

    def get_TrainMethod(self):
        return self.__TrainMethod

    def get_ATLASLabel(self):
        return self.__ATLASLabel

    def get_CMLabel(self):
        return self.__CMLabel

    def get_CustomLabel(self):
        return self.__CustomLabel

    def get_Blinding(self):
        return self.__Blinding

    def do_CtrlPlots(self):
        return self.__DoCtrlPlots

    def get_PlotFormat(self):
        return self.__PlotFormat

    def get_StackPlotScale(self):
        return self.__StackPlotScale

    def get_SeparationPlotScale(self):
        return self.__SeparationPlotScale

    def get_ConfusionPlotScale(self):
        return self.__ConfusionPlotScale

    def get_LargeCorrelation(self):
        return self.__LargeCorrelation

    def get_KSPlotScale(self):
        return self.__KSPlotScale

    def get_RatioMax(self):
        return self.__RatioMax

    def get_RatioMin(self):
        return self.__RatioMin

    def do_Yields(self):
        return self.__DoYields

    def __str__(self):
        return ("GENERALINFO:\tSelection: "+self.__Selection
                +"\nGENERALINFO:\tMCWeight: "+self.__MCWeight
                +"\nGENERALINFO:\tTreename: "+self.__Treename
                +"\nGENERALINFO:\tInputScaling: "+self.__InputScaling
                +"\nGENERALINFO:\tWeightScaling: "+str(self.__WeightScaling)
                +"\nGENERALINFO:\tTreatNegWeights: "+str(self.__TreatNegWeights)
                +"\nGENERALINFO:\tFolds: "+str(self.__Folds))

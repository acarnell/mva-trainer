import numpy as np
import itertools
import HelperModules.HelperFunctions as hf
from HelperModules.MessageHandler import ErrorMessage
from HelperModules.Directories import Directories
from sklearn.preprocessing import MinMaxScaler, StandardScaler
from pickle import dump

def ScaleWeights(W, Y, TreatNeg = True):
    """Function to scale the input weights to prevent the training to be imbalanced.

    Keyword arguments:
    W --- Array (float) of weights.
    Y --- Array (float) of Labels.
    TreatNeg --- Boolean to determine what to do with negative weights.

    returns list with scaled weights.
    """
    if len(W) != len(Y):
        ErrorMessage("Weights and labels should have same length but have length "+str(len(W))+" and "+str(len(Y))+".")
    # If we decide we want to handle negative weights we simply set them to zero. This is not optimal.
    if TreatNeg == True:
        W[W<0]=0
    unique_labels = np.unique(Y)
    for i in unique_labels:
        if i==-1:
            continue
        else:
            W[Y==i]/=np.sum(W[Y==i])
    W[Y!=-1]/=np.mean(W[Y!=-1])
    return W

def ScaleInputs(X,Option="minmax", scaler=None):
    """Function to scale the input features. Possible options are 'minmax', 'standard' and 'none'
    If 'minmax' is chosen the variables are scaled into [0,1].
    If 'standard' is chosen the varibales are standardised by removing the mean and scaling to unit variance.
    If 'none' is chosen no input scaling is applied.

    Keyword arguments:
    X      --- Array (float) of features.
    Option ---String to determine the scaling option.
    scaler --- Scaler object in case it is already available

    returns array of scaled features and corresponding scaler.
    """
    if scaler==None:
        if Option.lower() == "none":
            return X, None
        elif Option.lower() =="minmax":
            scaler = MinMaxScaler()
        elif Option.lower() =="minmax_symmetric":
            scaler = MinMaxScaler(feature_range=(-1,1))
        elif Option.lower() =="standard":
            scaler = StandardScaler()
        else:
            ErrorMessage("Unknown scaling method: "+Option)
        X = scaler.fit_transform(X)
        return X, scaler
    else:
        X = scaler.transform(X)
        return X, scaler

def Preprocess(X, Y, W, settings, scaler=None):
    """Function to perform the preprocessing steps.

    Keyword arguments:
    X        --- Array (float) of features.
    Y        --- Array (float) of Labels.
    W        --- Array (float) of weights.
    settings --- Settings from the config file.

    returns array of scaled features, weights and corresponding scaler.
    """
    InputScaleOption  = settings.get_General().get_InputScaling()
    WeightScaleOption = settings.get_General().get_WeightScaling()
    Folds             = settings.get_General().get_Folds()
    TreatNeg          = settings.get_General().get_TreatNegWeights()
    X, scaler = ScaleInputs(X,InputScaleOption, scaler)
    Dirs = Directories(settings.get_General().get_Job())
    dump(scaler, open(hf.ensure_trailing_slash(Dirs.ModelDir())+"scaler.pkl", 'wb'))

    if (WeightScaleOption==True):
        return (X,ScaleWeights(W,Y,TreatNeg), scaler)
    if (WeightScaleOption==False):
        return (X, W, scaler)

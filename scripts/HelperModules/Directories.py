"""
Module for directories files are saved in.
"""
from HelperModules.HelperFunctions import create_output_dir, ensure_trailing_slash

class Directories:
    """
    This class defines output directories
    """
    def __init__(self, output_path):
        self.Dir_dict = {"Main" : output_path,
                         "Plots" : ensure_trailing_slash(output_path)+"Plots",
                         "MVAPlots" : ensure_trailing_slash(output_path)+"Plots/MVAPlots",
                         "CtrlPlots" : ensure_trailing_slash(output_path)+"Plots/CtrlPlots",
                         "Data" : ensure_trailing_slash(output_path)+"Data",
                         "Model" : ensure_trailing_slash(output_path)+"Model",
                         "Configs" : ensure_trailing_slash(output_path)+"Configs",
                         "Lwtnn" : ensure_trailing_slash(output_path)+"lwtnn"}
        for v in self.Dir_dict.values():
            create_output_dir(v)

    def MainDir(self):
        """
        Function to return main directory path.
        """
        return self.Dir_dict["Main"]

    def PlotDir(self):
        """
        Function to return plot directory path.
        """
        return self.Dir_dict["Plots"]

    def CtrlPlotDir(self):
        """
        Function to return ctrl plot directory path.
        """
        return self.Dir_dict["CtrlPlots"]

    def MVAPlotDir(self):
        """
        Function to return mva plot directory path.
        """
        return self.Dir_dict["MVAPlots"]

    def DataDir(self):
        """
        Function to return data directory path.
        """
        return self.Dir_dict["Data"]

    def ModelDir(self):
        """
        Function to return model directory path.
        """
        return self.Dir_dict["Model"]

    def ConfigDir(self):
        """
        Function to return config directory path.
        """
        return self.Dir_dict["Configs"]

    def LwtnnDir(self):
        """
        Function to return lwtnn directory path.
        """
        return self.Dir_dict["Lwtnn"]

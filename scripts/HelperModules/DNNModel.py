"""
Module for a DNN model
"""

import pandas as pd
import os
import numpy as np
import tensorflow as tf
import HelperModules.CustomFunctions
import HelperModules.CustomLosses as cl
import HelperModules.HelperFunctions as hf
from collections import defaultdict
from HelperModules.OptionChecker import OptionChecker
from HelperModules.BasicModel import BasicModel
from HelperModules.MessageHandler import ErrorMessage, WarningMessage, TrainerMessage, TrainerDoneMessage, OptimiserMessage
from tensorflow.keras.layers import Add, Dense, Dropout, concatenate, BatchNormalization, Input, multiply
from tensorflow.keras.callbacks import EarlyStopping, TensorBoard
from tensorflow.keras import regularizers
from sklearn.model_selection import train_test_split

class DNNModel(BasicModel):
    """
    Class for implementing feed-forward-style neural networks.
    This class serves as a base class for any such type of neural network.
    """
    def __init__(self, group, input_dim, is_Train=True):
        super().__init__(group)
        self.__InputDim              = input_dim       # Input dimensionality of the data
        self.__kerasmodel            = None            # Keras model object
        self.__PredesignedModel      = None            # For usage of pre-designed models
        self.__Nodes                 = None            # Number of nodes (list) per layer
        self.__MaxNodes              = 100             # Maximum number of nodes per layer for optimisation
        self.__MinNodes              = 10              # Minimum number of nodes per layer for optimisation
        self.__StepNodes             = 10              # Defines granularity between Min/MaxNodes per layer for optimisation
        self.__MaxLayers             = 7               # Maximum number of layers for optimisation
        self.__MinLayers             = 1               # Minimum number of layers for optimisation
        self.__LearningRate          = 0.001           # Learning rate
        self.__Patience              = None            # Number of epochs in ES to wait for improvement
        self.__MinDelta              = None            # Minimum improvement for ES
        self.__DropoutIndece         = []              # Layers after which Dropout layers are added
        self.__DropoutProb           = 0.1             # Dropout probability (0.2=20%)
        self.__BatchNormIndece       = []              # Layers after which BatchNormalisation layers are added
        self.__OutputSize            = None            # Size of the output layer
        self.__ActivationFunctions   = None            # List of activations functions
        self.__SetActivationFunctions= None            # List to draw activations frunctions from for optimisation
        self.__KernelInitialiser     = "glorot_normal" # Kernel initialisation
        self.__KernelRegulariser     = None            # Regulariser to apply a penalty on the layer's kernel
        self.__BiasRegulariser       = None            # Regulariser to apply a penalty on the layer's biases
        self.__ActivityRegulariser   = None            # Regulariser to apply a penalty on the layer's outputs
        self.__KernelRegulariserl1   = 0.01            # L1 regularisation applied on the layer's kernel
        self.__KernelRegulariserl2   = 0.01            # L2 regularisation applied on the layer's kernel
        self.__BiasRegulariserl1     = 0.01            # L1 regularisation applied on the layer's bias
        self.__BiasRegulariserl2     = 0.01            # L2 regularisation applied on the layer's bias
        self.__ActivityRegulariserl1 = 0.01            # L1 regularisation applied on the layer's outputs
        self.__ActivityRegulariserl2 = 0.01            # L2 regularisation applied on the layer's outputs
        self.__OutputActivation      = None            # Activation function for the output layer
        self.__Loss                  = None            # Loss function to be used
        self.__Optimiser             = "Nadam"         # Optimiser to be used in for neural network loss optimisation
        self.__SmoothingAlpha        = None            # Parameter to be used for label smoothing
        self.__UseTensorBoard        = False           # Whether to use TensorBoard or not
        self.__RegressionTarget      = None            # Name of truth variable for regression
        self.__RegressionTargetLabel = None            # Label of the variable for regression for plots
        self.__RegressionScale       = None            # Scale factor to scale the output for regression
        self.__ReweightTargetDist    = True            # Whether to reweight the target distribution
        self.__HuberDelta            = None            # Delta value for Huber_loss
        self.__Verbosity             = 1               # Verbosity level for tensorflow (Default 1)

        self.__AllowedDNNTypes     = ["Classification-DNN",
                                      "Regression-DNN",
                                      "3LZ-Reconstruction",
                                      "4LZ-Reconstruction"]

        #######################################################################
        ############################ Input Options ############################
        #######################################################################
        DNNModel_OS = OptionChecker()
        for item in self._BasicModel__Group:
            DNNModel_OS.CheckDNNModelOption(item)
            if "Nodes" in item and all([not string in item for string in ["MaxNodes","MinNodes","StepNodes"]]):
                self.__Nodes = hf.ReadOption(item, "Nodes", istype="int", islist=True, msg="Could not load 'Nodes'. Make sure you pass a comma-separated list of integer.")
            if "MaxNodes" in item:
                self.__MaxNodes = hf.ReadOption(item, "MaxNodes", istype="int", islist=False, msg="Could not load 'MaxNodes'. Make sure you an integer.")
            if "MinNodes" in item:
                self.__MinNodes = hf.ReadOption(item, "MinNodes", istype="int", islist=False, msg="Could not load 'MinNodes'. Make sure you pass an integer.")
            if "StepNodes" in item:
                self.__StepNodes = hf.ReadOption(item, "StepNodes", istype="int", islist=False, msg="Could not load 'StepNodes'. Make sure you an integer.")
            if "MaxLayers" in item:
                self.__MaxLayers = hf.ReadOption(item, "MaxLayers", istype="int", islist=False, msg="Could not load 'MaxLayers'. Make sure you an integer.")
            if "MinLayers" in item:
                self.__MinLayers = hf.ReadOption(item, "MinLayers", istype="int", islist=False, msg="Could not load 'MinLayers'. Make sure you pass an integer.")
            if "PredesignedModel" in item:
                self.__PredesignedModel = hf.ReadOption(item, "PredesignedModel", istype="str", islist=False, msg="Could not load 'PredesignedModel'. Make sure you pass a string.")
            if "LearningRate" in item:
                self.__LearningRate = hf.ReadOption(item, "LearningRate", istype="float", islist=False, msg="Could not load 'LearningRate'. Make sure you pass a float.")
            if "Patience" in item:
                self.__Patience = hf.ReadOption(item, "Patience", istype="int", islist=False, msg="Could not load 'Patience'. Make sure you pass an integer.")
            if "MinDelta" in item:
                self.__MinDelta = hf.ReadOption(item, "MinDelta", istype="float", islist=False, msg="Could not load 'MinDelta'. Make sure you pass a float.")
            if "DropoutIndece" in item:
                self.__DropoutIndece = hf.ReadOption(item, "DropoutIndece", istype="int", islist=True, msg="Could not load 'DropoutIndece'. Make sure you pass a comma-separated list of integers.")
            if "DropoutProb" in item:
                self.__DropoutProb = hf.ReadOption(item, "DropoutProb", istype="float", islist=False, msg="Could not load 'DropoutProb'. Make sure you pass a float.")
            if "BatchNormIndece" in item:
                self.__BatchNormIndece = hf.ReadOption(item, "BatchNormIndece", istype="int", islist=True, msg="Could not load 'BatchNormIndece'. Make sure you pass a comma-separated list of integers.")
            if "ActivationFunctions" in item and not "SetActivationFunctions" in item:
                self.__ActivationFunctions = hf.ReadOption(item, "ActivationFunctions", istype="str", islist=True)
            if "SetActivationFunctions" in item:
                self.__SetActivationFunctions = hf.ReadOption(item, "SetActivationFunctions", istype="str", islist=True)
            if "KernelInitialiser" in item:
                self.__KernelInitialiser = hf.ReadOption(item, "KernelInitialiser", istype="str", islist=False)
            if "KernelRegulariser" in item:
                self.__KernelRegulariser = hf.ReadOption(item, "KernelRegulariser", istype="str", islist=True)
            if "BiasRegulariser" in item:
                self.__BiasRegulariser = hf.ReadOption(item, "BiasRegulariser", istype="str", islist=True)
            if "ActivityRegulariser" in item:
                self.__ActivityRegulariser = hf.ReadOption(item, "ActivityRegulariser", istype="str", islist=True)
            if "OutputSize" in item:
                self.__OutputSize = hf.ReadOption(item, "OutputSize", istype="int", islist=False, msg="Could not load 'OutputSize'. Make sure you pass an integer.")
            if "OutputActivation" in item:
                self.__OutputActivation = hf.ReadOption(item, "OutputActivation")
            if "Loss" in item:
                self.__Loss = hf.ReadOption(item, "Loss", istype="str", islist=False)
            if "Optimiser" in item:
                self.__Optimiser = hf.ReadOption(item, "Optimiser", istype="str", islist=False)
            if "SmoothingAlpha" in item:
                self.__SmoothingAlpha = hf.ReadOption(item, "SmoothingAlpha", istype="float", islist=False)
            if "UseTensorBoard" in item:
                self.__UseTensorBoard = hf.ReadOption(item, "UseTensorBoard", istype="bool", islist=False)
            if "RegressionTarget" in item and not "RegressionTargetLabel" in item:
                self.__RegressionTarget = hf.ReadOption(item, "RegressionTarget", istype="str", islist=False)
            if "RegressionTargetLabel" in item:
                self.__RegressionTargetLabel = hf.ReadOption(item, "RegressionTargetLabel", istype="str", islist=False)
            if "RegressionScale" in item:
                self.__RegressionScale = hf.ReadOption(item, "RegressionScale", istype="float", islist=False)
            if "ReweightTargetDist" in item:
                self.__ReweightTargetDist = hf.ReadOption(item, "ReweightTargetDist", istype="bool", islist=False)
            if "HuberDelta" in item:
                self.__HuberDelta = hf.ReadOption(item, "HuberDelta", istype="float", islist=False)
            if "Verbosity" in item:
                self.__Verbosity = hf.ReadOption(item, "Verbosity", istype="int", islist=False)

        #############################################################################
        ############################ Check Input Options ############################
        #############################################################################
        if self._BasicModel__Type not in self.__AllowedDNNTypes:
            ErrorMessage("MODEL " + self._BasicModel__Name + " has invalid type set for DNNMODEL!")
        if self.__MaxNodes < self.__MinNodes:
            ErrorMessage("MODEL " + self._BasicModel__Name + " has larger value set for 'MinNodes' than for 'MaxNodes'!")
        if self.__MaxNodes < 0 or self.__MinNodes < 0:
            ErrorMessage("MODEL " + self._BasicModel__Name + " has negative value set for 'MinNodes' or 'MaxNodes'!")
        if self.__MaxLayers < self.__MinLayers:
            ErrorMessage("MODEL " + self._BasicModel__Name + " has larger value set for 'MinLayers' than for 'MaxLayers'!")
        if self.__MaxLayers < 0 or self.__MinLayers < 0:
            ErrorMessage("MODEL " + self._BasicModel__Name + " has negative value set for 'MinLayers' or 'MaxLayers'!")
        if self._BasicModel__Type!="Classification-DNN" and self.__PredesignedModel is not None:
            ErrorMessage("MODEL " + self._BasicModel__Name + " has predesigned model "+self.__PredesignedModel+" set but type is "+self._BasicModel__Type+". Predesigned models only work for 'Type' Classification-DNN!")
        if self.__PredesignedModel is not None and self.__Nodes is not None and is_Train:
            WarningMessage("MODEL " + self._BasicModel__Name + " has predesigned model "+self.__PredesignedModel+" set as well as 'Nodes'. 'Nodes' has no effect for predesigned models.")
        if self.__PredesignedModel is not None and self.__DropoutIndece is not None and is_Train:
            WarningMessage("MODEL " + self._BasicModel__Name + " has predesigned model "+self.__PredesignedModel+" set as well as 'DropoutIndece'. 'DropoutIndece' has no effect for predesigned models.")
        if self.__PredesignedModel is not None and self.__BatchNormIndece is not None and is_Train:
            WarningMessage("MODEL " + self._BasicModel__Name + " has predesigned model "+self.__PredesignedModel+" set as well as 'BatchNormIndece'. 'BatchNormIndece' has no effect for predesigned models.")
        if self.__Nodes is None and self.__PredesignedModel is None:
            ErrorMessage("MODEL " + self._BasicModel__Name + " has no 'Nodes' defined. You need to pass a comma-separated list of integers to define an architecture or use the 'PredesignedModel' parameter.")
        if self.__Nodes is not None and self.__PredesignedModel is not None and is_Train:
            WarningMessage("MODEL " + self._BasicModel__Name + " has both 'Nodes' and 'PredesignedModel' defined. The pre-designed model "+self.__PredesignedModel+" is now used.")
        if self.__MinDelta is not None:
            if self.__MinDelta<=0:
                ErrorMessage("MODEL " + self._BasicModel__Name + " has 'MinDelta' set to "+str(self.__MinDelta)+". 'MinDelta' must be >0.")
        if self.__DropoutProb>=1:
            ErrorMessage("MODEL " + self._BasicModel__Name + " has 'DropoutProb' set to "+str(self.__DropoutProb)+". 'DropoutProb' must be <1.")
        if self.__OutputSize is None and is_Train:
            WarningMessage("MODEL " + self._BasicModel__Name + " has no 'OutputSize' set. Setting OutputSize=1.")
            self.__OutputSize = 1
        if self.__ActivationFunctions is None:
            if is_Train:
                WarningMessage("MODEL " + self._BasicModel__Name + " has no 'ActivationFunctions' set. Setting all activations functions to 'relu'.")
            self.__ActivationFunctions = ["relu"]*len(self.__Nodes)
        if not set(self.__ActivationFunctions).issubset(["relu", "sigmoid", "swish", "softmax", "softplus", "softsign", "tanh", "selu", "elu", "exponential"]) and is_Train:
            ErrorMessage("MODEL " + self._BasicModel__Name + " has unknown 'ActivationFunctions' set.")
        if self.__SetActivationFunctions is None:
            if is_Train==True:
                WarningMessage("MODEL " + self._BasicModel__Name + " has no 'SetActivationFunctions' set. Setting all activations functions to 'relu'.")
            self.__SetActivationFunctions = ["relu"]
        if not set(self.__SetActivationFunctions).issubset(["relu", "sigmoid", "swish", "softmax", "softplus", "softsign", "tanh", "selu", "elu", "exponential"]) and is_Train:
            ErrorMessage("MODEL " + self._BasicModel__Name + " has unknown activation function set in 'SetActivationFunctions'.")
        if len(self.__ActivationFunctions)!=len(self.__Nodes):
            ErrorMessage("MODEL " + self._BasicModel__Name + " has 'ActivationFunctions' set but length does not correspond to length of 'Nodes' parameter.")
        if self.__KernelInitialiser not in ["glorot_uniform", "glorot_normal", "lecun_normal", "lecun_uniform"]:
            ErrorMessage("MODEL " + self._BasicModel__Name + " has unknown 'KernelInitialiser' set.")
        if self.__KernelRegulariser is None:
            self.__KernelRegulariser = ["None"]*len(self.__Nodes)
        elif not set(self.__KernelRegulariser).issubset(["None","l1", "l2", "l1_l2"]) and is_Train:
            ErrorMessage("MODEL " + self._BasicModel__Name + " has unknown 'KernelRegulariser' set.")
        if self.__BiasRegulariser is None:
            self.__BiasRegulariser = ["None"]*len(self.__Nodes)
        elif not set(self.__BiasRegulariser).issubset(["None","l1", "l2", "l1_l2"]) and is_Train:
            ErrorMessage("MODEL " + self._BasicModel__Name + " has unknown 'BiasRegulariser' set.")
        if self.__ActivityRegulariser is None:
            self.__ActivityRegulariser = ["None"]*len(self.__Nodes)
        elif not set(self.__ActivityRegulariser).issubset(["None","l1", "l2", "l1_l2"]) and is_Train:
            ErrorMessage("MODEL " + self._BasicModel__Name + " has unknown 'ActivityRegulariser' set.")
        if self.__OutputActivation is None and is_Train:
            WarningMessage("MODEL " + self._BasicModel__Name + " has no 'OutputActivation' set. Setting OutputActivatation to 'relu'.")
            self.__OutputActivation = "relu"
        if self.__Loss is None:
            ErrorMessage("MODEL " + self._BasicModel__Name + " has no 'Loss' set.")
        elif self.__Loss == "categorical_focal_loss":
            self.__Loss = cl.categorical_focal_loss(gamma=2.0, alpha=0.25)
        elif self.__Loss == "binary_focal_loss":
            self.__Loss = cl.binary_focal_loss(gamma=2.0, alpha=0.25)
        elif self.__Loss == "huber_loss":
            if self.__HuberDelta is None:
                WarningMessage("MODEL " + self._BasicModel__Name + " has huber_loss set as 'Loss' but no 'HuberDelta. Setting 'HuberDelta to 1.5 now.")
                self.__HuberDelta = 1.5
            self.__Loss = tf.keras.losses.Huber(delta=self.__HuberDelta, reduction="auto", name="huber_loss")
        elif self.__Loss not in ["binary_crossentropy", "categorical_crossentropy", "mean_squared_error", "mean_absolute_error", "mean_absolute_percentage_error", "mean_squared_logarithmic_error", "cosine_similarity", "huber_loss", "log_cosh"]:
            ErrorMessage("MODEL " + self._BasicModel__Name + " has unknown or unsupported 'Loss' set: "+self.__Loss)
        if self.__Loss == "binary_crossentropy" and self.__OutputSize != 1:
            ErrorMessage("MODEL " + self._BasicModel__Name + " has 'binary crossentropy' set as 'Loss'. This does not make sense for a model with an outputsize of "+str(self.__OutputSize)+".")
        if self.__Optimiser not in ["Adadelta","Adagrad","Adam","Adamax","Ftrl","Nadam","RMSprop","SGD"]:
            ErrorMessage("MODEL " + self._BasicModel__Name + " has unsupported 'Optimiser' set. Optimiser must be in 'Adadelta','Adagrad','Adam','Adamax','Ftrl','Nadam','RMSprop','SGD'.")
        if self._BasicModel__ClassLabels is None:
            self._BasicModel__ClassLabels = ["Binary"]
        if self._BasicModel__ClassLabels is not None and len(self._BasicModel__ClassLabels)!=self.__OutputSize:
            ErrorMessage("MODEL " + self._BasicModel__Name + " has "+str(len(self._BasicModel__ClassLabels))+" class names defined but output size is "+str(self.__OutputSize)+".")
        if self._BasicModel__ClassLabels is None and self.__OutputSize>1:
            ErrorMessage("MODEL " + self._BasicModel__Name + " has 0 class names defined but output size is "+str(self.__OutputSize)+".")
        if self.__SmoothingAlpha is not None and (self.__SmoothingAlpha > 0.5 or self.__SmoothingAlpha<0):
            ErrorMessage("MODEL " + self._BasicModel__Name + " has invalid SmoothingAlpha value! Must be between >0 and <0.5!")
        if not self.isRegressionDNN() and self.__RegressionTarget is not None:
            ErrorMessage("MODEL " + self._BasicModel__Name + " has 'RegressionTarget' set! This is only allowed for NN with type 'Regression-DNN'!")
        if self.isRegressionDNN() and self.__RegressionTarget is None:
            ErrorMessage("MODEL " + self._BasicModel__Name + " has type 'Regression-DNN' set but no 'RegressionTarget' was defined.")
        if not self.isRegressionDNN() and self.__RegressionTargetLabel is not None:
            ErrorMessage("MODEL " + self._BasicModel__Name + " has 'RegressionTargetLabel' set! This is only allowed for NN with type 'Regression-DNN'!")
        if self.isRegressionDNN() and self.__RegressionTargetLabel is None:
            WarningMessage("MODEL " + self._BasicModel__Name + " has type 'Regression-DNN' set but no 'RegressionTargetLabel' was defined. Using 'RegressionTarget' for the label instead.")
            self.__RegressionTargetLabel = self.__RegressionTarget
        if not self.isRegressionDNN() and self.__RegressionScale is not None:
            ErrorMessage("MODEL " + self._BasicModel__Name + " has 'RegressionScale' set! This is only allowed for NN with type 'Regression-DNN'!")
        if self.isRegressionDNN() and self.__RegressionScale is None:
            WarningMessage("MODEL " + self._BasicModel__Name + " has no 'RegressionScale' set! Using default of 1.0!")
            self.__RegressionScale = 1.0
        if not self.isRegressionDNN and self.__HuberLoss is not None:
            ErrorMessage("Found definition of 'huber_loss' as Loss. This is only allowed for a 'Regression-DNN'.")
        if self.__HuberDelta is not None:
            if self.__HuberDelta < 0:
                ErrorMessage("Found negative 'HuberDelta'. You have to pass a positive float value!")
        if self.__Verbosity not in [0,1,2]:
            ErrorMessage("MODEL " + self._BasicModel__Name + " has invalid 'Verbosity' set! Possible options are 0 (silent), 1 (progress bar) and 2 (one line per epoch).")

    ###########################################################################
    ############################### Misc methods ##############################
    ###########################################################################
    def get_Optimiser(self):
        """
        Getter method to return the actual optimiser object
        """
        if self.__Optimiser == "Adadelta":
            return tf.keras.optimizers.Adadelta(learning_rate=self.__LearningRate, rho=0.95, epsilon=1e-07, name='Adadelta')
        if self.__Optimiser == "Adagrad":
            return tf.keras.optimizers.Adagrad(learning_rate=self.__LearningRate, initial_accumulator_value=0.1, epsilon=1e-07, name='Adagrad')
        if self.__Optimiser == "Adam":
            return tf.keras.optimizers.Adam(learning_rate=self.__LearningRate, beta_1=0.9, beta_2=0.999, epsilon=1e-07, amsgrad=False, name='Adam')
        if self.__Optimiser == "Adamax":
            return tf.keras.optimizers.Adamax(learning_rate=self.__LearningRate, beta_1=0.9, beta_2=0.999, epsilon=1e-07, name='Adamax')
        if self.__Optimiser == "Ftrl":
            return tf.keras.optimizers.Ftrl(learning_rate=self.__LearningRate, learning_rate_power=-0.5, initial_accumulator_value=0.1, l1_regularization_strength=0.0, l2_regularization_strength=0.0, name='Ftrl', l2_shrinkage_regularization_strength=0.0, beta=0.0)
        if self.__Optimiser == "Nadam":
            return tf.keras.optimizers.Nadam(learning_rate=self.__LearningRate, beta_1=0.9, beta_2=0.999, epsilon=1e-07, name='Nadam')
        if self.__Optimiser == "RMSprop":
            return tf.keras.optimizers.RMSprop(learning_rate=self.__LearningRate, rho=0.9, momentum=0.0, epsilon=1e-07, centered=False, name='RMSprop')
        if self.__Optimiser == "SGD":
            return tf.keras.optimizers.SGD(learning_rate=self.__LearningRate, momentum=0.0, nesterov=False, name='SGD')

    def get_PredesignedModel(self):
        """
        Getter method returning the identifier string for a predesigned model
        """
        return self.__PredesignedModel

    def get_Nodes(self):
        """
        Getter method returning the nodes list (integer)
        """
        return self.__Nodes

    def get_MinNodes(self):
        """
        Getter method returning the minimum number of nodes
        """
        return self.__MinNodes

    def get_MaxNodes(self):
        """
        Getter method returning the maximum number of nodes
        """
        return self.__MaxNodes

    def get_StepNodes(self):
        """
        Getter method returning the step size between different node
        configurations for optimisation
        """
        return self.__StepNodes

    def get_MinLayers(self):
        """
        Getter method returning the minimum number of layers
        """
        return self.__MinLayers

    def get_MaxLayers(self):
        """
        Getter method returning the maximum number of layers
        """
        return self.__MaxLayers

    def get_SetActivationFunctions(self):
        """
        Getter method returning the set activations functions
        from which the optimisation algorithm can choose
        """
        return self.__SetActivationFunctions

    def get_BatchNormIndece(self):
        """
        Getter method returning the list of layer indece
        behind which batchnormalisation layers are placed
        """
        return self.__BatchNormIndece

    def get_DropoutIndece(self):
        """
        Getter method returning the list of layer indece
        behind which dropout layers are placed
        """
        return self.__DropoutIndece

    def get_DropoutProb(self):
        """
        Getter method returning the global dropout probability
        """
        return self.__DropoutProb

    def get_OutputSize(self):
        """
        Getter method returning the output size
        """
        return self.__OutputSize

    def get_RegressionTarget(self):
        """
        Getter method returning the name of the variable in
        the ntuples used as the regression target.
        """
        return self.__RegressionTarget

    def get_RegressionTargetLabel(self):
        """
        Getter method returning the label for the variable in
        the ntuples which is used as the regression target.
        """
        return self.__RegressionTargetLabel

    def get_RegressionScale(self):
        """
        Getter method returning the scale for the variable in
        the ntuples which used as the regression target.
        """
        return self.__RegressionScale

    def ReweightTargetDist(self):
        """
        Getter method returning boolean to decide whether
        the target distribution is reweighted (flattened).
        """
        return self.__ReweightTargetDist

    def DefineRegulariser(self,Regulariser,l1,l2):
        """
        Simple function to create a list of keras.regularizers objects
        """
        Regularisers = []
        for R in Regulariser:
            if R == "l1":
                Regularisers.append(regularizers.l1(l1=l1))
            elif R == "l2":
                Regularisers.append(regularizers.l2(l2=l2))
            elif R == "l1_l2":
                Regularisers.append(regularizers.l1_l2(l1=l1, l2=l2))
            elif R == "None":
                Regularisers.append(None)
        return Regularisers

    def DNNCompile(self):
        """
        Compile a simple densely connected NN. This is our heavy lifting method.
        It is used whenever the user uses the "Nodes" option in the config file to build
        a densely connected NN.
        """
        DropoutIndece = self.__DropoutIndece
        BatchNormIndece = self.__BatchNormIndece
        # Set up the regularisers
        KernelRegularisers = self.DefineRegulariser(self.__KernelRegulariser, self.__KernelRegulariserl1, self.__KernelRegulariserl2)
        BiasRegularisers   = self.DefineRegulariser(self.__BiasRegulariser, self.__BiasRegulariserl1, self.__BiasRegulariserl2)
        ActivityRegularisers = self.DefineRegulariser(self.__ActivityRegulariser, self.__ActivityRegulariserl1, self.__ActivityRegulariserl2)
        # Lets build a feed forward NN using keras from scratch
        Layers = []
        Layers.append(Input(shape=(self.__InputDim,), name = "Input_Layer"))
        for NodeLayerID, nodes in enumerate(self.__Nodes):
            Layers.append(Dense(nodes,
                                name = "Hidden_Layer_"+str(NodeLayerID),
                                activation=self.__ActivationFunctions[NodeLayerID],
                                kernel_initializer=self.__KernelInitialiser,
                                kernel_regularizer=KernelRegularisers[NodeLayerID],
                                bias_regularizer=BiasRegularisers[NodeLayerID],
                                activity_regularizer=ActivityRegularisers[NodeLayerID])(Layers[-1]))
            if len(DropoutIndece)!=0:
                if NodeLayerID in DropoutIndece:
                    Layers.append(Dropout(self.__DropoutProb)(Layers[-1])) # Adding a Dropout layers
            if len(BatchNormIndece)!=0:
                if NodeLayerID in BatchNormIndece:
                    Layers.append(BatchNormalization()(Layers[-1])) # Adding BatchNormalisation layers
        Layers.append(Dense(self.__OutputSize, activation=self.__OutputActivation, name="Output_Layer")(Layers[-1]))
        self.__kerasmodel = tf.keras.Model(inputs=Layers[0],outputs=Layers[-1])
        self.__kerasmodel.compile(loss = [self.__Loss],
                                  optimizer = self.get_Optimiser(),
                                  metrics = self._BasicModel__Metrics)
        self.__kerasmodel.summary()

    def PDNNCompile(self):
        """
        Compile a custom connected NN with two parallel layers.
        Takes simple list of variables as input.
        """
        Input_Layer = Input(shape=(self.__InputDim,))
        Dense_Layer = Dense(50, activation = "relu")(Input_Layer)

        # First parallel arm
        Dense_Layer_1   = Dense(50, activation = "relu")(Dense_Layer)
        Dropout_Layer_1 = Dropout(0.2)(Dense_Layer_1)
        Dense_Layer_1   = Dense(50, activation = "relu")(Dropout_Layer_1)

        # Second parallel arm
        Dense_Layer_2   = Dense(50, activation = "relu")(Dense_Layer)
        Dropout_Layer_2 = Dropout(0.2)(Dense_Layer_2)
        Dense_Layer_2   = Dense(50, activation = "relu")(Dropout_Layer_2)

        # Concatenation Layer
        Concat_Layer  = concatenate([Dense_Layer_1, Dense_Layer_2])
        Dense_Layer   = Dense(50, activation='relu')(Concat_Layer)
        Output_Layer  = Dense(self.__OutputSize, activation=self.__OutputActivation,name="Output")(Dense_Layer)
        self.__kerasmodel = tf.keras.Model(inputs=Input_Layer,outputs=Output_Layer)
        self.__kerasmodel.compile(loss = [self.__Loss],
                                  optimizer = self.get_Optimiser(),
                                  metrics = self._BasicModel__Metrics)
        self.__kerasmodel.summary()

    def DPDNNCompile(self):
        """
        Compile a custom connected NN with four parallel layers.
        Takes simple list of variables as input.
        """
        Input_Layer = Input(shape=(self.__InputDim,))
        Dense_Layer = Dense(50, activation = "relu")(Input_Layer)

        # First parallel arm
        Dense_Layer_1   = Dense(50, activation = "relu")(Dense_Layer)
        Dropout_Layer_1 = Dropout(0.2)(Dense_Layer_1)
        Dense_Layer_1   = Dense(50, activation = "relu")(Dropout_Layer_1)

        # Second parallel arm
        Dense_Layer_2   = Dense(50, activation = "relu")(Dense_Layer)
        Dropout_Layer_2 = Dropout(0.2)(Dense_Layer_2)
        Dense_Layer_2   = Dense(50, activation = "relu")(Dropout_Layer_2)

        # Third parallel arm
        Dense_Layer_3   = Dense(50, activation = "relu")(Dense_Layer)
        Dropout_Layer_3 = Dropout(0.2)(Dense_Layer_3)
        Dense_Layer_3   = Dense(50, activation = "relu")(Dropout_Layer_3)

        # Fourth parallel arm
        Dense_Layer_4   = Dense(50, activation = "relu")(Dense_Layer)
        Dropout_Layer_4 = Dropout(0.2)(Dense_Layer_4)
        Dense_Layer_4   = Dense(50, activation = "relu")(Dropout_Layer_4)

        # Concatenation Layer
        Concat_Layer  = concatenate([Dense_Layer_1, Dense_Layer_2, Dense_Layer_3, Dense_Layer_4])
        Dense_Layer   = Dense(50, activation='relu')(Concat_Layer)
        Output_Layer  = Dense(self.__OutputSize, activation=self.__OutputActivation,name="Output")(Dense_Layer)
        self.__kerasmodel = keras.Model(inputs=Input_Layer,outputs=Output_Layer)
        self.__kerasmodel.compile(loss = [self.__Loss],
                                  optimizer = self.get_Optimiser(),
                                  metrics = self._BasicModel__Metrics)
        self.__kerasmodel.summary()

    def PADNNCompile(self):
        """
        Compile a custom connected NN with two parallel layers including attention.
        Takes simple list of variables as input.
        """
        Input_Layer = Input(shape=(self.__InputDim,))
        Dense_Layer = Dense(50, activation = "relu")(Input_Layer)

        # First parallel arm
        Dense_Layer_1          = Dense(50, activation = "relu")(Dense_Layer)
        Attention_Prob_Layer_1 = Dense(50, activation = "sigmoid")(Dense_Layer_1)
        Attention_Layer_1      = multiply([Dense_Layer_1, Attention_Prob_Layer_1])
        Dropout_Layer_1        = Dropout(0.2)(Attention_Layer_2)
        Dense_Layer_1          = Dense(50, activation = "relu")(Dropout_Layer_1)

        # Second parallel arm
        Dense_Layer_2          = Dense(50, activation = "relu")(Dense_Layer)
        Attention_Prob_Layer_2 = Dense(50, activation = "sigmoid")(Dense_Layer_2)
        Attention_Layer_2      = multiply([Dense_Layer_2, Attention_Prob_Layer_2])
        Dropout_Layer_2        = Dropout(0.2)(Attention_Layer_2)
        Dense_Layer_2          = Dense(50, activation = "relu")(Dropout_Layer_2)

        # Concatenation Layer
        Concat_Layer  = concatenate([Dense_Layer_1, Dense_Layer_2])
        Dense_Layer   = Dense(50, activation='relu')(Concat_Layer)
        Output_Layer  = Dense(self.__OutputSize, activation=self.__OutputActivation,name="Output")(Dense_Layer)
        self.__kerasmodel = keras.Model(inputs=Input_Layer,outputs=Output_Layer)
        self.__kerasmodel.compile(loss = [self.__Loss],
                                  optimizer = get_Optimiser(),
                                  metrics = self._BasicModel__Metrics)
        self.__kerasmodel.summary()

    def DPADNNCompile(self):
        """
        Compile a custom connected NN with Four parallel layers including attention.
        Takes simple list of variables as input.
        """
        Input_Layer = Input(shape=(self.__InputDim,))
        Dense_Layer = Dense(50, activation = "relu")(Input_Layer)

        # First parallel arm
        Dense_Layer_1          = Dense(50, activation = "relu")(Dense_Layer)
        Attention_Prob_Layer_1 = Dense(50, activation = "sigmoid")(Dense_Layer_1)
        Attention_Layer_1      = multiply([Dense_Layer_1, Attention_Prob_Layer_1])
        Dropout_Layer_1        = Dropout(0.2)(Attention_Layer_1)
        Dense_Layer_1          = Dense(50, activation = "relu")(Dropout_Layer_1)

        # Second parallel arm
        Dense_Layer_2          = Dense(50, activation = "relu")(Dense_Layer)
        Attention_Prob_Layer_2 = Dense(50, activation = "sigmoid")(Dense_Layer_2)
        Attention_Layer_2      = multiply([Dense_Layer_2, Attention_Prob_Layer_2])
        Dropout_Layer_2        = Dropout(0.2)(Attention_Layer_2)
        Dense_Layer_2          = Dense(50, activation = "relu")(Dropout_Layer_2)

        # Third parallel arm
        Dense_Layer_3          = Dense(50, activation = "relu")(Dense_Layer)
        Attention_Prob_Layer_3 = Dense(50, activation = "sigmoid")(Dense_Layer_3)
        Attention_Layer_3      = multiply([Dense_Layer_3, Attention_Prob_Layer_3])
        Dropout_Layer_3        = Dropout(0.2)(Attention_Layer_3)
        Dense_Layer_3          = Dense(50, activation = "relu")(Dropout_Layer_3)

        # Fourth parallel arm
        Dense_Layer_4          = Dense(50, activation = "relu")(Dense_Layer)
        Attention_Prob_Layer_4 = Dense(50, activation = "sigmoid")(Dense_Layer_4)
        Attention_Layer_4      = multiply([Dense_Layer_4, Attention_Prob_Layer_4])
        Dropout_Layer_4        = Dropout(0.2)(Attention_Layer_4)
        Dense_Layer_4          = Dense(50, activation = "relu")(Dropout_Layer_4)

        # Concatenation Layer
        Concat_Layer  = concatenate([Dense_Layer_1, Dense_Layer_2, Dense_Layer_3, Dense_Layer_4])
        Dense_Layer   = Dense(50, activation='relu')(Concat_Layer)
        Output_Layer  = Dense(self.__OutputSize, activation=self.__OutputActivation,name="Output")(Dense_Layer)
        self.__kerasmodel = keras.Model(inputs=Input_Layer,outputs=Output_Layer)
        self.__kerasmodel.compile(loss = [self.__Loss],
                                  optimizer = self.get_Optimiser(),
                                  metrics = self._BasicModel__Metrics)
        self.__kerasmodel.summary()

    def ResDNNCompile(self):
        """
        Compile a custom connected NN with Four parallel layers including attention.
        Takes simple list of variables as input.
        """
        Input_Layer = Input(shape=(self.__InputDim,))
        Dense_Layer = Dense(50, activation = "relu")(Input_Layer)

        Shortcut    = Dense_Layer
        Dense_Layer = Dense(50, activation = "relu")(Dense_Layer)
        BatchNorm_Layer = BatchNormalization()(Dense_Layer)
        Dense_Layer = Dense(50, activation = "relu")(BatchNorm_Layer)
        Dropout_Layer = Dropout(0.2)(Dense_Layer)
        Add_Layer   = Add()([Dropout_Layer, Shortcut])
        Dense_Layer = Dense(50, activation = "relu")(Add_Layer)

        Shortcut    = Dense_Layer
        Dense_Layer = Dense(50, activation = "relu")(Dense_Layer)
        BatchNorm_Layer = BatchNormalization()(Dense_Layer)
        Dense_Layer = Dense(50, activation = "relu")(BatchNorm_Layer)
        Dropout_Layer = Dropout(0.2)(Dense_Layer)
        Add_Layer   = Add()([Dropout_Layer, Shortcut])
        Dense_Layer = Dense(50, activation = "relu")(Add_Layer)

        Output_Layer  = Dense(self.__OutputSize, activation=self.__OutputActivation,name="Output")(Add_Layer)
        self.__kerasmodel = keras.Model(inputs=Input_Layer,outputs=Output_Layer)
        self.__kerasmodel.compile(loss = [self.__Loss], optimizer = tf.keras.optimizers.Nadam(learning_rate=self.__LearningRate)
, metrics = self._BasicModel__Metrics)
        self.__kerasmodel.summary()

    ########################################################################
    ############################ Model Training ############################
    ########################################################################

    def DNNTrain(self, X, Y, W, output_path, Fold=""):
        """
        Training a simple densely connected NN. This method serves as the "heavy lifter" for training.
        It should work well for not to complicated inputs/outputs.

        Keyword Arguments:
        X           --- Array of data to be used for training
        Y           --- Labels
        W           --- Weights
        output_path --- Output path where the model.h5 files and the history is to be saved
        Fold        --- String declaring the fold name. This will be appended to the modelname upon saving it.
        """
        if Fold!="":
            Fold = "_Fold"+Fold
        # Definition of Callbacks
        callbacks = []
        if self.__UseTensorBoard:
            tb = TensorBoard(log_dir=hf.ensure_trailing_slash(output_path)+self._BasicModel__Name+"_"+self._BasicModel__Type+"_TensorBoardLog"+Fold,
                             histogram_freq=5)
            callbacks.append(tb)
        if self.__Patience is not None and self.__MinDelta is not None:
            es = EarlyStopping(monitor='val_loss', min_delta=self.__MinDelta, patience = self.__Patience, restore_best_weights=True)
            callbacks.append(es)
        seed = np.random.randint(1000, size=1)[0]
        W_train, W_val, _, _ = train_test_split(W, Y, test_size=self._BasicModel__ValidationSize, random_state=seed)
        X_train, X_val, y_train, y_val = train_test_split(X, Y, test_size=self._BasicModel__ValidationSize, random_state=seed)

        TrainerMessage("Starting Training...")
        if len(callbacks)!=0:
            history = self.__kerasmodel.fit(X_train, y_train, epochs=self._BasicModel__Epochs, batch_size=self._BasicModel__BatchSize, verbose=self.__Verbosity, sample_weight=W_train, validation_data = (X_val, y_val, W_val), callbacks = callbacks)
        else:
            history = self.__kerasmodel.fit(X_train, y_train, epochs=self._BasicModel__Epochs, batch_size=self._BasicModel__BatchSize, verbose=self.__Verbosity, sample_weight=W_train, validation_data = (X_val, y_val, W_val))

        TrainerDoneMessage("Training done! Writing model to "+ hf.ensure_trailing_slash(output_path)+self._BasicModel__Name+"_"+self._BasicModel__Type+Fold+".h5")
        self.__kerasmodel.save(hf.ensure_trailing_slash(output_path)+self._BasicModel__Name+"_"+self._BasicModel__Type+Fold+".h5")
        arch = self.__kerasmodel.to_json()
        # save the architecture string to a file somehow, the below will work
        TrainerDoneMessage("Training done! Writing model architecture to "+ hf.ensure_trailing_slash(output_path)+self._BasicModel__Name+"_"+self._BasicModel__Type+Fold+"_Architecture.json")
        with open(hf.ensure_trailing_slash(output_path)+self._BasicModel__Name+"_"+self._BasicModel__Type+Fold+"_Architecture.json", 'w') as arch_file:
            arch_file.write(arch)
        # now save the weights as an HDF5 file
        TrainerDoneMessage("Training done! Writing model weights to "+ hf.ensure_trailing_slash(output_path)+self._BasicModel__Name+"_"+self._BasicModel__Type+Fold+"_Weights.h5")
        self.__kerasmodel.save_weights(hf.ensure_trailing_slash(output_path)+self._BasicModel__Name+"_"+self._BasicModel__Type+Fold+"_Weights.h5")
        TrainerDoneMessage("Writing history to "+ hf.ensure_trailing_slash(output_path)+self._BasicModel__Name+"_"+self._BasicModel__Type+"_history"+Fold+".json")
        hist_df = pd.DataFrame(history.history)
        with open(hf.ensure_trailing_slash(output_path)+self._BasicModel__Name+"_"+self._BasicModel__Type+"_history"+Fold+".json", mode='w') as f:
            hist_df.to_json(f)

    def CompileAndTrain(self, X, Y, W, output_path, Fold=""):
        """
        Combination of compilation and training for a densely connected NN.

        Keyword Arguments:
        X           --- Array of data to be used for training
        Y           --- Labels
        W           --- Weights
        output_path --- Output path where the model.h5 files and the history is to be saved
        Fold        --- String declaring the fold name. This will be appended to the modelname upon saving it.
        """
        if len(self.get_ClassLabels())>2 and self.__Loss == "categorical_crossentropy":
            Y=np.array(hf.OneHot(Y, len(self.get_ClassLabels()))).astype(np.float32)
        else:
            Y=np.array(Y).astype(np.float32)
        if self.__SmoothingAlpha is not None:
            Y=self.smooth_labels(Y,K=self.__OutputSize,alpha=self.__SmoothingAlpha)
        # Pre-designed paralell DNN
        if self.__PredesignedModel=="PDNN":
            self.PDNNCompile()
        # Pre-designed double paralell DNN
        elif self.__PredesignedModel=="DPDNN":
            self.DPDNNCompile()
        # Pre-designed paralell DNN with attention
        elif self.__PredesignedModel=="PADNN":
            self.PADNNCompile()
        # Pre-designed double paralell DNN with attention
        elif self.__PredesignedModel=="DPADNN":
            self.DPADNNCompile()
        # Pre-designed ResDNN
        elif self.__PredesignedModel=="ResDNN":
            self.ResDNNCompile()
        # "Normal" densely connected DNN
        else:
            self.DNNCompile()
        self.DNNTrain(X, Y, W, output_path, Fold)

    ##############################################################################
    ############################ Printing Information ############################
    ##############################################################################

    def __str__(self):
        """
        Method to represent the class objects as a string.
        """
        Nodes = ",".join([str(int) for int in self.__Nodes])
        DropoutIndece = ",".join([str(int) for int in self.__DropoutIndece]) if len(self.__DropoutIndece) != 0 else "None"
        BatchNormIndece = ",".join([str(int) for int in self.__BatchNormIndece]) if len(self.__BatchNormIndece) != 0 else "None"

        return ("MODELINFO:\tName: "+self._BasicModel__Name
                +"\nMODELINFO:\tType: "+self._BasicModel__Type
                +"\nMODELINFO:\tPredesignedModel: "+str(self.__PredesignedModel)
                +"\nMODELINFO:\tNodes: "+Nodes
                +"\nMODELINFO:\tEpochs: "+str(self._BasicModel__Epochs)
                +"\nMODELINFO:\tBatchSize: "+str(self._BasicModel__BatchSize)
                +"\nMODELINFO:\tPatience: "+str(self.__Patience)
                +"\nMODELINFO:\tMinDelta: "+str(self.__MinDelta)
                +"\nMODELINFO:\tDropoutIndece: "+DropoutIndece
                +"\nMODELINFO:\tDropoutProb: "+str(self.__DropoutProb)
                +"\nMODELINFO:\tBatchNormIndece: "+BatchNormIndece
                +"\nMODELINFO:\tOutputSize: "+str(self.__OutputSize)
                +"\nMODELINFO:\tOutputActivation: "+self.__OutputActivation
                +"\nMODELINFO:\tLoss: "+str(self.__Loss)
                +"\nMODELINFO:\tOptimiser: "+str(self.__Optimiser)
                +"\nMODELINFO:\tMetrics: "+str(self._BasicModel__Metrics))

    def Print(self):
        """
        Simple print method.
        """
        self.__kerasmodel.Print()

class DNNOptimisation(object):
    """
    Class handling the optimisation of a DNN
    by defining config files (e.g. for HTCondor)
    """
    def __init__(self, args, cfg_settings, n_iter = 10):
        self.__niter            = n_iter                   # number of models to select for randomized search
        self.__cfgsettings      = cfg_settings
        self.__SeedModel        = cfg_settings.get_Model()
        self.__args             = args
        self.__P                = defaultdict(dict)
        self.__SubNames         = {}

        # Limits for optimisation parameters
        self.__Parameters = {
            "MinLayers"              : self.__SeedModel.get_MinLayers(),
            "MaxLayers"              : self.__SeedModel.get_MaxLayers(),
            "Nodes"                  : range(self.__SeedModel.get_MinNodes(),self.__SeedModel.get_MaxNodes(),self.__SeedModel.get_StepNodes()),
            "SetActivationFunctions" : self.__SeedModel.get_SetActivationFunctions()
            }

    def get_Parameter(self,par):
        """
        Getter method to return parameter par

        Keyword arguments:
        par --- string identifying the parameter to be returned.
        """
        return self.__Parameters[par]

    def Define_ParameterDict(self):
        """
        Function to fill a dictionary with different parameter configurations
        """
        if self.__args.RandomSeed is not None:
            np.random.seed(self.__args.RandomSeed)
        for i in range(self.__niter):
            self.__P["P"+str(i)]["Nodes"] = np.random.choice(self.get_Parameter("Nodes"), np.random.randint(self.get_Parameter("MinLayers"),self.get_Parameter("MaxLayers"),1) , replace = True).tolist()
            N_Layers = len(self.__P["P"+str(i)]["Nodes"])
            Choice_Array  = range(0,N_Layers)
            DropoutSize   = np.random.randint(0,N_Layers,1)
            BatchNormSize = np.random.randint(0,N_Layers,1)
            self.__P["P"+str(i)]["DropoutIndece"]       = sorted(np.random.choice(Choice_Array, DropoutSize, replace = False).tolist())
            self.__P["P"+str(i)]["BatchNormIndece"]     = sorted(np.random.choice(Choice_Array, BatchNormSize, replace = False).tolist())
            self.__P["P"+str(i)]["ActivationFunctions"] = [np.random.choice(self.get_Parameter("SetActivationFunctions")) for Layer in range(N_Layers)]
            if len(self.__P["P"+str(i)]["DropoutIndece"])==0:
                self.__P["P"+str(i)]["DropoutIndece"]=[-1]
            if len(self.__P["P"+str(i)]["BatchNormIndece"])==0:
                self.__P["P"+str(i)]["BatchNormIndece"]=[-1]

    def get_ParameterDict(self):
        """
        Getter method to return the entire parameter dict
        """
        return self.__P

    def Print_ParameterDict(self):
        """
        Method to print the entire parameter dict
        """
        Parameter_df = pd.DataFrame.from_dict(self.get_ParameterDict())
        OptimiserMessage("Resulting parameter configurations:")
        print(Parameter_df.T)

    def Write_Configs(self):
        """
        Method to actually write new config files based
        on a given seed config file.
        """
        SeedGroups = self.__cfgsettings.get_Groups()
        SeedModelName = self.__cfgsettings.get_Model().get_Name()
        for i,group in enumerate(SeedGroups):
            if group[0]=="DNNMODEL":
                ModelGroup = i
        for j,group in enumerate(SeedGroups):
            if group[0]=="GENERAL":
                GeneralGroup=j
        self.Define_ParameterDict()
        self.Print_ParameterDict()
        for key, ParameterDict in self.get_ParameterDict().items():
            hf.create_output_dir(hf.ensure_trailing_slash(self.__args.configpath))
            # for i,item in enumerate(SeedGroups[GeneralGroup]):
            #     if "Job" in item:
            #         SeedGroups[GeneralGroup][i] = SeedGroups[GeneralGroup][i].split("=",1)[0]+"= "+",".join([str(key)])
            for ParameterKey, ParameterValue in ParameterDict.items():
                for i,item in enumerate(SeedGroups[ModelGroup]):
                    if ParameterKey in item and not ("MaxNodes" in item or "MinNodes" in item or "StepNodes" in item):
                        SeedGroups[ModelGroup][i] = SeedGroups[ModelGroup][i].split("=",1)[0]+"= "+",".join([str(val) for val in ParameterValue])
                    if "Name" in item:
                        SeedGroups[ModelGroup][i] = SeedGroups[ModelGroup][i].split("=",1)[0]+"= "+SeedModelName+"_"+key
            f = open(hf.ensure_trailing_slash(self.__args.configpath)+SeedModelName+"_"+key+".cfg", "w")
            OptimiserMessage("Generating config file for %s"%(SeedModelName+"_"+key))
            for group in SeedGroups:
                for j,item in enumerate(group):
                    if j==0:
                        f.write(item+"\n")
                    else:
                        f.write("\t"+item+"\n")
            f.close()

    def Define_Submission(self):
        """
        Method to define submissions files for HTCondor
        """
        hf.create_output_dir(hf.ensure_trailing_slash(self.__args.configpath))
        OptimiserMessage("Generating HTCondor submission file(s) for %s ..."%(self.__cfgsettings.get_General().get_Job()))
        if self.__args.RunOption=="ConversionAndTraining":
            self.Fill_SubFile("Converter",1) # No need to run the converter niter times, once is enough
            self.Fill_SubFile("Trainer",self.__niter)
            OptimiserMessage("Generating HTCondor DAG file for %s ..."%(self.__cfgsettings.get_General().get_Job()))
            f_DAG = open(hf.ensure_trailing_slash(self.__args.configpath)+self.__cfgsettings.get_General().get_Job()+".dag","w")
            f_DAG.write("JOB Conversion %s\n"%(self.__SubNames["Converter"]))
            f_DAG.write("JOB Training %s\n"%(self.__SubNames["Trainer"]))
            f_DAG.write("PARENT Conversion CHILD Training\n")
            f_DAG.close()
        if self.__args.RunOption=="Converter":
            self.Fill_SubFile("Converter",1) # No need to run the converter niter times, once is enough
        if self.__args.RunOption=="Trainer":
            self.Fill_SubFile("Trainer",self.__niter)

    def Fill_SubFile(self, Option, QueueSize):
        """
        Method to fill the submission files with content

        Keyword arguments:
        Option --- String identifying the type of submssion file
        QueueSize --- Integer describing the number of jobs to be submitted
        """
        self.__SubNames[Option] = hf.ensure_trailing_slash(self.__args.configpath)+self.__cfgsettings.get_General().get_Job()+"_"+Option+".sub"
        f = open(self.__SubNames[Option],"w")
        f.write("###################\n")
        f.write("### Job Options ###\n")
        f.write("###################\n")
        f.write("executable\t\t= $ENV(MVA_TRAINER_BASE_DIR)/scripts/HTCondorScripts/HTCondorMain.sh\n")
        f.write("arguments\t\t= "+Option+" $ENV(MVA_TRAINER_BASE_DIR)/"+hf.ensure_trailing_slash(self.__args.configpath)+self.__cfgsettings.get_Model().get_Name()+"_P$(ProcId).cfg\n")
        f.write("request_memory\t\t= 2000\n")
        f.write("+MaxRuntime\t\t= 28800\n") # Setting the maximum run time to 8 hours (because this does not exist as an independent flavour)
        f.write('+JobBatchName\t\t= "'+self.__cfgsettings.get_Model().get_Name()+'"\n')
        f.write("\n")
        f.write("###################\n")
        f.write("### OutputFiles ###\n")
        f.write("###################\n")
        f.write("log\t\t= $ENV(MVA_TRAINER_BASE_DIR)/"+hf.ensure_trailing_slash(self.__args.configpath)+self.__cfgsettings.get_Model().get_Name()+ "_$(ClusterId)_P$(ProcId)"+".log\n")
        f.write("error\t\t= $ENV(MVA_TRAINER_BASE_DIR)/"+hf.ensure_trailing_slash(self.__args.configpath)+self.__cfgsettings.get_Model().get_Name()+ "_$(ClusterId)_P$(ProcId)"+"_error"+".txt\n")
        f.write("output\t\t= $ENV(MVA_TRAINER_BASE_DIR)/"+hf.ensure_trailing_slash(self.__args.configpath)+self.__cfgsettings.get_Model().get_Name()+ "_$(ClusterId)_P$(ProcId)"+"_output"+".txt\n")
        f.write("getenv\t\t= True\n")
        f.write("stream_error\t\t= True\n")
        f.write("stream_output\t\t= True\n")
        f.write('requirements\t\t= OpSysAndVer == "CentOS7"\n')
        f.write("should_transfer_files\t= YES\n")
        f.write("when_to_transfer_output\t= ON_EXIT\n")
        f.write("\n")
        f.write("#####################\n")
        f.write("### Notifications ###\n")
        f.write("#####################\n")
        f.write("#notify_user\t\t= %s@cern.ch\n"%(os.environ.get("USER")))
        f.write("#notification\t\t= always\n")
        f.write("queue "+str(QueueSize))
        f.close()
        OptimiserMessage("Submission file generated: %s"%(self.__SubNames[Option]))

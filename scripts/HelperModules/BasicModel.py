"""
Module to build a basic model from which more complicated model modules inherit.
"""
import tensorflow as tf
import HelperModules.HelperFunctions as hf
from HelperModules.MessageHandler import ErrorMessage, WarningMessage
from HelperModules.OptionChecker import OptionChecker

class BasicModel(tf.keras.Model):
    """
    This class serves as the base class for any machine learning model in the framework.
    All major properties should be defined in here and then passed down via inheritance.
    All sub-classes should implicitly call the constructor of this class.
    """
    def __init__(self, group):
        self.__Group               = group     # String containing all config options for the model
        self.__Name                = None      # Name of the model
        self.__Type                = None      # Type of the model (e.g. "Classification")
        self.__Epochs              = 100       # Maximum training epochs
        self.__BatchSize           = 32        # Batch size
        self.__ValidationSize      = 0.2       # Size of the validation set (0.2=20%)
        self.__Metrics             = None      # Metrics to be evaluated during training (e.g. accuracy)
        self.__ClassLabels         = None      # Labels for multi-class classification
        self.__ModelBinning        = None      # Chosen binning for NN Output plots (e.g. 10,0,1)
        self.__ClassColors         = None      # Colors for each Class for the stacked plots
        self.__SampleFrac          = 1         # Fraction of events used for training
        self.__BinningOptimisation = "None"

        self.__AllowedTypes        = ["Classification-DNN",
                                      "Classification-BDT",
                                      "Classification-Xg-BDT",
                                      "Classification-WGAN",
                                      "Regression-DNN",
                                      "Regression-BDT",
                                      "3LZ-Reconstruction",
                                      "4LZ-Reconstruction"]

        # Making sure tensorflow only grabs as much memory as it needs and not ALL OF IT!
        gpus = tf.config.experimental.list_physical_devices('GPU')
        if gpus:
            try:
                for gpu in gpus:
                    tf.config.experimental.set_memory_growth(gpu, True)
            except RuntimeError as e:
                print(e)
        super(BasicModel, self).__init__()

        #############################################################################
        ############################ Basic Input Options ############################
        #############################################################################
        BasicModel_OS = OptionChecker()
        for item in group:
            BasicModel_OS.CheckBasicModelOption(item)
            if "Name" in item:
                self.__Name = hf.ReadOption(item, "Name", istype="str", islist=False)
            if "Type" in item:
                self.__Type = hf.ReadOption(item, "Type", istype="str", islist=False)
            if "Epochs" in item:
                self.__Epochs = hf.ReadOption(item, "Epochs", istype="int", islist=False, msg="Could not load 'Epochs'. Make sure you pass an integer.")
            if "BatchSize" in item:
                self.__BatchSize = hf.ReadOption(item, "BatchSize", istype="int", islist=False, msg="Could not load 'BatchSize'. Make sure you pass an integer.")
            if "ValidationSize" in item:
                self.__ValidationSize = hf.ReadOption(item, "ValidationSize", istype="float", islist=False, msg="Could not load 'ValidationSize'. Make sure you pass a float between 0 and 1.")
            if "Metrics" in item:
                self.__Metrics = hf.ReadOption(item, "Metrics", istype="str", islist=True)
            if "ClassLabels" in item:
                self.__ClassLabels = hf.ReadOption(item, "ClassLabels", istype="str", islist=True)
            if "ModelBinning" in item:
                self.__ModelBinning = hf.ReadOption(item, "ModelBinning", istype="float", islist=True)
            if "BinningOptimisation" in item:
                self.__BinningOptimisation = hf.ReadOption(item, "BinningOptimisation", istype="str", islist=False)
            if "ClassColors" in item:
                self.__ClassColors = hf.ReadOption(item, "ClassColors", istype = "int", islist=True)
            if "SampleFraction" in item:
                self.__SampleFrac = hf.ReadOption(item, "SampleFraction", istype = "float", islist=False)

        #############################################################################
        ############################ Check Input Options ############################
        #############################################################################
        if self.__Name is None:
            ErrorMessage("Model with missing 'Name' attribute detected!.")
        if self.__Type is None:
            ErrorMessage("MODEL " + self.__Name + " has no 'Type' set.")
        elif self.__Type not in self.__AllowedTypes:
            ErrorMessage("MODEL " + self.__Name + " has unknown type " + self.__Type +". 'Type' can be 'Classification-DNN','Classification-BDT', 'Classification-Xg-BDT', 'Classification-WGAN', 'Regression-DNN', '3LZ-Reconstruction' or '4LZ-Reconstruction'.")
        if self.__Epochs<=10:
            WarningMessage("MODEL " + self.__Name + " has 'Epochs' set to "+str(self.__Epochs)+". This is a very low number. The model might not converge.")
        if self.__ValidationSize>=1 or self.__ValidationSize<=0:
            ErrorMessage("MODEL " + self.__Name + " has 'ValidationSize' set to "+str(self.__ValidationSize)+". 'MinDelta' must be in (0,1).")
        if self.__ClassLabels is None:
            ErrorMessage("MODEL " + self.__Name + " has no 'ClassLabels' defined!")
        if self.__ClassLabels is not None: # If we are in a multi-class scenario we expect multiple custom binnings or have to set multiple defaults.
            if self.__ModelBinning is None:
                self.__ModelBinning = [[20,0,1] for nClassLabels in range(len(self.__ClassLabels))]
            elif len(self.__ClassLabels)*3!=len(self.__ModelBinning):
                ErrorMessage("MODEL " + self.__Name + " 'ModelBinning' must be list of values using this format: nbins, x_low and must correspond to the number of given 'ClassLabels'. A binning must be defined for each multi-class classifier.")
            else:
                self.__ModelBinning = hf.chunks(self.__ModelBinning,3)
        else: # If we are in a binary scenario we expect one custom binning.
            if self.__ModelBinning is None:
                self.__ModelBinning = [[20,0,1]]
            elif len(self.__ModelBinning)!=3:
                ErrorMessage("MODEL " + self.__Name + " 'ModelBinning' must be list of values using this format: nbins, x_low, x_high.")
        if self.__BinningOptimisation not in ["None","TransfoD_symmetric","TransfoD_flatS","TransfoD_flatB"]:
            ErrorMessage("MODEL " + self.__Name + " 'ModelBinning' must be 'None', 'TransfoD_flatS', TransfoD_flatB' or 'TransfoD_symmetric'!")

    ######################################################################
    ############################ Misc Methods ############################
    ######################################################################
    def smooth_labels(self, Y, K, alpha=0.1):
        '''
        Function to smooth labels as presented in arXiv:1906.02629 and arXiv:1512.00567

        Keyword Arguments:
        Y     --- Labels (list)
        alpha --- Smoothing parameter (should be between 0 and < 0.5)
        '''
        return Y*(1-alpha)+alpha/K

    def get_Name(self):
        """
        Getter method to return the name of the model
        """
        return self.__Name

    def get_Type(self):
        """
        Getter method to return the type of the model
        """
        return self.__Type

    def get_Epochs(self):
        """
        Getter method to return the epochs
        """
        return self.__Epochs

    def get_BatchSize(self):
        """
        Getter method to return the batch size
        """
        return self.__BatchSize

    def get_ValidationSize(self):
        """
        Getter method to return the sie of the validation set
        """
        return self.__ValidationSize

    def get_ClassLabels(self):
        """
        Getter method to return the class labels
        """
        return self.__ClassLabels

    def get_Metrics(self):
        """
        Getter method to return the metrics (strings)
        """
        return self.__Metrics

    def get_ModelBinning(self):
        """
        Getter method to return the chosen model binnings
        """
        return self.__ModelBinning

    def get_BinningOptimisation(self):
        """
        Getter method to return the name of the chosen binning optimisation
        """
        return self.__BinningOptimisation

    def get_ClassColors(self):
        """
        Getter method to return the colours of the classes
        """
        return self.__ClassColors

    def get_SampleFrac(self):
        """
        Getter method to return fraction of the sample
        """
        return self.__SampleFrac

    def isClassification(self):
        """
        Defines whether a model is a classification model
        """
        return self.isClassificationDNN() or self.isClassificationBDT() or self.isClassificationWGAN() or self.isClassificationXgBDT()

    def isClassificationDNN(self):
        """
        Defines whether a model is a classification model (DNN)
        """
        return self.__Type=="Classification-DNN"

    def isClassificationBDT(self):
        """
        Defines whether a model is a classification model (BDT)
        """
        return self.__Type=="Classification-BDT"
    
    def isClassificationXgBDT(self):
        """
        Defines whether a model is a classification model with xgboost (BDT)
        """
        return self.__Type=="Classification-Xg-BDT"

    def isClassificationWGAN(self):
        """
        Defines whether a model is a classification model (WGAN)
        """
        return self.__Type=="Classification-WGAN"

    def isRegressionDNN(self):
        """
        Defines whether a model is a regression model (DNN)
        """
        return self.__Type=="Regression-DNN"

    def isRegressionBDT(self):
        """
        Defines whether a model is a regression model (BDT)
        """
        return self.__Type=="Regression-BDT"

    def isRegression(self):
        """
        Defines whether a model is a regression model
        """
        return self.isRegressionBDT() or self.isRegressionDNN()

    def is3LZReconstruction(self):
        """
        Defines whether a model is a model for 3LZ reconstruction
        """
        return self.__Type=="3LZ-Reconstruction"

    def is4LZReconstruction(self):
        """
        Defines whether a model is a model for 4LZ reconstruction
        """
        return self.__Type=="4LZ-Reconstruction"

    def isReconstruction(self):
        """
        Defines whether a model is a reconstruction model
        """
        return self.is3LZReconstruction() or self.is4LZReconstruction()

    def get_nLeps(self):
        """
        Method to return the number of expected leptons
        """
        if self.is3LZReconstruction():
            return 3
        if self.is4LZReconstruction():
            return 4
        return 0

    ##############################################################################
    ############################ Printing Information ############################
    ##############################################################################

    def __str__(self):
        """
        Method to represent the class objects as a very basic string.
        """
        return ("MODELINFO:\tName: "+self.__Name
                +"\nMODELINFO:\tType: "+self.__Type
                +"\nMODELINFO:\tEpochs: "+str(self.__Epochs)
                +"\nMODELINFO:\tBatchSize: "+str(self.__BatchSize)
                +"\nMODELINFO:\tMetrics: "+str(self.__Metrics))

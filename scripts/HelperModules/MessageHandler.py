"""
Module to handle messages on the console.
"""

import os, sys

class fontcolors:
    """This class defines fonts and colors for a nicer output on the terminal.

    Available Colors:
    HEADER, OKBLUE, OKCYAN, OKGREEN, WARNING, FAIL, ENDC, BOLD, UNDERLINE.
    """
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKCYAN = '\033[96m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'

def InfoDelimiter():
    """
    A simple delimiter
    """
    print("##############################################")

def InfoMessage(text):
    """
    An info message
    """
    print("INFO:\t" + text)

def InfoDoneMessage(text):
    """
    An info message in green
    """
    print(fontcolors.OKGREEN + "INFO:\t" + text + fontcolors.ENDC)

def PostProcessorMessage(text):
    """
    Post processor message
    """
    print("POSTPROCESSOR:\t" + text)

def ConverterMessage(text):
    """
    Converter message
    """
    print("CONVERTER:\t" + text)

def ConverterDoneMessage(text):
    """
    Converter message in green
    """
    print(fontcolors.OKGREEN + "CONVERTER:\t" + text + fontcolors.ENDC)

def TrainerMessage(text):
    """
    Trainer message
    """
    print("TRAINER:\t" + text)

def TrainerDoneMessage(text):
    """
    Trainer message in green
    """
    print(fontcolors.OKGREEN + "TRAINER:\t" + text + fontcolors.ENDC)

def InjectorMessage(text):
    """
    Injector message
    """
    print("MVAINJECTOR:\t" + text)

def InjectorDoneMessage(text):
    """
    Injector message in green
    """
    print(fontcolors.OKGREEN + "MVAINJECTOR:\t" + text + fontcolors.ENDC)

def OptimiserMessage(text):
    """
    Optimiser message
    """
    print("Optimiser:\t" + text)

def ErrorMessage(text):
    """
    Error message
    """
    print(fontcolors.FAIL + "ERROR:\t" + text +fontcolors.ENDC)
    print(fontcolors.FAIL + "ERROR:\tExiting!" +fontcolors.ENDC)
    sys.exit(-1)

def WarningMessage(text):
    """
    Warning message
    """
    print(fontcolors.WARNING + "WARNING:\t" + text +fontcolors.ENDC)

def WelcomeMessage(option="Trainer"):
    """
    Different welcome messages
    """
    if option == "Trainer":
        print(fontcolors.HEADER + "###################################################################" +fontcolors.ENDC)
        print(fontcolors.HEADER + "########################## MVA - TRAINER ##########################" +fontcolors.ENDC)
        print(fontcolors.HEADER + "###################################################################" +fontcolors.ENDC)
    elif option == "Converter":
        print(fontcolors.HEADER + "###################################################################" +fontcolors.ENDC)
        print(fontcolors.HEADER + "######################### MVA - Converter #########################" +fontcolors.ENDC)
        print(fontcolors.HEADER + "###################################################################" +fontcolors.ENDC)
    elif option == "Evaluate":
        print(fontcolors.HEADER + "###################################################################" +fontcolors.ENDC)
        print(fontcolors.HEADER + "######################### MVA - Evaluater #########################" +fontcolors.ENDC)
        print(fontcolors.HEADER + "###################################################################" +fontcolors.ENDC)
    elif option == "Injector":
        print(fontcolors.HEADER + "###################################################################" +fontcolors.ENDC)
        print(fontcolors.HEADER + "######################### MVA - Injector ##########################" +fontcolors.ENDC)
        print(fontcolors.HEADER + "###################################################################" +fontcolors.ENDC)
    elif option == "Optimiser":
        print(fontcolors.HEADER + "###################################################################" +fontcolors.ENDC)
        print(fontcolors.HEADER + "######################### MVA - Optimiser #########################" +fontcolors.ENDC)
        print(fontcolors.HEADER + "###################################################################" +fontcolors.ENDC)

def EnvironmentalCheck(check="all"):
    """
    Function to check whether environmental
    variables were set correctly.
    """
    if check in ["all", "env"] and os.environ.get("NTUPLE_LOCATION") is None:
        ErrorMessage("NTUPLE_LOCATION undefined. Did you forget to source the setup script?")
    if check in ["all", "dir"] and not os.path.isdir(os.environ["NTUPLE_LOCATION"]+os.environ["NTUPLE_VERSION"]+"/"):
        ErrorMessage(os.environ["NTUPLE_LOCATION"]+os.environ["NTUPLE_VERSION"]+"/ does not seem to exist. You need to check this!")

"""
This module introduces a class handling the conversion of root files.
"""

import glob, os, ROOT
import pickle
pickle.HIGHEST_PROTOCOL = 4
import pandas as pd
import numpy as np
import uproot
import HelperModules.HelperFunctions as hf
from HelperModules.EventReco.ttXReco import ttXEventVariables, ttXEvent
from HelperModules.Settings import Settings
from HelperModules.Directories import Directories
from HelperModules.MessageHandler import ConverterMessage, ConverterDoneMessage, ErrorMessage, WarningMessage, WelcomeMessage, EnvironmentalCheck
from PlotterModules.CtrlPlotHandler import CtrlPlotHandler
from shutil import copyfile

class ConversionHandler:
    """
    This class handles the conversion for all models.
    """
    def __init__(self, args):
        EnvironmentalCheck()
        WelcomeMessage("Converter")
        ROOT.gErrorIgnoreLevel = ROOT.kWarning
        cfg_Settings        = Settings(args.configfile, option="all")
        self.__cfg_Settings = cfg_Settings
        self.__Job          = cfg_Settings.get_General().get_Job()
        self.__Selection    = cfg_Settings.get_General().get_Selection()
        self.__Variables    = cfg_Settings.get_Variables()
        self.__VarNames     = [var.get_Name() for var in self.__Variables]+["eventNumber"]
        self.__Treename     = cfg_Settings.get_General().get_Treename()
        self.__MCWeight     = cfg_Settings.get_General().get_MCWeight()
        self.__Model        = cfg_Settings.get_Model()
        self.__Samples      = cfg_Settings.get_Samples()
        self.__df_X_all     = None
        self.__df_Y_all     = None
        self.__df_W_all     = None
        self.__df_N_all     = None
        self.__df_T_all     = None
        self.__df_isNominal_all = None

        self.__SampleTreeName     = None
        self.__SampleSelection    = None
        self.__SampleMCWeight     = None
        self.__SanitizedVariables = None
        self.__SampleFilePaths    = []
        self.__nEmptyFrames       = 0
        self.__nEmptySamples      = 0

        self.__NTUPLELOCATION = os.environ["NTUPLE_LOCATION"]+os.environ["NTUPLE_VERSION"]+"/"
        self.__Dirs = Directories(self.__Job)
        self.__DoCtrlPlots = cfg_Settings.get_General().do_CtrlPlots()
        # Copy the config file to the job directory
        copyfile(args.configfile,
                 hf.ensure_trailing_slash(self.__Dirs.ConfigDir())+os.path.basename(args.configfile).replace(".cfg","_conversion_step.cfg"))

    def is_Empty(self, Sample):
        """
        Method to check whether no events from a Sample passed selection.

        Keyword arguments:
        Sample --- Sample object
        """
        if self.__SampleFilePaths == self.__nEmptyFrames:
            WarningMessage("None of the ntuple files for %s contribute events passing the selection %s"%(Sample.get_Name(),self.__SampleSelection))
            self.__nEmptySamples += 1
            return True
        self.__nEmptyFrames = 0
        return False

    def Check_LogicStrings(self):
        """
        Method to check whether the logic operators in string are valid,
        whether parenthesis are matched and whether string is python conform.
        """
        # Checking for matched brackets
        if not hf.matched(self.__SampleSelection):
            ErrorMessage("Sample selection has unmatched brackets! %s"%(self.__SampleSelection))
        if not hf.matched(self.__SampleMCWeight):
            ErrorMessage("Sample selection has unmatched brackets! %s"%(self.__SampleMCWeight))
        if any([item in self.__SampleSelection for item in ["@","->","^"]]):
            ErrorMessage("Found c++ style character in selection! %s"%(self.__SampleSelection))
        if any([item in self.__MCWeight for item in ["@","->","^"]]):
            ErrorMessage("Found c++ style character in weight!"%(self.__MCWeight))
        if "&&" in self.__SampleSelection:
            ErrorMessage("Found c++ style '&&' in combined selection string. For a proper conversion please use (statement A)&(statement B) instead.")
        if "||" in self.__SampleSelection:
            ErrorMessage("Found c++ style '||' in combined selection string. For a proper conversion please use (statement A)|(statement B) instead.")
        if "&&" in self.__SampleMCWeight:
            ErrorMessage("Found c++ style '&&' in combined MCWeight string. For a proper conversion please use (statement A)&(statement B) instead.")
        if "||" in self.__SampleMCWeight:
            ErrorMessage("Found c++ style '||' in combined MCWeight string. For a proper conversion please use (statement A)|(statement B) instead.")

    def set_SampleMCWeight(self, Sample):
        '''
        Function combine Sample and general MCWeight in case a sample-specific MCWeight is given

        Keyword arguments:
        Sample --- Sample object to be checked for MCWeight
        '''
        if Sample.get_MCWeight()!="":
            self.__SampleMCWeight = self.__MCWeight+"*"+Sample.get_MCWeight()
        else:
            self.__SampleMCWeight = self.__MCWeight

    def set_SampleSelection(self, Sample):
        '''
        Function combine Sample and general selection in case a sample-specific selection is given

        Keyword arguments:
        Sample --- Sample object to be checked for selection string
        '''
        if Sample.get_Selection()!="":
            self.__SampleSelection = self.__Selection+"&"+"("+Sample.get_Selection()+")"
        else:
            self.__SampleSelection = self.__Selection

    def InitialiseConversion(self, Sample):
        '''
        Function to initialise several different variables and objects that are needed
        for the conversion of ntuples from individual samples.

        Keyword arguments:
        Sample -- Sample object
        '''
        ConverterMessage("Processing "+Sample.get_Name()+" ("+Sample.get_Type()+")")
        ### Resetting DataFrames ###
        self.__df_X_all     = None
        self.__df_Y_all     = None
        self.__df_W_all     = None
        self.__df_N_all     = None
        self.__df_T_all     = None
        self.__df_isNominal_all = None
        ### Resetting some Variables ###
        if Sample.get_TreeName() is not None:
            self.__SampleTreename=Sample.get_TreeName()
        else:
            self.__SampleTreename=self.__Treename
        ### setting sample MC weight and selection ###
        self.set_SampleMCWeight(Sample)
        self.set_SampleSelection(Sample)
        self.Check_LogicStrings()
        ### sanitize the variablle inputs ###
        self.__SanitizedVariables = hf.sanitize_root(self.__VarNames)
        ### get list of filepaths ###
        self.__SampleFilePaths = []
        for FileString in Sample.get_NtupleFiles():
            self.__SampleFilePaths.extend(glob.glob(self.__NTUPLELOCATION+FileString))

    def PerformConversion(self):
        '''
        Main function to perform the conversion. It checks the type of the model and subsequently
        calls the corresponding conversion method to perform the conversion.
        Afterwards it merges the created .h5 datasets and (optionally) creates control plots.
        '''
        if self.__Model.isClassification():
            self.do_Classification_Conversion()
        elif self.__Model.isReconstruction():
            self.do_Reconstruction_Conversion()
        elif self.__Model.isRegression():
            self.do_Regression_Conversion()
        self.MergeDatasets()
        if self.__DoCtrlPlots:
            CtrlPlotHandler(self.__cfg_Settings).do_CtrlPlots()

    def FinaliseConversion(self, Sample):
        '''
        Function to finalise the conversion by concatenating the pandas dataframes corresponding to
        individual folds. The user is notified at the end.

        Keyword arguments:
        Sample -- Sample object
        '''
        # Adding all temporary lists together
        self.__df_X_all=self.__df_X_all.reset_index(drop=True)
        self.__df_Y_all=self.__df_Y_all.reset_index(drop=True)
        self.__df_W_all=self.__df_W_all.reset_index(drop=True)
        self.__df_N_all=self.__df_N_all.reset_index(drop=True)
        self.__df_T_all=self.__df_T_all.reset_index(drop=True)
        self.__df_isNominal_all=self.__df_isNominal_all.reset_index(drop=True)
        df_all = pd.concat([self.__df_X_all,
                            self.__df_Y_all,
                            self.__df_W_all,
                            self.__df_N_all,
                            self.__df_T_all,
                            self.__df_isNominal_all],
                           axis=1,
                           sort=False)
        df_all.to_hdf(hf.ensure_trailing_slash(self.__Dirs.DataDir())+Sample.get_Name()+".h5", key="df", mode="w")
        ConverterDoneMessage("Processing "+Sample.get_Name()+" ("+Sample.get_Type()+") DONE!")

    def MergeDatasets(self):
        '''
        Simple function to merge the .h5 datasets which were created together into one big dataset containing everything.
        '''
        if self.__nEmptySamples == len(self.__Samples):
            ErrorMessage("None of the given Samples has events passing the respective selection! You need to check this!")
        ConverterMessage("Merging datasets...")
        dfs = [pd.read_hdf(fname) for fname in [hf.ensure_trailing_slash(self.__Dirs.DataDir())+Sample.get_Name()+".h5" for Sample in self.__Samples]]
        merged = pd.concat(dfs)
        # Shuffle the merged dataframes in place and reset index. Use random_state for reproducibility
        seed = np.random.randint(1000, size=1)[0]
        merged = merged.sample(frac=1, random_state = seed).reset_index(drop=True)
        merged.to_hdf(hf.ensure_trailing_slash(self.__Dirs.DataDir())+"merged.h5", key="df", mode="w")
        ConverterDoneMessage("Merged hdf5 file (data) written to "+hf.ensure_trailing_slash(self.__Dirs.DataDir())+"merged.h5")

    def get_X(self, Filepath, Treename, Variables, Selection):
        '''
        Function to extract the feature dataset or a specific sample and filepath using
        arrays from the uproot package.

        Keyword Arguments:
        Filepath --- String representing the file path of the .root file to be converted
        Treename --- String representing the name of the tree
        Variables --- List (string) of variables
        Selection --- String representing the selection
        '''
        try:
            X_Uncut = uproot.open(Filepath+":"+Treename)
        except:
            ErrorMessage("Could not open %s tree in %s"%(Treename,Filepath))

        try:
            if Selection=="":
                X = X_Uncut.arrays(Variables, library="pd")
            else:
                X = X_Uncut.arrays(Variables, Selection, library="pd")
                X.reset_index(drop=True)
        except KeyError as e:
            ErrorMessage("The following variable is missing %s : %s"%(Filepath+":"+Treename,e.args[0]))
        except Exception as e:
            ErrorMessage("Could not convert %s. %s!"%(Filepath,e))
        # By default, vector-elements not found in ntuples are filled with
        # np.nan, which results in an error
        # TODO: Add fill-value support as an advanced option
        if len(X) == 0:
            WarningMessage("No events added from %s"%Filepath)
            self.__nEmptyFrames+=1
            return None
        elif len(X) < 50:
            WarningMessage("Less than 50 events added from %s"%Filepath)
        # By default, vector-elements not found in ntuples are filled with
        # np.nan, which results in an error
        # TODO: Add fill-value support as an advanced option
        for variable in Variables:
            if X[variable].isnull().values.any():
                ErrorMessage("Variable %s has nan entries!"%variable)
        return X

    def get_W(self,Filepath, Sample, DefaultLength, returnEventNumbers=False):
        '''
        Function to extract the weights for a specific sample using
        arrays from the uproot package. DefaultLength is used to
        define the weight vector in case we are handling data.

        Keyword Arguments:
        Sample        --- Sample object
        DefaultLength --- Integer value defining the default length of the weight vector
        '''

        W_Uncut = uproot.open(Filepath+":"+self.__SampleTreename)
        try:
            if self.__MCWeight == "" or self.__MCWeight == "1" or Sample.get_Type()=="Data":
                W_tmp = [1]*DefaultLength
                W = pd.DataFrame(data=np.array(W_tmp),columns=["Weight"])
                if returnEventNumbers:
                    if self.__SampleSelection=="":
                        eventNumbers = W_Uncut.arrays(["eventNumber"], library="pd").values
                    else:
                        eventNumbers = W_Uncut.arrays(["eventNumber"], self.__SampleSelection, library="pd").values
                    W["eventNumber"] = eventNumbers
            elif self.__SampleSelection=="":
                W = W_Uncut.arrays(self.__SampleMCWeight, library="pd")
                if returnEventNumbers:
                    eventNumbers = W_Uncut.arrays(["eventNumber"], library="pd").values
                    W["eventNumber"] = eventNumbers
                W = W.rename(columns={self.__SampleMCWeight: "Weight"})
            else:
                W = W_Uncut.arrays(self.__SampleMCWeight, self.__SampleSelection, library="pd")
                if returnEventNumbers:
                    eventNumbers = W_Uncut.arrays(["eventNumber"], self.__SampleSelection, library="pd").values
                    W["eventNumber"] = eventNumbers
                W = W.rename(columns={self.__SampleMCWeight: "Weight"})
        except Exception as e:
            ErrorMessage("Could not convert %s! %s"%(Filepath,e))
        return W

    def get_N(self, Sample, DefaultLength):
        '''
        Function to extract the names for a specific sample using
        arrays from the uproot package. DefaultLength is used to
        define the length of the name list.

        Keyword Arguments:
        Sample        --- Sample object
        DefaultLength --- Integer value defining the default length of the name list
        '''
        if Sample.get_Group() is not None:
            N = [Sample.get_Group()]*DefaultLength
        else:
            N = [Sample.get_Name()]*DefaultLength
        return N

    def get_isNominal(self, Sample, DefaultLength):
        '''
        Function to extract whether a sample is a nominal one or not
        for a specific sample using arrays from the uproot package.
        DefaultLength is used to define the length of the list in case
        we are handling data.

        Keyword Arguments:
        Sample        --- Sample object
        DefaultLength --- Integer value defining the default length of the isNominal list
        '''
        if Sample.get_Type() == "Data":
            isNominal = [True]*DefaultLength
        if Sample.get_Type() == "Systematic":
            isNominal = [False]*DefaultLength
        else:
            isNominal = [True]*DefaultLength
        return isNominal

    def do_Classification_Conversion(self):
        '''
        Function to perform the conversion for a standard classification case.
        '''
        for Sample in self.__Samples:
            self.InitialiseConversion(Sample)
            df_X_list, df_Y_list, df_W_list, df_N_list, df_T_list, df_isNominal_list = ([] for i in range(6))
            for Filepath in self.__SampleFilePaths:
                X = self.get_X(Filepath=Filepath,
                               Treename=self.__SampleTreename,
                               Variables=self.__SanitizedVariables,
                               Selection=self.__SampleSelection)
                if X is None:
                    continue
                DefaultLength = len(X)
                Y = [Sample.get_TrainLabel()]*len(X)
                W = self.get_W(Filepath=Filepath, Sample=Sample, DefaultLength=DefaultLength)
                N = self.get_N(Sample=Sample, DefaultLength=DefaultLength)
                isNominal = self.get_isNominal(Sample=Sample, DefaultLength=DefaultLength)
                T = [Sample.get_Type()]*len(X)
                df_X_list.append(X)
                df_Y_list.append(pd.DataFrame(data = np.array(Y), columns = ["Label"]))
                df_W_list.append(W)
                df_N_list.append(pd.DataFrame(data = np.array(N), columns = ["Sample_Name"]))
                df_T_list.append(pd.DataFrame(data = np.array(T), columns = ["Sample_Type"]))
                df_isNominal_list.append(pd.DataFrame(data = np.array(isNominal), columns = ["isNominal"]))
            if not self.is_Empty(Sample):
                self.__df_X_all = pd.concat(df_X_list)
                self.__df_Y_all = pd.concat(df_Y_list)
                self.__df_W_all = pd.concat(df_W_list)
                self.__df_N_all = pd.concat(df_N_list)
                self.__df_T_all = pd.concat(df_T_list)
                self.__df_isNominal_all = pd.concat(df_isNominal_list)
                self.FinaliseConversion(Sample)
            else:
                continue

    def do_Reconstruction_Conversion(self):
        '''
        Function to perform the conversion for a standard reconstruction case.
        '''
        EventVariables = ttXEventVariables()
        if self.__Model.is3LZReconstruction():
            nLeps = 3
            RecoVariables = EventVariables.Reco3L()
        if self.__Model.is4LZReconstruction():
            nLeps = 4
            RecoVariables = EventVariables.Reco4L()
        TruthVariables = EventVariables.Truth()
        for Sample in self.__Samples:
            self.InitialiseConversion(Sample)
            # Producing a list of files. Since event numbers are not unique between samples we need to iterate over all files
            df_X_list, df_Y_list, df_W_list, df_N_list, df_T_list, df_isNominal_list = ([] for i in range(6))
            for Filepath in self.__SampleFilePaths:
                X = self.get_X(Filepath=Filepath,
                               Treename=self.__SampleTreename,
                               Variables=self.__SanitizedVariables,
                               Selection=self.__SampleSelection)
                if X is None:
                    continue
                df_X = pd.DataFrame(X)
                if Sample.get_Type()=="Data":
                    Y = [-1]*len(X)
                else:
                    Y = []
                X_df_RecoVariables = self.get_X(Filepath=Filepath,
                                                Treename=self.__SampleTreename,
                                                Variables=RecoVariables,
                                                Selection=self.__SampleSelection)
                X_df_TruthVariables = self.get_X(Filepath=Filepath,
                                                 Treename="truth",
                                                 Variables=TruthVariables,
                                                 Selection="")
                X_merged = pd.merge(X_df_TruthVariables, X_df_RecoVariables, how = "inner", on=["eventNumber"])
                df_X = pd.merge(X_df_TruthVariables, df_X, how = "inner", on=["eventNumber"])
                Y = []
                for _, EventKinematics in X_merged.iterrows():
                    EventKinematicsDict = EventKinematics.to_dict()
                    e = ttXEvent(EventKinematicsDict, nLeps)
                    # Z-Reconstruction
                    Y.append(e.get_ZIndece_1D())
                    #OR using Labels: Approach Using the invariant mass works/works partly/does nor work
                    #Y.append(e.get_InvMassClasses(Nlep))
                    # Add more reconstruction methods here
                if Sample.get_Type()=="Data":
                    W = [1]*len(X_merged)
                else:
                    # We have to load eventNumber and MCWeight separate from one another or MCWeight's type is evaluated wrongly
                    DefaultLength=len(X_merged)
                    W_df = self.get_W(Filepath=Filepath, Sample=Sample, DefaultLength=DefaultLength,returnEventNumbers=True)
                    W = pd.merge(X_df_TruthVariables, W_df, how = "inner", on=["eventNumber"])["Weight"].values
                DefaultLength = len(X_merged)
                N = self.get_N(Sample=Sample, DefaultLength=DefaultLength)
                T = [Sample.get_Type()]*DefaultLength
                isNominal = self.get_isNominal(Sample=Sample, DefaultLength=DefaultLength)
                removeTruth = TruthVariables[:-1]
                df_X = df_X.drop(removeTruth, axis = 1) # Getting rid of truth variables again (except evenNumber which is the last item)
                df_X_list.append(df_X)
                df_Y_list.append(pd.DataFrame(data = np.array(Y), columns = ["Label"]))
                df_W_list.append(pd.DataFrame(data = np.array(W), columns = ["Weight"]))
                df_N_list.append(pd.DataFrame(data = np.array(N), columns = ["Sample_Name"]))
                df_T_list.append(pd.DataFrame(data = np.array(T), columns = ["Sample_Type"]))
                df_isNominal_list.append(pd.DataFrame(data = np.array(isNominal), columns = ["isNominal"]))
            self.__df_X_all = pd.concat(df_X_list)
            self.__df_Y_all = pd.concat(df_Y_list)
            self.__df_W_all = pd.concat(df_W_list)
            self.__df_N_all = pd.concat(df_N_list)
            self.__df_T_all = pd.concat(df_T_list)
            self.__df_isNominal_all = pd.concat(df_isNominal_list)
            self.FinaliseConversion(Sample)

    def do_Regression_Conversion(self):
        """
        Performing the conversion for a regression model.
        """
        for Sample in self.__Samples:
            self.InitialiseConversion(Sample)
            # Producing a list of files. Since event numbers are not unique between samples we need to iterate over all files
            df_X_list, df_Y_list, df_W_list, df_N_list, df_T_list, df_isNominal_list = ([] for i in range(6))
            for Filepath in self.__SampleFilePaths:
                df_X = self.get_X(Filepath=Filepath,
                                  Treename=self.__SampleTreename,
                                  Variables=self.__SanitizedVariables,
                                  Selection=self.__SampleSelection)
                if df_X is None:
                    continue
                # for Y we use get_X but just ask for other variables in the truth tree
                df_Y = self.get_X(Filepath=Filepath,
                                  Treename="truth",
                                  Variables=[self.__cfg_Settings.get_Model().get_RegressionTarget(),"eventNumber"],
                                  Selection="")
                df_Y[self.__cfg_Settings.get_Model().get_RegressionTarget()]*=self.__cfg_Settings.get_Model().get_RegressionScale()
                df_Y = df_Y.rename(columns={self.__cfg_Settings.get_Model().get_RegressionTarget(): "Label"})
                df_X_merged = pd.merge(df_X, df_Y, how = "inner", on=["eventNumber"])
                DefaultLength=len(df_X_merged)
                df_W = self.get_W(Filepath=Filepath, Sample=Sample, DefaultLength=DefaultLength,returnEventNumbers=True)
                df_W_merged = pd.merge(df_Y, df_W, how = "inner", on=["eventNumber"])
                N = self.get_N(Sample=Sample, DefaultLength=DefaultLength)
                isNominal = self.get_isNominal(Sample=Sample, DefaultLength=DefaultLength)
                T = [Sample.get_Type()]*DefaultLength
                df_X_list.append(df_X_merged[self.__VarNames])
                df_Y_list.append(df_X_merged["Label"])
                df_W_list.append(df_W_merged["Weight"])
                df_N_list.append(pd.DataFrame(data = np.array(N), columns = ["Sample_Name"]))
                df_T_list.append(pd.DataFrame(data = np.array(T), columns = ["Sample_Type"]))
                df_isNominal_list.append(pd.DataFrame(data = np.array(isNominal), columns = ["isNominal"]))
            self.__df_X_all = pd.concat(df_X_list)
            self.__df_Y_all = pd.concat(df_Y_list)
            self.__df_W_all = pd.concat(df_W_list)
            self.__df_N_all = pd.concat(df_N_list)
            self.__df_T_all = pd.concat(df_T_list)
            self.__df_isNominal_all = pd.concat(df_isNominal_list)
            self.FinaliseConversion(Sample)

    def get_FullSampleMCWeight(self, Sample):
        '''
        Function combine Sample and general MCWeight in case a sample-specific MCWeight is given

        Keyword arguments:
        Sample --- Sample object to be checked for MCWeight
        '''
        if Sample.get_MCWeight()!="":
            return self.__MCWeight+"*"+Sample.get_MCWeight()
        return self.__MCWeight

    def get_FullSampleSelection(self, Sample):
        '''
        Function combine Sample and general selection in case a sample-specific selection is given

        Keyword arguments:
        Selection --- Selection string to be checked
        '''
        if Sample.get_Selection()!="":
            return self.__Selection+"&&"+"("+Sample.get_Selection()+")"
        return self.__Selection

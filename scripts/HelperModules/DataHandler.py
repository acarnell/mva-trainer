import copy, json
import numpy as np
import pandas as pd
from pickle import load
import HelperModules.HelperFunctions as hf
from HelperModules.Directories import Directories
from HelperModules.Preprocessing import Preprocess, ScaleInputs
from HelperModules.MessageHandler import ErrorMessage
from sklearn.model_selection import train_test_split

#pd.options.mode.chained_assignment = None

class DataHandler():
    def __init__(self,Settings):
        ###################################################################
        ############################ Constants ############################
        ###################################################################
        self.__WEIGHTNAME     = "Weight"                               # Name of weight column in df
        self.__PENALTYNAME    = "PenaltyFactor"                        # Name of penalty factor column in df
        self.__LABELNAME      = "Label"                                # Name of label column in df
        self.__SAMPLENAME     = "Sample_Name"                          # Name of sample name column in df
        self.__SAMPLETYPE     = "Sample_Type"                          # Name of sample type column in df
        self.__PREDICTIONNAME = "Prediction"                           # Name of prediction column in df
        self.__FOLDIDENTIFIER = "eventNumber"                          # Name of fold identifier column in df
        self.__ISNOMINAL      = "isNominal"                            # Name of the identifier defining the nominal tree
                        

        ###################################################################
        ####################### Settings variables ########################
        ###################################################################
        self.__Samples         = Settings.get_Samples()                 # List of all samples
        self.__NominalSamples  = Settings.get_NominalSamples()          # List of nominal samples
        self.__SystSamples     = Settings.get_SystSamples()             # List of sytematic samples
        self.__OutputPath      = Settings.get_General().get_Job()       # Path to the 'JOB' directory
        self.__nFolds          = Settings.get_General().get_Folds()     # Number of folds
        self.__VarObjects      = Settings.get_Variables()               # List of variable objects
        self.__VarNames        = Settings.get_VarNames()                # List of variable names
        self.__VarLabels       = Settings.get_VarLabels()               # List of variable labels
        self.__ModelSettings   = Settings.get_Model()                   # Object holding general Model information
        self.__GeneralSettings = Settings.get_General()                 # Object holding general information
        self.__ClassLabels     = self.__ModelSettings.get_ClassLabels() # List of class labels
        self.__Dirs            = Directories(self.__OutputPath)         # Directory object
        self.__SampleFrac      = self.__ModelSettings.get_SampleFrac()
        self.__ScaleOutput     = self.__GeneralSettings.get_OutputScaling()

        self.__TrainMethod     = Settings.get_General().get_TrainMethod()

        ###################################################################
        ############## Settings decoration BDT samples ####################
        ###################################################################
        if self.__TrainMethod == "BDT-4t":
            self.__SIGNBKG        = "isSignBkg"                             # Name of the identifier for training
            self.__ODDEVEN        = "isOddEven"
            self.__TRAINTEST      = "isTrainTest"
        
        ###################################################################
        ######################## Model Information ########################
        ###################################################################
        if self.__GeneralSettings.get_RecalcScaler() or self.__GeneralSettings.get_InputScaling()=="none":
            self.__Scaler = None
        else:
            try:
                self.__Scaler = load(open(hf.ensure_trailing_slash(self.__Dirs.ModelDir())+"scaler.pkl", 'rb'))
            except:
                ErrorMessage("Could not load scalar from %s make sure the object exist and consider turning 'UseExistingScalar' off to regenerate a scaler"%(hf.ensure_trailing_slash(self.__Dirs.ModelDir())+"scaler.pkl"))
        self.__DataFrame = pd.read_hdf(hf.ensure_trailing_slash(self.__Dirs.DataDir())+"merged.h5")
        self.__DataFrame = self.__DataFrame.sample(frac = self.__SampleFrac, random_state = 1)
        self.__DataFrame["Index_tmp"] = np.arange(len(self.__DataFrame))
        if self.__TrainMethod == "BDT-4t":
            self.Prepare_OddEven()
        F_tmp = self.__DataFrame[self.__FOLDIDENTIFIER].values # temporarily save fold identifier to reattach them later
        X_tmp, W_tmp, self.__Scaler  = Preprocess(X=copy.deepcopy(self.__DataFrame[self.__VarNames].values),
                                                  Y=copy.deepcopy(self.__DataFrame[self.__LABELNAME].values),
                                                  W=copy.deepcopy(self.__DataFrame[self.__WEIGHTNAME].values),
                                                  settings=Settings,
                                                  scaler=self.__Scaler)
        Y_tmp = copy.deepcopy(self.__DataFrame[self.__LABELNAME].values)
        if self.__ModelSettings.isRegressionDNN():
            if self.__ScaleOutput is not None:
                Y_tmp, _ = ScaleInputs(Y_tmp.reshape(-1,1),self.__ScaleOutput,None)
                Y_tmp = Y_tmp.T[0]

            if self.__ModelSettings.ReweightTargetDist():
                nbins = int(self.__ModelSettings.get_ModelBinning()[0][0])
                xlow = self.__ModelSettings.get_ModelBinning()[0][1]
                xhigh = self.__ModelSettings.get_ModelBinning()[0][2]
                hist, bin_edges = np.histogram(Y_tmp,
                                               bins=nbins,
                                               range=(xlow,xhigh),
                                               weights=W_tmp)
                SF = np.max(hist)/hist
                indece = [ind-1 for ind in np.digitize(Y_tmp,bin_edges)]
                indece = [ind if ind!=nbins else nbins-1 for ind in indece]
                W_corr = SF[indece]
                W_tmp*=W_corr
                W_tmp = W_tmp/np.mean(W_tmp)
                hist, bin_edges = np.histogram(Y_tmp, bins=25, range=(0,300), weights=W_tmp)
        if self.__ISNOMINAL in self.__DataFrame: # for older versions this variable might not exist yet
            isNominal_tmp = copy.deepcopy(self.__DataFrame[self.__ISNOMINAL].values)
        else:
            isNominal_tmp = [True]*len(Y_tmp)
            self.__DataFrame[self.__ISNOMINAL] = isNominal_tmp
        N_tmp = copy.deepcopy(self.__DataFrame[self.__SAMPLENAME].values)
        if self.__TrainMethod == "BDT-4t":
            SB_tmp = copy.deepcopy(self.__DataFrame[self.__SIGNBKG].values)
            OE_tmp = copy.deepcopy(self.__DataFrame[self.__ODDEVEN].values)
            TT_tmp = copy.deepcopy(self.__DataFrame[self.__TRAINTEST].values)
        self.__DataFrame_sc = pd.DataFrame(data=X_tmp, columns = self.__VarNames)
        self.__DataFrame_sc[self.__WEIGHTNAME] = W_tmp
        self.__DataFrame_sc[self.__LABELNAME] = Y_tmp
        self.__DataFrame_sc[self.__ISNOMINAL] = isNominal_tmp
        self.__DataFrame_sc[self.__SAMPLENAME] = N_tmp
        self.__DataFrame_sc[self.__FOLDIDENTIFIER] = F_tmp # reattaching fold identifer
        self.__DataFrame_sc["Index_tmp"] = np.arange(len(self.__DataFrame_sc))
        if self.__TrainMethod == "BDT-4t":
            self.__DataFrame_sc[self.__SIGNBKG] = SB_tmp
            self.__DataFrame_sc[self.__ODDEVEN] = OE_tmp
            self.__DataFrame_sc[self.__TRAINTEST] = TT_tmp
        self.apply_Penalty()
        self.prep_VariablesFile()
        
    ###################################################################
    ########################## Class Methods ##########################
    ###################################################################
    def TrainAllFolds(self):

        for Fold in range(self.get_nFolds()):
            self.get_ModelSettings().CompileAndTrain(self.get_TrainInputs(Fold=Fold, returnNonZeroLabels=False, returnNominalOnly=False).values, self.get_TrainLabels(Fold=Fold, returnNonZeroLabels=False, returnNominalOnly=False).values, self.get_TrainWeights_sc(Fold=Fold, returnNonZeroLabels=False, returnNominalOnly=False).values, Directories(self.get_OutputPath()).ModelDir(), Fold=str(Fold))


    def prep_VariablesFile(self):
        '''
        Function to prepare a variables.json file for the usage with lwtnn.
        '''
        if self.__GeneralSettings.get_InputScaling() == "minmax" or self.__GeneralSettings.get_InputScaling() == "minmax_symmetric":
            offsets = -self.__Scaler.data_min_
            scales = self.__Scaler.scale_
        elif self.__GeneralSettings.get_InputScaling() == "standard":
            offsets = -self.__Scaler.mean_
            scales = self.__Scaler.scale_
        else:
            offsets = [0]*len(self.__VarNames) # Default offset is 0
            scales = [1]*len(self.__VarNames) # Default scale is 1
        Variables_Dict = {"input_sequences":[],
                          "inputs": [{"name": "Input_Variables",
                                      "variables": [{"name": var_name, "offset": float(offset), "scale": float(scale)} for var_name, offset, scale in zip(self.__VarNames,offsets,scales)]}],
                          "outputs": [{"labels":[Label for Label in self.__ClassLabels],
                                       "name": "Predictions"}]}
        try:
            with open(hf.ensure_trailing_slash(self.__Dirs.LwtnnDir())+self.__ModelSettings.get_Name()+"_"+self.__ModelSettings.get_Type()+"_Variables.json", "w") as outfile:
                json.dump(Variables_Dict, outfile)
        except Exception as e:
            ErrorMessage("Could not convert variables into json file! %e"%e)

    def apply_Penalty(self):
        Penalty_dict = {Sample.get_Name(): Sample.get_PenaltyFactor() for Sample in self.__Samples}
        # If a sample is in a group the penalty term is 1 by default
        if any([Sample.get_Group()!=None and Sample.get_PenaltyFactor()!=1 for Sample in self.__Samples]):
            ErrorMessage("Can not use 'Group' for sample that has 'PenaltyFactor' defined")
        Groups = [Sample.get_Group() for Sample in self.__Samples]
        for group in Groups:
            Penalty_dict[group] = 1
        N_tmp = self.__DataFrame_sc[self.__SAMPLENAME].values.tolist()
        Penalty_Factor = np.array([Penalty_dict[SampleName] for SampleName in N_tmp])
        self.__DataFrame_sc[self.__PENALTYNAME] = Penalty_Factor
        self.__DataFrame_sc[self.__WEIGHTNAME] = self.__DataFrame_sc[self.__PENALTYNAME]*self.__DataFrame_sc[self.__WEIGHTNAME]


    def Prepare_OddEven(self):
        Datasets = [self.__DataFrame]#, self.__DataFrame_sc
        randset = np.random.rand(len(self.__DataFrame))
        for set in Datasets:
            set.loc[set.index[set[self.__SAMPLENAME] == "tttt"], self.__SIGNBKG] = "SNLO"
            set.loc[set.index[set[self.__SAMPLENAME] == "tttt_LO"], self.__SIGNBKG] = "SLO"
            set.loc[set.index[(set[self.__LABELNAME] == 0)], self.__SIGNBKG] = "BKG"

        
            set.loc[set.index % 2 == 0, self.__ODDEVEN] = "Even"
            set.loc[set.index % 2 == 1, self.__ODDEVEN] = "Odd"
        
            set[self.__TRAINTEST] = np.where((set[self.__LABELNAME] == 0), np.where((randset > 0.2), "TRAIN", "TEST"), "SIGN")           
       

    def get_DataFrameInfo(self, variable,  dataframe, ReturnTrainORtest=None ,Fold=0, returnNonZeroLabels=False, returnNominalOnly=True):
        if dataframe == "":
            getterFunction = self.get_DataFrame(returnNominalOnly)
        if dataframe == "sc":
            getterFunction = self.get_DataFrame_sc(returnNominalOnly)
        if ReturnTrainORtest == None:
            if returnNonZeroLabels==False and self.__nFolds!=1:
                tmp = getterFunction[getterFunction[self.__FOLDIDENTIFIER]%self.get_nFolds()==Fold]
                return tmp[tmp[self.__LABELNAME]>=0][variable]
            if returnNonZeroLabels==True and self.__nFolds!=1:
                tmp = getterFunction[getterFunction[self.__FOLDIDENTIFIER]%self.get_nFolds()==Fold]
                return tmp[variable]
            if returnNonZeroLabels==False and self.__nFolds==1:
                return getterFunction[getterFunction[self.__LABELNAME]>=0][variable]
            if returnNonZeroLabels==True and self.__nFolds==1:
                tmp = getterFunction[variable]
                return tmp
        if ReturnTrainORtest == True:
            if returnNonZeroLabels==False and self.__nFolds!=1:
                tmp = getterFunction[getterFunction[self.__FOLDIDENTIFIER]%self.get_nFolds()==Fold]
                if(Fold==0):
                    tmp_var =  tmp[(tmp[self.__LABELNAME]>=0)&(tmp[self.__SIGNBKG] == "SLO")|((tmp[self.__SIGNBKG] == "BKG")&(tmp[self.__TRAINTEST] == "TRAIN"))&(tmp[self.__ODDEVEN]== "Odd")]
                if(Fold==1):
                    tmp_var =  tmp[(tmp[self.__LABELNAME]>=0)&(tmp[self.__SIGNBKG] == "SLO")|((tmp[self.__SIGNBKG] == "BKG")&(tmp[self.__TRAINTEST] == "TRAIN"))&(tmp[self.__ODDEVEN]== "Even")]
                return tmp_var[tmp[self.__LABELNAME]>=0][variable]
            if returnNonZeroLabels==True and self.__nFolds!=1:
                tmp = getterFunction[getterFunction[self.__FOLDIDENTIFIER]%self.get_nFolds()==Fold]
                if(Fold==0):
                    tmp_var =  tmp[(tmp[self.__SIGNBKG] == "SLO")|((tmp[self.__SIGNBKG] == "BKG")&(tmp[self.__TRAINTEST] == "TRAIN"))&(tmp[self.__ODDEVEN]== "Odd")]
                if(Fold==1):
                    tmp_var =  tmp[(tmp[self.__SIGNBKG] == "SLO")|((tmp[self.__SIGNBKG] == "BKG")&(tmp[self.__TRAINTEST] == "TRAIN"))&(tmp[self.__ODDEVEN]== "Even")]
                return tmp_var[variable] 
        if ReturnTrainORtest == False:
            if returnNonZeroLabels==False and self.__nFolds!=1:
                tmp = getterFunction[getterFunction[self.__FOLDIDENTIFIER]%self.get_nFolds()==Fold]
                if(Fold==0):
                    tmp_var =  tmp[(tmp[self.__LABELNAME]>=0)&(tmp[self.__SIGNBKG] == "SNLO")|((tmp[self.__SIGNBKG] == "BKG")&(tmp[self.__TRAINTEST] == "TEST"))]
                if(Fold==1):
                    tmp_var =  tmp[(tmp[self.__LABELNAME]>=0)&(tmp[self.__SIGNBKG] == "SNLO")|((tmp[self.__SIGNBKG] == "BKG")&(tmp[self.__TRAINTEST] == "TEST"))]
                return tmp_var[tmp[self.__LABELNAME]>=0][variable]
            if returnNonZeroLabels==True and self.__nFolds!=1:
                tmp = getterFunction[getterFunction[self.__FOLDIDENTIFIER]%self.get_nFolds()==Fold]
                if(Fold==0):
                    tmp_var =  tmp[(tmp[self.__SIGNBKG] == "SNLO")|((tmp[self.__SIGNBKG] == "BKG")&(tmp[self.__TRAINTEST] == "TEST"))]
                if(Fold==1):
                    tmp_var =  tmp[(tmp[self.__SIGNBKG] == "SNLO")|((tmp[self.__SIGNBKG] == "BKG")&(tmp[self.__TRAINTEST] == "TEST"))]
                return tmp_var[variable]
            
    def get_TrainInputs_prescaled(self, Fold=0, returnNonZeroLabels=False, returnNominalOnly=True):
        '''
        Function to return training inputs but before they are scaled.

        Keyword Arguments:
        Fold                --- Number (integer) of the fold to be used
        returnNonZeroLabels --- Whether samples with negative training labels should be taken into account
        returnNominalOnly   --- Whether only nominal samples (no systematics) should be taken into account
        '''
        dataframe = ""
        if self.__TrainMethod == "BDT-4t":
            ReturnTrainORtest = True
        tmp = self.get_DataFrameInfo(self.__VarNames, dataframe, ReturnTrainORtest,  Fold, returnNonZeroLabels, returnNominalOnly)
        return tmp

    def get_TestInputs_prescaled(self, Fold=0, returnNonZeroLabels=False, returnNominalOnly=True):
        '''
        Function to return testing inputs but before they are scaled.

        Keyword Arguments:
        Fold                --- Number (integer) of the fold to be used
        returnNonZeroLabels --- Whether samples with negative training labels should be taken into account
        returnNominalOnly   --- Whether only nominal samples (no systematics) should be taken into account
        '''
        dataframe = ""
        if self.__TrainMethod == "BDT-4t":
            ReturnTrainORtest = False
        tmp = self.get_DataFrameInfo(self.__VarNames, dataframe, ReturnTrainORtest,  Fold, returnNonZeroLabels, returnNominalOnly)
        return tmp


    def get_TrainInputs(self, Fold=0, returnNonZeroLabels=False, returnNominalOnly=True):
        
        dataframe = "sc"
        if self.__TrainMethod == "BDT-4t":
            ReturnTrainORtest = True
        tmp = self.get_DataFrameInfo(self.__VarNames, dataframe, ReturnTrainORtest,  Fold, returnNonZeroLabels, returnNominalOnly)
        return tmp
       

    def get_TestInputs(self, Fold=0, returnNonZeroLabels=False, returnNominalOnly=True):
        
        dataframe = "sc"
        if self.__TrainMethod == "BDT-4t":
            ReturnTrainORtest = False
        tmp = self.get_DataFrameInfo(self.__VarNames, dataframe, ReturnTrainORtest,  Fold, returnNonZeroLabels, returnNominalOnly)
        return tmp


    def get_TrainLabels(self, Fold=0, returnNonZeroLabels=False, returnNominalOnly=True):
       
        dataframe = "sc"
        if self.__TrainMethod == "BDT-4t":
            ReturnTrainORtest = True
        tmp = self.get_DataFrameInfo(self.__LABELNAME, dataframe, ReturnTrainORtest,  Fold, returnNonZeroLabels, returnNominalOnly)
        return tmp 

    def get_TestLabels(self, Fold=0, returnNonZeroLabels=False, returnNominalOnly=True):
        
        dataframe = "sc"
        if self.__TrainMethod == "BDT-4t":
            ReturnTrainORtest = False
        tmp = self.get_DataFrameInfo(self.__LABELNAME, dataframe, ReturnTrainORtest,  Fold, returnNonZeroLabels, returnNominalOnly)
        return tmp 
        
        
    def get_TrainWeights(self, Fold=0, returnNonZeroLabels=False, returnNominalOnly=True):

        dataframe = ""
        if self.__TrainMethod == "BDT-4t":
            ReturnTrainORtest = True
        tmp = self.get_DataFrameInfo(self.__WEIGHTNAME, dataframe, ReturnTrainORtest,  Fold, returnNonZeroLabels, returnNominalOnly)
        return tmp 
    
            
    def get_TestWeights(self, Fold=0, returnNonZeroLabels=False, returnNominalOnly=True):
        
        dataframe = ""
        if self.__TrainMethod == "BDT-4t":
            ReturnTrainORtest = False
        tmp = self.get_DataFrameInfo(self.__WEIGHTNAME, dataframe, ReturnTrainORtest,  Fold, returnNonZeroLabels, returnNominalOnly)
        return tmp
      

    def get_TrainWeights_sc(self, Fold=0, returnNonZeroLabels=False, returnNominalOnly=True):
       
        dataframe = "sc"
        if self.__TrainMethod == "BDT-4t":
            ReturnTrainORtest = True
        tmp = self.get_DataFrameInfo(self.__WEIGHTNAME, dataframe, ReturnTrainORtest,  Fold, returnNonZeroLabels, returnNominalOnly)
        return tmp
       

    def get_TestWeights_sc(self, Fold=0, returnNonZeroLabels=False, returnNominalOnly=True):
     
        dataframe = "sc"
        if self.__TrainMethod == "BDT-4t":
            ReturnTrainORtest = False
        tmp = self.get_DataFrameInfo(self.__WEIGHTNAME, dataframe, ReturnTrainORtest,  Fold, returnNonZeroLabels, returnNominalOnly)
        return tmp
     
    def get_Labels(self, returnNominalOnly=True):
            return self.get_DataFrame_sc(returnNominalOnly)[self.__LABELNAME]
    
    def get_TrainSignBkg(self, returnNominalOnly=True):
            return self.get_DataFrame(returnNominalOnly)[self.__SIGNBKG]
    
    def get_TrainSignBkg_sc(self, returnNominalOnly=True):
            return self.get_DataFrame_sc(returnNominalOnly)[self.__SIGNBKG]

    def get_Weights(self, returnNominalOnly=True):
        return self.get_DataFrame(returnNominalOnly)[self.__WEIGHTNAME]

    def get_Weights_sc(self, returnNominalOnly=True):
        return self.get_DataFrame_sc(returnNominalOnly)[self.__WEIGHTNAME]

    def get_FoldIdentifier(self, returnNominalOnly=True):
        return self.get_DataFrame(returnNominalOnly)[self.__FOLDIDENTIFIER]
    
    def get_Sample_Names(self, returnNominalOnly=True):
        return self.get_DataFrame(returnNominalOnly)[self.__SAMPLENAME]

    def get_Sample_Types(self, returnNominalOnly=True):
        return self.get_DataFrame(returnNominalOnly)[self.__SAMPLETYPE]
    
    def get_DataFrame(self, returnNominalOnly=True):
        if returnNominalOnly==True:
            return self.__DataFrame[self.__DataFrame[self.__ISNOMINAL]==returnNominalOnly]
        else:
            return self.__DataFrame

    def get_DataFrame_sc(self, returnNominalOnly=True):
        if returnNominalOnly==True:
            return self.__DataFrame_sc[self.__DataFrame_sc[self.__ISNOMINAL]==returnNominalOnly]
        else:
            return self.__DataFrame_sc
           
    def get_OutputPath(self):
        return self.__OutputPath

    def get_nFolds(self):
        return self.__nFolds

    def get_VarObjects(self):
        return self.__VarObjects

    def get_VarNames(self):
        return self.__VarNames

    def get_VarLabels(self):
        return self.__VarLabels

    def get_Samples(self):
        return self.__Samples

    def get_NominalSamples(self):
        return self.__NominalSamples

    def get_SystSamples(self):
        return self.__SystSamples

    def get(self, variable, returnNominalOnly=True):
        return self.__DataFrame[self.__DataFrame[self.__ISNOMINAL]==returnNominalOnly][variable]

    def get_sc(self, variable, returnNominalOnly=True):
        return self.__DataFrame_sc[self.__DataFrame_sc[self.__ISNOMINAL]==returnNominalOnly][variable]

    def get_ModelDirectory(self):
        return self.__Dirs.ModelDir()
    
    def get_MVAPlotDirectory(self):
        return self.__Dirs.MVAPlotDir()

    def get_CtrlPlotDirectory(self):
        return self.__Dirs.CtrlPlotDir()

    def get_ModelSettings(self):
        return self.__ModelSettings

    def get_GeneralSettings(self):
        return self.__GeneralSettings

    def get_ClassLabels(self):
        return self.__ClassLabels

"""
This module handles binning optimisation as described
in https://cds.cern.ch/record/2296985/files/CERN-THESIS-2017-258.pdf
"""
from array import array

class TransfoBinning:
    """
    Class for binning optimisation
    """
    def __init__(self, hist_b, hist_s, zb, zs):
        self.hist_b = hist_b
        self.hist_s = hist_s
        self.zb = zb
        self.zs = zs
        self.Nb = self.hist_b.Integral()
        self.Ns = self.hist_s.Integral()

    def TransfoD(self):
        """
        Function to get the actual TransfoD binning

        returns: float array with bin edges
        """
        nb = 0
        ns = 0
        Binning = []
        half_binwidth = (self.hist_b.GetBinCenter(1)-self.hist_b.GetBinCenter(0))/2
        Binning.append(self.hist_b.GetBinCenter(self.hist_b.GetNbinsX()+1)-half_binwidth)
        for bin_id in reversed(range(1, self.hist_b.GetNbinsX()+1)):
            nb += self.hist_b.GetBinContent(bin_id)
            ns += self.hist_s.GetBinContent(bin_id)
            Z = self.zb*nb/self.Nb+self.zs*ns/self.Ns
            if Z > 1:
                Binning.append(self.hist_b.GetBinCenter(bin_id)-half_binwidth)
                nb = 0
                ns = 0
        Binning.append(self.hist_b.GetBinCenter(1)-half_binwidth)
        return array("d", Binning[::-1])

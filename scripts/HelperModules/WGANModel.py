from HelperModules.MessageHandler import ErrorMessage, TrainerDoneMessage

import json
import pandas as pd
import HelperModules.HelperFunctions as hf
import tensorflow as tf

from HelperModules.BasicModel import BasicModel
from tensorflow.keras.layers import Dense, Dropout
from tensorflow.keras.models import Model, Sequential
from tensorflow.keras.optimizers import Adam
from sklearn.model_selection import train_test_split

import numpy as np

class WGANModel(BasicModel):
    """ 
    The WGAN (Wasserstein GAN) is introduced/motivated in https://arxiv.org/abs/1701.07875 (Martin Arjovsky, et al.)
    This WGAN implementation is based on https://arxiv.org/abs/1704.00028.
    Instead of using the discriminator to predict whether a sample is real or fake the dicriminator in a WGAN
    the discriminator serves as a "critic" that scores the realness or fakeness of a given sample.
    The WGAN seeks a minimisation of the mathematical distance between the real distribution and the distribution
    of the generated samples. Typical distance measures are the Kullback-Leibler divergence, Jensen-Shannon divergence
    or the Earth-Mover distance (also known as Wasserstein distance).
    """
    def __init__(self, group, input_dim, Variables, is_Train=True):
        super().__init__(group)
        self.__InputDim            = input_dim # Input dimensionality of the data
        self.__Variables           = Variables # All Variables
        self.__kerasmodel          = None      # Keras model object
        self.__OutputSize          = 1         # Size of the output layer (for now set to 1 by default)
        self.__LatentDim           = 128       # Latent dimension for GAN Training
        self.__Critic              = None      # Critic model for GAN Training
        self.__Generator           = None      # Generator model for GAN Taining
        self.__SmoothingAlpha      = None      # Parameter to be used for label smoothing
        self.__nCriticUpdates      = 5         # Number of times the critic is updated
        self.__GPWeight            = 10        # Weighting of the gradient penalty
        self.__CriticLR            = 0.0001    # Learning rate of the critic
        self.__GeneratorLR         = 0.0001    # Learning rate of the generator
        self.__Critic_Optimizer    = Adam(learning_rate=self.__CriticLR, beta_1=0.0, beta_2=0.9)
        self.__Generator_Optimizer = Adam(learning_rate=self.__GeneratorLR, beta_1=0.0, beta_2=0.9)

        self.__AllowedWGANTypes    = ["Classification-WGAN"]

        #######################################################################
        ############################ Input Options ############################
        #######################################################################
        for item in self._BasicModel__Group:
            if "LatentDim" in item:
                self.__LatentDim = hf.ReadOption(item, "LatentDim", istype="int", islist=False)
            if "GPWeight" in item:
                self.__GPWeight = hf.ReadOption(item, "GPWeight", istype="float", islist=False)
            if "nCriticUpdates" in item:
                self.__nCriticUpdates = hf.ReadOption(item, "nCriticUpdates", istype="int", islist=False)
            if "CriticLR" in item:
                self.__CriticLR = hf.ReadOption(item, "CriticLR", istype="float", islist=False)
            if "GeneratorLR" in item:
                self.__GeneratorLR = hf.ReadOption(item, "GeneratorLR", istype="float", islist=False)
            if "SmoothingAlpha" in item:
                self.__SmoothingAlpha = hf.ReadOption(item, "SmoothingAlpha", istype="float", islist=False)

        #######################################################################
        ############################ Options Check ############################
        #######################################################################
        if self._BasicModel__Type not in self.__AllowedWGANTypes:
            ErrorMessage("MODEL " + self._BasicModel__Name + " has invalid type set for WGANMODEL!")
        if self.__SmoothingAlpha != None and (self.__SmoothingAlpha > 0.5 or self.__SmoothingAlpha<0):
            ErrorMessage("MODEL " + self._BasicModel__Name + " has invalid SmoothingAlpha value! Must be between >0 and <0.5!")
        if self.__LatentDim <=0:
            ErrorMessage("MODEL " + self._BasicModel__Name + " has unsupported 'LatentDim' set. Needs to be >0. Default is 128.")
        if self.__nCriticUpdates <=0:
            ErrorMessage("MODEL " + self._BasicModel__Name + " has unsupported 'nCriticUpdates' set. Needs to be >0.")
        if self.__GPWeight <0:
            ErrorMessage("MODEL " + self._BasicModel__Name + " has negative 'GPWeight' set. This does not make sense.")
        if self.__CriticLR <0:
            ErrorMessage("MODEL " + self._BasicModel__Name + " has negative 'CriticLR' set. This does not make sense.")
        if self.__GeneratorLR <0:
            ErrorMessage("MODEL " + self._BasicModel__Name + " has negative 'GeneratorLR' set. This does not make sense.")

    ############################################################################
    ############################ GAN Helper Methods ############################
    ############################################################################

    def Gradient_Penalty(self,batch_size, Real_Data, Fake_Data):
        """
        Calculates the gradient penalty on interpolated data.
        This penalty is then added to the critic's loss to enforce a
        Lipschitz contraint

        Keyword arguments:
        batch_size --- Batch size
        Real_Data  --- Set of real events
        Fake_Data  --- Set of fake events
        """
        Alpha        = tf.random.normal([batch_size, 1], 0.0, 1.0)
        Difference   = Fake_Data - Real_Data
        Interpolated = Real_Data + Alpha*Difference

        with tf.GradientTape() as gp_tape:
            gp_tape.watch(Interpolated)
            Prediction = self.__Critic(Interpolated, training=True)

        Gradients = gp_tape.gradient(Prediction, [Interpolated])[0]
        Norm      = tf.sqrt(tf.reduce_sum(tf.square(Gradients), axis = [1]))
        GP        = tf.reduce_mean((Norm - 1.0)**2)
        return GP

    def train_step(self, Real_Data):
        """
        This function overrides the keras train_step function.

        Keyword arguments:
        Real_Data  --- Set of real events
        """
        if isinstance(Real_Data,tuple):
            Real_Data = Real_Data[0]

        batch_size = tf.shape(Real_Data)[0]

        for i in range(self.__nCriticUpdates):
            Random_Latent_Vectors = tf.random.normal(shape = (batch_size, self.__LatentDim))
            with tf.GradientTape() as tape:
                Fake_Data   = self.__Generator(Random_Latent_Vectors, training=True)
                fake_logits = self.__Critic(Fake_Data, training=True)
                real_logits = self.__Critic(Real_Data, training=True)

                Critic_Cost = self.Critic_Loss_Function(Real_Data = real_logits, Fake_Data = fake_logits)
                GP          = self.Gradient_Penalty(batch_size, Real_Data, Fake_Data)
                Critic_Loss = Critic_Cost + GP * self.__GPWeight

            Critic_Gradient = tape.gradient(Critic_Loss, self.__Critic.trainable_variables)
            self.__Critic_Optimizer.apply_gradients(zip(Critic_Gradient, self.__Critic.trainable_variables))

        Random_Latent_Vectors = tf.random.normal(shape = (batch_size, self.__LatentDim))
        with tf.GradientTape() as tape:
            generated_data  = self.__Generator(Random_Latent_Vectors, training=True)
            gen_data_logits = self.__Critic(generated_data, training=True)
            Generator_Loss  = self.Generator_Loss_Function(gen_data_logits)

        Generator_Gradient = tape.gradient(Generator_Loss, self.__Generator.trainable_variables)
        self.__Generator_Optimizer.apply_gradients(zip(Generator_Gradient, self.__Generator.trainable_variables))
        return {"Critic Loss": Critic_Loss, "Generator Loss": Generator_Loss}

    def Critic_Loss_Function(self, Real_Data, Fake_Data):
        """
        Implementation of the loss function of the WGAN critic.

        Keyword arguments:
        Real_Data  --- Set of real events
        Fake_Data  --- Set of fake events
        """
        real_loss = tf.reduce_mean(Real_Data)
        fake_loss = tf.reduce_mean(Fake_Data)
        return fake_loss - real_loss

    def Generator_Loss_Function(self, Fake_Data):
        """
        Implementation of the loss function of the WGAN generator.

        Keyword arguments:
        Fake_Data  --- Set of fake events
        """
        return -tf.reduce_mean(Fake_Data)

    def Define_Generator(self):
        """
        Building a generator model which takes noise as input and produces fake events
        """
        Generator = Sequential()
        Generator.add(Dense(2048, input_dim=self.__LatentDim, activation="relu"))
        # Generator.add(Dropout(0.05))
        Generator.add(Dense(2048, activation="relu"))
        # Generator.add(Dropout(0.05))
        Generator.add(Dense(1024, activation="relu"))
        # Generator.add(Dropout(0.05))
        Generator.add(Dense(256, activation="relu"))
        Generator.add(Dense(self.__InputDim, activation='tanh'))
        return Generator

    def Define_Critic(self):
        """
        Building a critic model which takes event information as input and 
        'criticises' them.
        """
        Critic = Sequential()
        Critic.add(Dense(2048, input_dim=self.__InputDim, activation="relu"))
        Critic.add(Dropout(0.05))
        Critic.add(Dense(2048, activation="relu"))
        Critic.add(Dropout(0.05))
        Critic.add(Dense(1024, activation="relu"))
        Critic.add(Dropout(0.05))
        Critic.add(Dense(256, activation="relu"))
        Critic.add(Dense(self.__OutputSize))
        return Critic

    ###########################################################################
    ############################ Model Compilation ############################
    ###########################################################################

    def compile(self):
        """
        Compilation of the WGAN model. Effectively stacking them on top of each other.
        The critic layers are turned of for the training.
        """
        super(WGANModel,self).compile()
        Generator = self.Define_Generator()
        Critic = self.Define_Critic()
        self.__Generator = Generator
        self.__Critic = Critic

    ########################################################################
    ############################ Model Training ############################
    ########################################################################

    def Train(self, X, Y, W, output_path, Fold):
        """
        Training the WGAN model.

        Keyword Arguments:
        X           --- Array of data to be used for training
        Y           --- Labels
        W           --- Weights
        output_path --- Output path where the model.h5 files and the history is to be saved
        Fold        --- String declaring the fold name. This will be appended to the modelname upon saving it.
        """
        c1_hist, c2_hist, g_hist = list(), list(), list()

        for i in range(self._BasicModel__Epochs):
            c1_tmp, c2_tmp = list(), list()
            for _ in range(self.__NCritic):
                choice = np.random.randint(0,len(X),size=self._BasicModel__BatchSize)
                X_real = X[choice][:]
                W_real = W[choice][:]
                Y_real = -np.ones((self._BasicModel__BatchSize, 1))
                c_loss1 = self.__Critic.train_on_batch(X_real, Y_real, sample_weight=W_real)
                c1_tmp.append(c_loss1)

                noise = np.random.uniform(0,1,size=[self._BasicModel__BatchSize, self.__LatentDim])
                X_fake = self.__Generator.predict(noise)
                Y_fake = np.ones((self._BasicModel__BatchSize, 1))
                # update critic model weights
                c_loss2 = self.__Critic.train_on_batch(X_fake, Y_fake)
                c2_tmp.append(c_loss2)
            c1_hist.append(np.mean(c1_tmp))
            c2_hist.append(np.mean(c2_tmp))

            X_gan = np.random.uniform(-1,1,size=[self._BasicModel__BatchSize, self.__LatentDim])
            Y_gan = np.ones((self._BasicModel__BatchSize, 1))
            # update the generator via the critic's error
            g_loss = self.__kerasmodel.train_on_batch(X_gan, Y_gan)
            g_hist.append(g_loss)
            print('Epoch %d/%d'%(i+1, self._BasicModel__Epochs))
            print('critic_real_loss: %.3f - critic_fake_loss: %.3f - generator_loss:%.3f' % (c1_hist[-1], c2_hist[-1], g_loss))

        WGAN_Loss_dict = {"Critic_Real_Loss": dict(zip(range(len(c1_hist)),c1_hist)),
                          "Critic_Fake_Loss": dict(zip(range(len(c2_hist)),c2_hist)),
                          "Generator_Loss": dict(zip(range(len(g_hist)),g_hist))}

        TrainerDoneMessage("Training done! Writing model to "+ hf.ensure_trailing_slash(output_path)+self._BasicModel__Name+"_"+self._BasicModel__Type+Fold+".h5")
        self.__kerasmodel.save(hf.ensure_trailing_slash(output_path)+self._BasicModel__Name+"_"+self._BasicModel__Type+Fold+".h5")
        self.__Generator.save(hf.ensure_trailing_slash(output_path)+self._BasicModel__Name+"_"+self._BasicModel__Type+"_Generator"+Fold+".h5")
        TrainerDoneMessage("Writing history to "+ hf.ensure_trailing_slash(output_path)+self._BasicModel__Name+"_"+self._BasicModel__Type+"_history"+Fold+".json")
        with open(hf.ensure_trailing_slash(output_path)+self._BasicModel__Name+"_"+self._BasicModel__Type+"_history"+Fold+".json", mode='w') as f:
            json.dump(WGAN_Loss_dict,f)

    def CompileAndTrain(self, X, Y, W, output_path, Fold="", Variables=None):
        '''
        Combination of compilation and training for a densely connected NN.

        Keyword Arguments:
        X           --- Array of data to be used for training
        Y           --- Labels
        W           --- Weights
        output_path --- Output path where the model.h5 files and the history is to be saved
        Fold        --- String declaring the fold name. This will be appended to the modelname upon saving it.
        '''
        if(len(np.unique(Y))>2):
            Y=np.array(hf.OneHot(Y)).astype(np.float32)
        else:
            Y=np.array(Y).astype(np.float32)
        if self.__SmoothingAlpha!=None:
            Y=self.smooth_labels(Y,self.__SmoothingAlpha)
        seed = np.random.randint(1000, size=1)[0]
        X_train, X_val, y_train, y_val = train_test_split(X, Y, test_size=self._BasicModel__ValidationSize, random_state=seed)
        self.compile()
        history = self.fit(X_train, y_train, batch_size=self._BasicModel__BatchSize, epochs = self._BasicModel__Epochs)
        TrainerDoneMessage("Training done! Writing model to "+ hf.ensure_trailing_slash(output_path)+self._BasicModel__Name+"_"+self._BasicModel__Type+Fold+".h5")
        self.__Critic.save(hf.ensure_trailing_slash(output_path)+self._BasicModel__Name+"_"+self._BasicModel__Type+Fold+".h5")
        self.__Generator.save(hf.ensure_trailing_slash(output_path)+self._BasicModel__Name+"_"+self._BasicModel__Type+"_Generator"+Fold+".h5")
        TrainerDoneMessage("Writing history to "+ hf.ensure_trailing_slash(output_path)+self._BasicModel__Name+"_"+self._BasicModel__Type+"_history"+Fold+".json")
        hist_df = pd.DataFrame(history.history)
        with open(hf.ensure_trailing_slash(output_path)+self._BasicModel__Name+"_"+self._BasicModel__Type+"_history"+Fold+".json", mode='w') as f:
            hist_df.to_json(f)

    def get_LatentDim(self):
        return self.__LatentDim

    def __str__(self):
        '''
        Method to represent the class objects as a string.
        '''

        return ("MODELINFO:\tName: "+self._BasicModel__Name
                +"\nMODELINFO:\tType: "+self._BasicModel__Type
                +"\nMODELINFO:\tEpochs: "+str(self._BasicModel__Epochs)
                +"\nMODELINFO:\tBatchSize: "+str(self._BasicModel__BatchSize)
                +"\nMODELINFO:\tOutputSize: "+str(self.__OutputSize)
                +"\nMODELINFO:\tLoss: Wasserstein Loss"
                +"\nMODELINFO:\tMetrics: "+str(self._BasicModel__Metrics))

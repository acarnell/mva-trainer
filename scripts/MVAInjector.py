#!/usr/bin/env python3

import argparse, os
import tensorflow as tf
from multiprocessing import Pool
from HelperModules.HelperFunctions import create_output_dir
from HelperModules.Injector import Injector
from HelperModules.MessageHandler import InjectorMessage, InjectorDoneMessage, WelcomeMessage, EnvironmentalCheck
from HelperModules.MultiprocessingErrorHandling import LoggingPool

def inject(args, fname, ID, MAX_ID):
    # Here we create the necessary settings objects and read information from the config file
    InjectorMessage("Processing "+str(ID)+"/"+str(MAX_ID)+": "+fname)
    Inj = Injector(args)
    Inj.do_Injection(fname)
    InjectorDoneMessage("Finished "+fname)

def absoluteFilePaths(directory):
    for dirpath,_,filenames in os.walk(directory):
        for f in filenames:
            yield os.path.abspath(os.path.join(dirpath, f))

if __name__ == "__main__":
    parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument("-o", "--output_path", help="output path for the converted ntuples", required=True)
    parser.add_argument("-c", "--configfile", help="Config file", nargs='+', required=True)
    parser.add_argument("-i", "--input_path", help="Ntuple path", required=True)
    parser.add_argument("-f", "--filter", dest="wildcard", help="additional string to filter input files", type=str, default='')
    parser.add_argument("-t", "--treefilter", dest="tree_wildcard", help="additional string to filter trees", type=str, default='')
    parser.add_argument("-m", "--model-path", help="input path of the ML model and history", nargs='+', required=True)
    parser.add_argument("--processes", help="the number of parallel processes to run", type=int, default=8)
    args = parser.parse_args()

    #Ignoring tf warnings and info messages
    tf.get_logger().setLevel('ERROR')

    WelcomeMessage("Injector")
    EnvironmentalCheck(check="env")

    # Let's make sure the output directory exists. This is done recursively and should work for python>3.5
    for dirpath, dirnames, filenames in os.walk(args.input_path):
        structure = os.path.join(args.output_path, dirpath[len(args.input_path):].strip("/"))
        if not os.path.isdir(structure):
            os.makedirs(structure)
    fnames = absoluteFilePaths(args.input_path)
    fnames = [f for f in fnames if args.wildcard in f or args.wildcard=='']
    p = Pool(args.processes, maxtasksperchild=1)
    MAX_ID = len(fnames)
    results = [p.apply_async(inject, (args, fname, ID+1, MAX_ID,)) for ID,fname in enumerate(fnames)]
    results_g = [r.get() for r in results]
    p.close()
    p.join()

#!/usr/bin/env python3

import argparse, os, sys
from HelperModules.DNNModel import DNNModel, DNNOptimisation
from HelperModules import Settings
from HelperModules.MessageHandler import ErrorMessage, WelcomeMessage, OptimiserMessage, EnvironmentalCheck

if __name__ == "__main__":
    parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument("-c", "--configfile", help="Config file", required=True)
    parser.add_argument("-cp", "--configpath", help="Output path for config files for hyperparameter optimisation", required=True)
    parser.add_argument("--RunOption", help="Available options for the HTCondor to run scripts.", choices=["ConversionAndTraining","Converter","Trainer"], required=True)
    parser.add_argument("--NNetworks", help="Number of networks to be trained", type=int, required=True, default=10)
    parser.add_argument("--RandomSeed", help="Random seed for determining the hyperparameters", type=int)
    args = parser.parse_args()

    WelcomeMessage("Optimiser")
    EnvironmentalCheck()

    # Here we create the necessary settings objects and read information from the config file
    cfg_settings = Settings.Settings(args.configfile, option="all")
    OptimisedDNN = DNNOptimisation(args, cfg_settings,args.NNetworks)
    OptimisedDNN.Write_Configs()
    OptimisedDNN.Define_Submission()
        

                    

"""
Module to handle plots for classification models
"""
import HelperModules.HelperFunctions as hf
from HelperModules.AtlasLabel import ATLASLabel, CustomLabel, DrawLabels
from HelperModules.MessageHandler import PostProcessorMessage, WarningMessage
from HelperModules.Transfo import TransfoBinning
from PlotterModules.BasicPlotHandler import BasicPlotHandler
from sklearn.metrics import confusion_matrix, roc_auc_score, roc_curve, auc
from array import array
from ROOT import  gPad, gStyle, gROOT, kRed, kBlue, TH1D, TH2D, TCanvas, THStack, TGraph, TLegend, TMultiGraph, TMVA, TPad, TLine, vector
import copy
import numpy as np

class ClassificationPlotHandler(BasicPlotHandler):
    """
    Class to handle the plots for classification models
    """
    def __init__(self,Settings):
        super().__init__(Settings)

    ########################################################################
    ############################ misc functions ############################
    ########################################################################

    def array_to_cpp_vector(vector_type : str, original_array) -> vector:
        """
        Convertes array to c++ style vector
        """
        result = vector(vector_type)()
        result += original_array
        return result

    ###########################################################################
    ############################ Wrapper functions ############################
    ###########################################################################
    def do_Plots(self):
        """
        Wrapper function to produce all available plots in one go.
        """
        if self.Model.isClassificationDNN() or self.Model.isReconstruction():
            try:
                self.do_ArchitecturePlot()
            except ImportError:
                print("Unable to create architecture plots")
            self.do_PerformancePlot()
        if len(self.Model.get_ClassLabels())==3:
            self.__do_Stack2D()
        if self.Model.is3LZReconstruction() or self.Model.is4LZReconstruction():
            self.__do_WeightedAccuracies()
            self.__do_EfficiencyPlot()
        self.__do_YieldsPlot()
        self.__do_StackPlots()
        self.__do_Separations1D()
        self.__do_TrainTestPlots()
        self.__do_SoverBPlots()
        self.__do_CorrelationPlots()
        self.__do_BinaryConfusionPlots()
        self.__do_ROCCurvePlotsCombined()
        self.__do_AUCSummaries()
        self.__do_ConfusionMatrix()
        self.__do_PermutationImportances()

    def __do_PermutationImportances(self):
        """
        Wrapper function to produce all permutation importance plots in one go.
        """
        if self.isBinary:
            self.__do_PermutationImportance(ClassLabel=self._DataHandler__ClassLabels[0], ClassID=1)
        else:
            for ClassID, ClassLabel in enumerate(self._DataHandler__ClassLabels):
                if ClassID in self.get_Labels().values:
                    self.__do_PermutationImportance(PredictionColumn=ClassLabel, ClassLabel=ClassLabel, ClassID=ClassID)
                else:
                    WarningMessage("No event in class {}: No Permutation Importance plot produced.".format(ClassLabel))

    def __do_StackPlots(self):
        """
        Wrapper function to produce all stack plots in one go.
        """
        if self.isBinary:
            self.do_StackPlot(ClassLabel=self._DataHandler__ClassLabels[0], ClassID=1)
        else:
            for ClassID, ClassLabel in enumerate(self._DataHandler__ClassLabels):
                self.do_StackPlot(PredictionColumn=ClassLabel, ClassLabel=ClassLabel, ClassID=ClassID)

    def __do_Separations1D(self):
        """
        Wrapper function to produce all 1D separation plots in one go.
        """
        if self.isBinary:
            self.__do_Separation1D(ClassLabel=self._DataHandler__ClassLabels[0], ClassID=1)
        else:
            for ClassID, ClassLabel in enumerate(self._DataHandler__ClassLabels):
                if ClassID in self.get_Labels().values:
                    self.__do_Separation1D(PredictionColumn=ClassLabel, ClassLabel=ClassLabel, ClassID=ClassID)
                else:
                    WarningMessage("No event in class {}: No 1D separation plot produced.".format(ClassLabel))

    def __do_TrainTestPlots(self):
        """
        Wrapper function to produce all TrainTest plots in one go.
        """
        if self.isBinary:
            self.__do_TrainTestPlot(ClassLabel=self._DataHandler__ClassLabels[0], ClassID=1)
        else:
            for ClassID, ClassLabel in enumerate(self._DataHandler__ClassLabels):
                if ClassID in self.get_Labels().values:
                    self.__do_TrainTestPlot(PredictionColumn=ClassLabel, ClassLabel=ClassLabel, ClassID=ClassID)
                else:
                    WarningMessage("No event in class {}: No K-Fold comparison produced.".format(ClassLabel))

    def __do_SoverBPlots(self):
        """
        Wrapper function to produce all S over B plots in one go.
        """
        if self.isBinary:
            self.__do_SoverBPlot(ClassLabel=self._DataHandler__ClassLabels[0], ClassID=1)
        else:
            for ClassID, ClassLabel in enumerate(self._DataHandler__ClassLabels):
                if ClassID in self.get_Labels().values:
                    self.__do_SoverBPlot(PredictionColumn=ClassLabel, ClassLabel=ClassLabel, ClassID=ClassID)
                else:
                    WarningMessage("No event in class {}: No signal over background plots produced.".format(ClassLabel))

    def __do_CorrelationPlots(self):
        """
        Wrapper function to produce all correlation plots in one go.
        """
        if self.isBinary:
            self.__do_CorrelationPlot(ClassLabel=self._DataHandler__ClassLabels[0])
        else:
            for ClassLabel in self._DataHandler__ClassLabels:
                self.__do_CorrelationPlot(PredictionColumn=ClassLabel, ClassLabel=ClassLabel)

    def __do_BinaryConfusionPlots(self):
        """
        Wrapper function to produce all binary confusion plots in one go.
        """
        if self.isBinary:
            self.__do_BinaryConfusionPlot(ClassLabel=self._DataHandler__ClassLabels[0], ClassID=1)
        else:
            for ClassID, ClassLabel in enumerate(self._DataHandler__ClassLabels):
                self.__do_BinaryConfusionPlot(PredictionColumn=ClassLabel, ClassLabel=ClassLabel, ClassID=ClassID)

    def __do_ROCCurvePlotsCombined(self):
        """
        Wrapper function to produce all ROC curve plots in one go
        """
        if self.isBinary:
            self.__do_ROCCurvePlotCombined(ClassLabel=self._DataHandler__ClassLabels[0], ClassID=1)
        else:
            for ClassID, ClassLabel in enumerate(self._DataHandler__ClassLabels):
                self.__do_ROCCurvePlotCombined(PredictionColumn=ClassLabel, ClassLabel=ClassLabel, ClassID=ClassID)

    def __do_AUCSummaries(self):
        """
        Wrapper function to produce all AUCSummary plots in one go
        """
        if self.isBinary:
            self.__do_AUCSummary(ClassLabel=self._DataHandler__ClassLabels[0], ClassID=1)
        else:
            for ClassID, ClassLabel in enumerate(self._DataHandler__ClassLabels):
                self.__do_AUCSummary(PredictionColumn=ClassLabel, ClassLabel=ClassLabel, ClassID=ClassID)

    def __do_WeightedAccuracies(self):
        """
        Wrapper function to produce all Accuracy plots in one go
        """
        if self.isBinary:
            self.__do_WeightedAccuracy(ClassLabel=self._DataHandler__ClassLabels[0], ClassID=1)
        else:
            for ClassID, ClassLabel in enumerate(self._DataHandler__ClassLabels):
                if ClassID in self.get_Labels().values:
                    self.__do_WeightedAccuracy(PredictionColumn=ClassLabel, ClassLabel=ClassLabel, ClassID=ClassID)
                else:
                    WarningMessage("No event in class {}: No accuracy plots produced.".format(ClassLabel))


    ###########################################################################
    ########################### Plotting functions ############################
    ###########################################################################
    def __do_Stack2D(self):
        """
        Function to produce a 2D stacked plot for a 3-class multi-class classifier.
        """
        ClassLabels = self.get_ClassLabels()

        # Defining histograms filling them and scaling them to unity
        if self.Model.isClassification():
            hists = hf.DefineAndFill2D(
                Binning_1=[50.0, 0.0, 1.0],
                Binning_2=[50.0, 0.0, 1.0],
                Samples=self._DataHandler__Samples,
                Predictions_1=self._DataHandler__DataFrame.get(ClassLabels[0]).values,
                Predictions_2=self._DataHandler__DataFrame.get(ClassLabels[2]).values,
                N=self.get_Sample_Names().values,
                W=self.get_Weights().values)
        elif self.Model.is3LZReconstruction() or self.Model.is4LZReconstruction():
            hists = hf.DefineAndFill2D(
                Binning_1=[50.0, 0.0, 1.0],
                Binning_2=[50.0, 0.0, 1.0],
                Samples=self._DataHandler__ClassLabels,
                Predictions_1=self._DataHandler__DataFrame.get(ClassLabels[0]).values,
                Predictions_2=self._DataHandler__DataFrame.get(ClassLabels[2]).values,
                N=self.get_Labels().values,
                W=self._DataHandler__DataFrame[self._DataHandler__WEIGHTNAME].values,
                SamplesAreClasses=True)

        for key,value in hists.items():
            if value[1][0]!="Data":
                if self.Model.isClassification():
                    PostProcessorMessage("Plotting 2D MC plot for "+key+".")
                    basename = self.Model.get_Name()+"_"+key+"_2DMVA"
                elif self.Model.isReconstruction():
                    PostProcessorMessage("Plotting 2D MC plot for "+value[1][1]+".")
                    basename = value[1][1]+"_2DMVA"
                Canvas = TCanvas("c1","c1",800,600)
                value[0].Draw("colz")
                Canvas.SetRightMargin(0.15)
                value[0].GetZaxis().SetTitle("Events")
                value[0].GetXaxis().SetTitle(ClassLabels[0]+" Score")
                value[0].GetYaxis().SetTitle(ClassLabels[2]+" Score")
                value[0].SetMinimum(0)
                DrawLabels(self.ALabel, self.CMLabel, self.MyLabel, Option="MC", align="right_3")
                if self.Model.isClassification():
                    CustomLabel(0.4, 0.7, key+" Response")
                if self.Model.is3LZReconstruction() or self.Model.is4LZReconstruction():
                    CustomLabel(0.4, 0.7, value[1][1]+" Response")
                for file_extension in self.get_GeneralSettings().get_PlotFormat():
                    Canvas.SaveAs(hf.ensure_trailing_slash(self.get_MVAPlotDirectory())+basename+"."+file_extension)
                del Canvas
            if value[1][0]=="Data":
                continue

    def __do_Separation1D(self, PredictionColumn=None, ClassLabel=None, ClassID=1):
        """
        Function to generate a one dimensional separation plot.

        Keyword arguments:
        PredictionColumn --- Name of the column holding the prediction
        ClassLabel       --- Name of the class itself
        ClassID          --- Label associated to the requested class
        """
        if PredictionColumn is None:
            PredictionColumn=self._DataHandler__PREDICTIONNAME
        basename = self.Model.get_Name()+"_"+self.Model.get_Type()

        PostProcessorMessage("Plotting 1D separation plot for "+ClassLabel+" classifier.")
        Binning = self.ModelVariables[ClassLabel].get_VarBinning()
        if self.Model.isClassification():
            hists = hf.DefineAndFill(Binning,
                                     Samples=self._DataHandler__Samples,
                                     xvals=self.get_Predictions(PredictionColumn).values,
                                     xval_identifiers=self._DataHandler__DataFrame[self._DataHandler__SAMPLENAME].values,
                                     weights=self._DataHandler__DataFrame[self._DataHandler__WEIGHTNAME].values,
                                     ClassID=ClassID,
                                     binningoptimisation=self.Model.get_BinningOptimisation())
        elif self.Model.is3LZReconstruction() or self.Model.is4LZReconstruction():
            hists = hf.DefineAndFill(Binning,
                                     Samples=self._DataHandler__ClassLabels,
                                     xvals=self.get_Predictions(PredictionColumn).values,
                                     xval_identifiers=self.get_Labels().values,
                                     weights=self._DataHandler__DataFrame[self._DataHandler__WEIGHTNAME].values,
                                     ClassID=ClassID,
                                     SamplesAreClasses=True,
                                     binningoptimisation=self.Model.get_BinningOptimisation())
        temp_s_stack = THStack()
        temp_b_stack = THStack()
        for key,value in hists.items():
            if value[1]["Type"]=="Data":
                continue
            if value[1]["TrainingLabel"]==ClassID and value[1]["Type"]!="Data":
                temp_s_stack.Add(value[0])
            if value[1]["TrainingLabel"]!=ClassID and value[1]["Type"]!="Data":
                temp_b_stack.Add(value[0])
        s_hist = temp_s_stack.GetStack().Last().Clone("s_hist")
        b_hist = temp_b_stack.GetStack().Last().Clone("b_hist")
        Stack = THStack()
        s_hist.SetLineColor(2) # red
        b_hist.SetLineColor(4) # blue
        s_hist.Scale(1./s_hist.Integral())
        b_hist.Scale(1./b_hist.Integral())
        Stack.Add(s_hist)
        Stack.Add(b_hist)
        Canvas = TCanvas("c1","c1",800,600)
        Legend = TLegend(.63,.75,.85,.85)
        Legend.SetBorderSize(0)
        Legend.SetTextFont(42)
        Legend.SetTextSize(0.035)
        Legend.SetFillStyle(0)
        if self.Model.isClassification():
            if self.isBinary:
               Legend.AddEntry(s_hist,"Signal","L")
            else:
                Legend.AddEntry(s_hist,ClassLabel,"L")
            Legend.AddEntry(b_hist,"Background","L")
        Stack.Draw("hist nostack")
        Stack.GetYaxis().SetTitle("Fraction of Events")
        Stack.GetXaxis().SetTitle(ClassLabel+" Classifier")
        # Let's make sure we have enough space for the labels and the legend
        Stack.SetMaximum(Stack.GetMaximum()*self.SCALEFACTOR)
        Legend.Draw()
        DrawLabels(self.ALabel, self.CMLabel, self.MyLabel, Option="MC")
        CustomLabel(0.2,0.7, "Separation=%.2f%%"%(hf.Separation(s_hist, b_hist)*100)) # We want to get percent
        if self.get_GeneralSettings().get_SeparationPlotScale()=="Log":
            gPad.SetLogy()
        for file_extension in self.get_GeneralSettings().get_PlotFormat():
            Canvas.SaveAs(hf.ensure_trailing_slash(self.get_MVAPlotDirectory())+basename+"_Separation_"+ClassLabel+"."+file_extension)
        # Clean up
        gROOT.FindObject("s_hist").Delete()
        gROOT.FindObject("b_hist").Delete()
        for key in hists.keys():
            gROOT.FindObject(key).Delete()
        Canvas.Close()
        del Canvas

    def __do_SoverBPlot(self, PredictionColumn=None, ClassLabel=None, ClassID=1):
        """
        Function to generate S/B and a S/sqrt(B) plot.

        Keyword arguments:
        PredictionColumn --- Name of the column holding the prediction
        ClassLabel       --- Name of the class itself
        ClassID          --- Label associated to the requested class
        """
        if PredictionColumn is None:
            PredictionColumn=self._DataHandler__PREDICTIONNAME
        basename = self.Model.get_Name()+"_"+self.Model.get_Type()
        PostProcessorMessage("Plotting signal over background plot.")

        basename = self.Model.get_Name()+"_"+self.Model.get_Type()
        Binning = self.ModelVariables[ClassLabel].get_VarBinning()
        if self.Model.isClassification():
            hists = hf.DefineAndFill(Binning,
                                     Samples=self._DataHandler__Samples,
                                     xvals=self.get_Predictions(PredictionColumn).values,
                                     xval_identifiers=self._DataHandler__DataFrame[self._DataHandler__SAMPLENAME].values,
                                     weights=self._DataHandler__DataFrame[self._DataHandler__WEIGHTNAME].values,
                                     ClassID=ClassID,
                                     binningoptimisation=self.Model.get_BinningOptimisation())
        elif self.Model.is3LZReconstruction() or self.Model.is4LZReconstruction():
            hists = hf.DefineAndFill(Binning,
                                     Samples=self.get_ClassLabels(),
                                     xvals=self.get_Predictions(PredictionColumn).values,
                                     xval_identifiers=self.get_Labels().values,
                                     weights=self._DataHandler__DataFrame[self._DataHandler__WEIGHTNAME].values,
                                     ClassID=ClassID,
                                     SamplesAreClasses=True,
                                     binningoptimisation=self.Model.get_BinningOptimisation())
        Canvas = TCanvas("c1","c1",800,600)
        temp_s_stack = THStack()
        temp_b_stack = THStack()
        for key,value in hists.items():
            if value[1]["TrainingLabel"]==ClassID and value[1]["Type"]!="Data":
                temp_s_stack.Add(value[0])
            if value[1]["TrainingLabel"]!=ClassID and value[1]["Type"]!="Data":
                temp_b_stack.Add(value[0])
        s_hist = temp_s_stack.GetStack().Last().Clone("s_hist")
        b_hist = temp_b_stack.GetStack().Last().Clone("b_hist")
        edges = array("d",[s_hist.GetBinLowEdge(bin_id) for bin_id in range(1,s_hist.GetNbinsX()+2)])
        s_over_b_hist = TH1D("","",int(len(edges)-1),edges)
        s_over_sqrtb_hist = TH1D("","",int(len(edges)-1),edges)
        s_over_sqrtb_hist.SetLineColor(4) #blue
        s_over_b_hist.SetLineColor(2) #red
        for b in range(1, s_hist.GetXaxis().GetNbins()+1):
            sum_s_bin_cont = 0
            sum_b_bin_cont = 0
            sum_s_bin_cont = sum([s_hist.GetBinContent(j) for j in range(b, s_hist.GetXaxis().GetNbins()+1)])
            sum_b_bin_cont = sum([b_hist.GetBinContent(j) for j in range(b, b_hist.GetXaxis().GetNbins()+1)])
            if sum_s_bin_cont < 0 or sum_b_bin_cont <= 0:
                sum_s_bin_cont = 0
                WarningMessage("Detected negative bin yields for bin {}. Ratio was set to zero".format(b))
            else:
                s_over_b_hist.SetBinContent(b, sum_s_bin_cont/sum_b_bin_cont)
                s_over_sqrtb_hist.SetBinContent(b, sum_s_bin_cont/np.sqrt(sum_b_bin_cont))
        s_over_b_hist.Draw("hist")
        if not self.isBinary:
            s_over_b_hist.GetXaxis().SetTitle(ClassLabel+" Classifier")
            s_over_sqrtb_hist.GetXaxis().SetTitle(ClassLabel+" Classifier")
        else:
            s_over_b_hist.GetXaxis().SetTitle("DNN Output")
            s_over_sqrtb_hist.GetXaxis().SetTitle("DNN Output")
        s_over_b_hist.GetYaxis().SetTitle("S/B")
        s_over_sqrtb_hist.GetYaxis().SetTitle("S/#sqrt{B}")

        s_over_b_hist.SetMaximum(s_over_b_hist.GetMaximum()*self.SCALEFACTOR)
        DrawLabels(self.ALabel, self.CMLabel, self.MyLabel, Option="MC")
        for file_extension in self.get_GeneralSettings().get_PlotFormat():
            Canvas.SaveAs(hf.ensure_trailing_slash(self.get_MVAPlotDirectory())+basename+ClassLabel+"_SoB_Binary."+file_extension)
        s_over_sqrtb_hist.Draw()
        s_over_sqrtb_hist.SetMaximum(s_over_sqrtb_hist.GetMaximum()*self.SCALEFACTOR)
        DrawLabels(self.ALabel, self.CMLabel, self.MyLabel, Option="MC")
        for file_extension in self.get_GeneralSettings().get_PlotFormat():
            Canvas.SaveAs(hf.ensure_trailing_slash(self.get_MVAPlotDirectory())+basename+ClassLabel+"_SosB_Binary."+file_extension)

        # Doing some cleanup
        gROOT.FindObject("s_hist").Delete()
        gROOT.FindObject("b_hist").Delete()
        for key in hists.keys():
            gROOT.FindObject(key).Delete()
        Canvas.Close()
        del Canvas

    def __do_PermutationImportance(self, PredictionColumn=None, ClassLabel=None, ClassID=1, nPerms=5):
        """
        Generate a horizontal bar chart wich displays the permutation importance.


        Keyword arguments:
        PredictionColumn --- Name of the column holding the prediction
        ClassLabel       --- Name of the class itself
        ClassID          --- Label associated to the requested class
        nPerms           --- Number of permutation to be calculated
        """
        if PredictionColumn is None:
            PredictionColumn=self._DataHandler__PREDICTIONNAME
        if self.isBinary:
            ColumnID = 0
        else:
            ColumnID = ClassID
        PostProcessorMessage("Plotting permutation importance for "+ClassLabel+" classifier.")
        basename = self.Model.get_Name()+"_"
        Canvas = TCanvas("PermutationImportance","PermutationImportance",800,800)
        auc_list_complete = []

        X = self.get_TestInputs(Fold=0).values
        Y_Pred = self.get_TestPredictions(ColumnID=ColumnID, Fold=0)
        Y = self.get_TestLabels(Fold=0).values
        Y[Y!=ClassID] = -99
        Y[Y==ClassID] = 1
        Y[Y==-99] = 0
        nominal_auc = roc_auc_score(np.array(Y), np.array(Y_Pred)) # Calculate the nominal AUC value as a reference value
        for i in range(len(self.get_VarNames())):
            auc_list =[]
            unshuffled = copy.deepcopy(X) # deep copy because shuffle shuffles in place
            for _ in range(nPerms):
                np.random.shuffle(unshuffled[:,i])
                if self.isBinary: # binary case
                    Y_Pred = self.get_ModelObjects()[0].predict(unshuffled)
                else: # multi-class case
                    Y_Pred = self.get_ModelObjects()[0].predict(unshuffled)[:,ClassID]
                auc_list.append((nominal_auc-roc_auc_score(Y, Y_Pred))/nominal_auc)
            auc_list_complete.append(auc_list)

        #Calculate mean:
        auc_list = np.mean(auc_list_complete, axis=1)
        # Sort the AUC list and keep track of indece to apply them on Variables later
        indece = sorted(range(len(auc_list)), key=lambda k: auc_list[k], reverse=True)
        auc_sorted = np.array(auc_list)[indece]
        Variables_sorted = np.array(self.get_VarLabels())[indece]

        # We use a TH1D histogram which we will plot as a hbar chart
        Histogram = TH1D("Histogram","",int(len(auc_sorted)),0,len(auc_sorted))
        Histogram.SetFillColor(4)
        Histogram.SetBarWidth(0.5)
        Histogram.SetBarOffset(0.25) # Since the bar 0.25+0.5+0.25 -> bar is centered with width 0.5
        for bin_ID,mean in enumerate(auc_sorted):
            Histogram.SetBinContent(bin_ID+1,mean)
        Histogram.Draw("hbar")
        Histogram.GetYaxis().SetLabelSize(0.04)
        Histogram.GetXaxis().SetLabelSize(0.015*40/len(auc_list))
        DrawLabels(self.ALabel, self.CMLabel, self.MyLabel, Option="MC", align="top_outside")
        CustomLabel(0.6,0.85, ClassLabel+" Classifier")
        for bin_ID,var in enumerate(Variables_sorted):
            Histogram.GetXaxis().SetBinLabel(bin_ID+1,var.replace(" [GeV]",""))
        gPad.SetLogx()
        gPad.SetGridx(1)
        Canvas.SetRightMargin(0.1)
        Canvas.SetLeftMargin(0.25)
        Histogram.SetMaximum(1.0)
        Histogram.GetYaxis().SetTitle("(AUC_{nom.}-AUC)/AUC_{nom.}")
        for file_extension in self.get_GeneralSettings().get_PlotFormat():
            Canvas.SaveAs(hf.ensure_trailing_slash(self.get_MVAPlotDirectory())+basename+"Permutation_Importance"+ClassLabel+"."+file_extension)
        del Canvas

    def __do_TrainTestPlot(self, PredictionColumn=None, ClassLabel=None, ClassID=1):
        """
        Generate a plot with superimposed test and train results for signal and background.

        Keyword arguments:
        PredictionColumn --- Name of the column holding the prediction
        ClassLabel       --- Name of the class itself
        ClassID          --- Label associated to the requested class
        """
        if PredictionColumn is None:
            PredictionColumn=self._DataHandler__PREDICTIONNAME
        if self.isBinary:
            ColumnID = 0
        else:
            ColumnID = ClassID
        basename = "TrainTest_"+self.Model.get_Name()+"_"+self.Model.get_Type()
        ModelBinning = self.ModelVariables[ClassLabel].get_VarBinning()

        for i in range(self.get_nFolds()):
            PostProcessorMessage("Plotting K-Fold comparison for "+ClassLabel+" classifier, Fold="+str(i))
            Canvas = TCanvas("c1","c1",800,600)
            p_train = self.get_TrainPredictions(ColumnID=ColumnID, Fold=i, returnNonZeroLabels=False)
            y_train = self.get_TrainLabels(Fold=i, returnNonZeroLabels=False).values
            w_train = self.get_TrainWeights(Fold=i, returnNonZeroLabels=False).values
            p_test  = self.get_TestPredictions(ColumnID=ColumnID, Fold=i, returnNonZeroLabels=False)
            y_test  = self.get_TestLabels(Fold=i, returnNonZeroLabels=False).values
            w_test  = self.get_TestWeights(Fold=i, returnNonZeroLabels=False).values
            if self.Model.get_BinningOptimisation() != "None":
                Train_Signal_Hist = TH1D("Train_Signal_Hist_temp"+str(i),"",1000,ModelBinning[0],ModelBinning[-1])
                Test_Signal_Hist = TH1D("Test_Signal_Hist_temp"+str(i),"",1000,ModelBinning[0],ModelBinning[-1])
                Train_Background_Hist = TH1D("Train_Background_Hist_temp"+str(i),"",1000,ModelBinning[0],ModelBinning[-1])
                Test_Background_Hist = TH1D("Test_Background_Hist_temp"+str(i),"",1000,ModelBinning[0],ModelBinning[-1])
                hf.FillHist(Train_Signal_Hist, p_train[y_train==ClassID], w_train[y_train==ClassID])
                hf.FillHist(Test_Signal_Hist, p_test[y_test==ClassID], w_test[y_test==ClassID])
                hf.FillHist(Train_Background_Hist, p_train[y_train!=ClassID], w_train[y_train!=ClassID])
                hf.FillHist(Test_Background_Hist, p_test[y_test!=ClassID], w_test[y_test!=ClassID])
                if self.Model.get_BinningOptimisation() == "TransfoD_symmetric":
                    Transfo = TransfoBinning(hist_b=Train_Background_Hist, hist_s=Train_Signal_Hist, zb=int(len(ModelBinning)/2), zs=int(len(ModelBinning)/2))
                elif self.Model.get_BinningOptimisation() == "TransfoD_flatS":
                    Transfo = TransfoBinning(hist_b=Train_Background_Hist, hist_s=Train_Signal_Hist, zb=int(0), zs=int(len(ModelBinning)))
                elif self.Model.get_BinningOptimisation() == "TransfoD_flatB":
                    Transfo = TransfoBinning(hist_b=Train_Background_Hist, hist_s=Train_Signal_Hist, zb=int(len(ModelBinning)), zs=0)
                optimised_binning = Transfo.TransfoD()
                Train_Signal_Hist = Train_Signal_Hist.Rebin(len(optimised_binning)-1,"Train_Signal_Hist"+str(i),optimised_binning)
                Test_Signal_Hist = Test_Signal_Hist.Rebin(len(optimised_binning)-1,"Test_Signal_Hist"+str(i),optimised_binning)
                Train_Background_Hist = Train_Background_Hist.Rebin(len(optimised_binning)-1,"Train_Background_Hist"+str(i),optimised_binning)
                Test_Background_Hist = Test_Background_Hist.Rebin(len(optimised_binning)-1,"Test_Background_Hist"+str(i),optimised_binning)
            else:
                Train_Signal_Hist = TH1D("Train_Signal_Hist"+str(i),"",int(len(ModelBinning)-1),ModelBinning)
                Test_Signal_Hist = TH1D("Test_Signal_Hist"+str(i),"",int(len(ModelBinning)-1),ModelBinning)
                Train_Background_Hist = TH1D("Train_Background_Hist"+str(i),"",int(len(ModelBinning)-1),ModelBinning)
                Test_Background_Hist = TH1D("Test_Background_Hist"+str(i),"",int(len(ModelBinning)-1),ModelBinning)
                hf.FillHist(Train_Signal_Hist, p_train[y_train==ClassID], w_train[y_train==ClassID])
                hf.FillHist(Test_Signal_Hist, p_test[y_test==ClassID], w_test[y_test==ClassID])
                hf.FillHist(Train_Background_Hist, p_train[y_train!=ClassID], w_train[y_train!=ClassID])
                hf.FillHist(Test_Background_Hist, p_test[y_test!=ClassID], w_test[y_test!=ClassID])

            Legend = TLegend(.63,.80-4*0.025,.85,.90)
            Legend.SetBorderSize(0)
            Legend.SetTextFont(42)
            Legend.SetTextSize(0.035)
            Legend.SetFillStyle(0)

            if self.isBinary:
                Legend.AddEntry(Train_Signal_Hist,"Signal (Train)","p")
                Legend.AddEntry(Test_Signal_Hist,"Signal (Test)","f")
            else:
                Legend.AddEntry(Train_Signal_Hist,ClassLabel+" (Train)","p")
                Legend.AddEntry(Test_Signal_Hist,ClassLabel+" (Test)","f")
            Legend.AddEntry(Train_Background_Hist,"Background (Train)","p")
            Legend.AddEntry(Test_Background_Hist,"Background (Test)","f")
            Train_Signal_Hist.Scale(1./Train_Signal_Hist.Integral())
            Test_Signal_Hist.Scale(1./Test_Signal_Hist.Integral())
            Train_Background_Hist.Scale(1./Train_Background_Hist.Integral())
            Test_Background_Hist.Scale(1./Test_Background_Hist.Integral())
            KS_Signal = Train_Signal_Hist.KolmogorovTest(Test_Signal_Hist)
            KS_Background = Train_Background_Hist.KolmogorovTest(Test_Background_Hist)

            maximum = max(Train_Signal_Hist.GetMaximum(),Test_Signal_Hist.GetMaximum(),Train_Background_Hist.GetMaximum(),Test_Background_Hist.GetMaximum())

            Train_Signal_Hist.SetLineColor(2) # red
            Test_Signal_Hist.SetLineColor(2) # red
            Train_Background_Hist.SetLineColor(4) # blue
            Test_Background_Hist.SetLineColor(4) # blue

            Test_Signal_Hist.SetMarkerSize(0)
            Test_Background_Hist.SetMarkerSize(0)
            Train_Signal_Hist.SetMarkerColor(2)
            Train_Background_Hist.SetMarkerColor(4)

            pad1 = TPad("pad1", "pad1", 0, 0.3, 1, 1.0)
            pad1.SetBottomMargin(0)  # joins upper and lower plot
            pad1.Draw()
            # Lower ratio plot is pad2
            Canvas.cd()  # returns to main canvas before defining pad2
            pad2 = TPad("pad2", "pad2", 0, 0.05, 1, 0.3)
            pad2.SetTopMargin(0)  # joins upper and lower plot
            pad2.SetBottomMargin(0.3)
            pad2.Draw()
            sig_ratio = hf.createRatio(Train_Signal_Hist, Test_Signal_Hist)
            bkg_ratio = hf.createRatio(Train_Background_Hist, Test_Background_Hist)
            sig_errband = sig_ratio.Clone("sig_errband")
            bkg_errband = bkg_ratio.Clone("bkg_errband")
            maxerr = 0.55
            for binID in range(sig_errband.GetNbinsX()+1):
                if sig_errband.GetBinContent(binID)==0:
                    sig_errband.SetBinContent(binID,1)
                    sig_errband.SetBinError(binID,maxerr) # completely fill the ratio plot
                else:
                    sig_errband.SetBinError(binID,sig_errband.GetBinError(binID)/sig_errband.GetBinContent(binID))
                if bkg_errband.GetBinContent(binID)==0:
                    bkg_errband.SetBinContent(binID,1)
                    bkg_errband.SetBinError(binID,maxerr) # completely fill the ratio plot
                else:
                    bkg_errband.SetBinError(binID,bkg_errband.GetBinError(binID)/bkg_errband.GetBinContent(binID))
            sig_errband.SetFillColor(kRed)
            sig_errband.SetFillStyle(3004)
            sig_errband.SetMarkerSize(0)
            bkg_errband.SetFillColor(kBlue)
            bkg_errband.SetFillStyle(3005)
            bkg_errband.SetMarkerSize(0)

            sig_ratio.SetMaximum(1+maxerr)
            sig_ratio.SetMinimum(1-maxerr)
            bkg_ratio.SetMaximum(1+maxerr)
            bkg_ratio.SetMinimum(1-maxerr)
            sig_ratio.SetMarkerSize(0)
            bkg_ratio.SetMarkerSize(0)
            # draw everything in first pad
            pad1.cd()
            Test_Signal_Hist.Draw("hist e")
            Test_Signal_Hist.GetYaxis().SetTitle("Fraction of Events")
            Test_Signal_Hist.GetXaxis().SetTitle("DNN Output")
            Test_Signal_Hist.SetMaximum(maximum*1.5)
            Test_Background_Hist.Draw("hist e SAME")
            Train_Signal_Hist.Draw("e1X0 SAME")
            Train_Background_Hist.Draw("e1X0 SAME")
            if self.ALabel.lower()!="None":
                ATLASLabel(0.175,0.85, self.ALabel)
            CustomLabel(0.175,0.8, self.CMLabel)
            CustomLabel(0.175,0.75, "KS Test: Sig.(Bkg.) P=%.3f (%.3f)"%(KS_Signal, KS_Background))
            if self.MyLabel!="":
                CustomLabel(0.2,0.70, self.MyLabel)
            Legend.Draw("SAME")

            # to avoid clipping the bottom zero, redraw a small axis
            axis = Test_Signal_Hist.GetYaxis()
            axis.ChangeLabel(1, -1, -1, -1, -1, -1, " ")

            # draw everything in second pad
            pad2.cd()
            sig_ratio.Draw("hist")
            bkg_ratio.Draw("hist same")
            sig_errband.Draw("e2 same")
            bkg_errband.Draw("e2 same")
            line = TLine(0,1,1,1)
            line.SetLineStyle(2) # dashed
            line.Draw("Same")
            sig_ratio.GetXaxis().SetTitle(ClassLabel+" Classifier")
            sig_ratio.GetYaxis().SetTitle("Train/Test")

            # switch back to first pad so legend is drawn correctly
            pad1.cd()
            if self.get_GeneralSettings().get_KSPlotScale() == "Log":
                gPad.SetLogy()
            for file_extension in self.get_GeneralSettings().get_PlotFormat():
                Canvas.SaveAs(hf.ensure_trailing_slash(self.get_MVAPlotDirectory())+basename+""+ClassLabel+"_"+str(i)+"."+file_extension)
            del Train_Signal_Hist, Test_Signal_Hist, Train_Background_Hist, Test_Background_Hist, sig_ratio, bkg_ratio, sig_errband, bkg_errband
            Canvas.Close()
            del Canvas

    def __do_CorrelationPlot(self, PredictionColumn=None, ClassLabel=None):
        """
        Function to produce a correlation plot between inputs and classifier outputs.

        Keyword arguments:
        PredictionColumn --- Name of the column holding the prediction
        ClassLabel       --- Name of the class itself
        ClassID          --- Label associated to the requested class
        """
        if PredictionColumn is None:
            PredictionColumn=self._DataHandler__PREDICTIONNAME
        PostProcessorMessage("Plotting correlation plot for "+ClassLabel+" classifier.")
        correlations = [[] for i in range(len(self.get_VarNames()))]
        X = self.get(self.get_VarNames()).values
        if self.Model.isClassification():
            N = self._DataHandler__DataFrame[self._DataHandler__SAMPLENAME].values
        if self.Model.is3LZReconstruction() or self.Model.is4LZReconstruction():
            N = self.get_Labels().values
            L = {i:n for i,n in enumerate(self.get_ClassLabels())}
            L[-2] = "None"
        P = self.get(PredictionColumn).values
        gStyle.SetPaintTextFormat(".2f")
        Canvas = TCanvas("c1","c1",800,600)
        for i, var in enumerate(self.get_VarNames()):
            for n in np.unique(N):
                if n=="Data":
                    continue
                correlations[i].append(np.corrcoef(X[N==n,i],P[N==n])[0][1])
        hist = TH2D("correlationhist","",len(correlations),0,len(correlations),len(correlations[0]),0,len(correlations[0]))
        for xbin in range(np.array(correlations).shape[0]):
            for ybin in range(np.array(correlations).shape[1]):
                hist.SetBinContent(xbin+1,ybin+1,correlations[xbin][ybin])
        hist.Draw("colz text45")
        Yaxis = hist.GetYaxis()
        Xaxis = hist.GetXaxis()
        if self.Model.isClassification():
            for i,Sample_Name in enumerate([n for n in np.unique(N) if n!="Data"]):
                Yaxis.SetBinLabel(i+1,Sample_Name)
        if self.Model.is3LZReconstruction() or self.Model.is4LZReconstruction():
            for i,Class_Name in enumerate([n for n in np.unique(N)]):
                Yaxis.SetBinLabel(i+1,L[Class_Name])

        for i,var in enumerate(self.get_VarLabels()):
            Xaxis.SetBinLabel(i+1,var.replace("[GeV]",""))
        Xaxis.LabelsOption("v")
        hist.SetMaximum(1.0)
        hist.SetMinimum(-1.0)
        Canvas.SetRightMargin(0.15)
        Canvas.SetBottomMargin(0.25)
        hist.GetZaxis().SetTitle("Correlation")
        DrawLabels(self.ALabel, self.CMLabel, self.MyLabel, Option="MC", align="top_outside")
        for file_extension in self.get_GeneralSettings().get_PlotFormat():
            Canvas.SaveAs(hf.ensure_trailing_slash(self.get_MVAPlotDirectory())+"Correlation_"+ClassLabel+"."+file_extension)
        Canvas.Close()
        del Canvas

    def __do_BinaryConfusionPlot(self, PredictionColumn=None, ClassLabel=None, ClassID=1):
        """
        Generate a 2D colour plot for all samples to study samples that lead to confusion

        Keyword arguments:
        PredictionColumn --- Name of the column holding the prediction
        ClassLabel       --- Name of the class itself
        ClassID          --- LaAccuracyto the requested class
        """
        if PredictionColumn is None:
            PredictionColumn=self._DataHandler__PREDICTIONNAME
        PostProcessorMessage("Plotting binary confusion plot for "+ClassLabel+" classifier.")
        basename = self.Model.get_Name()+"_"
        Binning = self.ModelVariables[ClassLabel].get_VarBinning()
        gStyle.SetPaintTextFormat(".2f")
        Canvas = TCanvas("c1","c1",800,600)

        if self.Model.isClassification():
            hists = hf.DefineAndFill(Binning,
                                     Samples=self._DataHandler__Samples,
                                     xvals=self.get_Predictions(PredictionColumn).values,
                                     xval_identifiers=self._DataHandler__DataFrame[self._DataHandler__SAMPLENAME].values,
                                     weights=self._DataHandler__DataFrame[self._DataHandler__WEIGHTNAME].values,
                                     ClassID=ClassID,
                                     binningoptimisation=self.Model.get_BinningOptimisation())
        elif self.Model.is3LZReconstruction() or self.Model.is4LZReconstruction():
            hists = hf.DefineAndFill(Binning,
                                     Samples=self._DataHandler__ClassLabels,
                                     xvals=self.get_Predictions(PredictionColumn).values,
                                     xval_identifiers=self.get_Labels().values,
                                     weights=self._DataHandler__DataFrame[self._DataHandler__WEIGHTNAME].values,
                                     ClassID=ClassID,
                                     SamplesAreClasses=True,
                                     binningoptimisation=self.Model.get_BinningOptimisation())
        edges = array("d",[list(hists.values())[0][0].GetBinLowEdge(bin_id) for bin_id in range(1,list(hists.values())[0][0].GetNbinsX()+2)])
        if self.Model.isClassification():
            N_dict = {n:i for i,n in enumerate(np.unique(self.get_Sample_Names().values))}
            W_dict = {n:1/np.sum(self.get_Weights().values[self.get_Sample_Names().values==n]) for n in np.unique(self.get_Sample_Names().values)}
            hist = TH2D("hist","hist",int(len(edges)-1),edges,len(N_dict),0,len(N_dict))
            for y_pred,n,w in zip(self.get(PredictionColumn).values,self.get_Sample_Names().values,self.get_Weights().values):
                hist.Fill(y_pred,N_dict[n],w*W_dict[n]*100)
        elif self.Model.is3LZReconstruction() or self.Model.is4LZReconstruction():
            N_dict = {n:i+1 for i,n in enumerate(self.get_ClassLabels())} # keep Bin 0 for "no class"
            N_dict["None"] = 0
            L_dict = {n:i for i,n in enumerate(np.unique(self.get_Labels()))}
            W_dict = {n:1/np.sum(self.get_Weights().values[self.get_Labels().values==n]) for n in np.unique(self.get_Labels())}
            hist = TH2D("hist","hist",int(len(edges)-1),edges,len(N_dict),0,len(N_dict))
            for y_pred,n,w in zip(self.get(PredictionColumn).values,self.get_Labels().values,self.get_Weights().values):
                hist.Fill(y_pred,L_dict[n],w*W_dict[n]*100)
        Yaxis = hist.GetYaxis()
        for k,v in N_dict.items():
            Yaxis.SetBinLabel(v+1,k)
        hist.Draw("colz")
        Canvas.SetRightMargin(0.15)
        hist.GetZaxis().SetTitle("Fraction of Events [%]")
        hist.GetXaxis().SetTitle(ClassLabel+" Classifier Output")
        DrawLabels(self.ALabel, self.CMLabel, self.MyLabel, Option="MC", align="top_outside")
        if self.get_GeneralSettings().get_ConfusionPlotScale() == "Log":
            gPad.SetLogz()
        for file_extension in self.get_GeneralSettings().get_PlotFormat():
            Canvas.SaveAs(hf.ensure_trailing_slash(self.get_MVAPlotDirectory())+basename+"Confusion_"+ClassLabel+"."+file_extension)
        Canvas.Close()
        del Canvas

    def __do_ROCCurvePlotCombined(self, PredictionColumn=None, ClassLabel=None, ClassID=1):
        """
        Function to draw all ROC curves in one plot.

        Keyword arguments:
        PredictionColumn --- Name of the column holding the prediction
        ClassLabel       --- Name of the class itself
        ClassID          --- Label associated to the requested class
        """
        if PredictionColumn is None:
            PredictionColumn=self._DataHandler__PREDICTIONNAME
        if self.isBinary:
            ColumnID = 0
        else:
            ColumnID = ClassID

        Canvas = TCanvas("ROCCurvePlotCombined","ROCCurvePlotCombined",800,600)
        Legend = TLegend(.63,.75-0.04*self.get_nFolds(),.85,.85) # Legend
        Legend.SetBorderSize(0)
        Legend.SetTextFont(42)
        Legend.SetTextSize(0.035)
        Legend.SetFillStyle(0)
        mg = TMultiGraph()
        TrainGraphs, TestGraphs, TrainAUCs, TestAUCs  = list(), list(), list(), list()
        PostProcessorMessage("Plotting ROC curve for "+ClassLabel+" classifier")
        basename = self.Model.get_Name()+"_"
        for i in range(self.get_nFolds()):
            test_mvaTargets  = np.array((self.get_TestLabels(Fold=i, returnNonZeroLabels=True).values==ClassID).tolist(), dtype=bool)
            train_mvaTargets = np.array((self.get_TrainLabels(Fold=i, returnNonZeroLabels=True).values==ClassID).tolist(), dtype=bool)
            test_mvaValues   = array('f',self.get_TestPredictions(ColumnID=ColumnID,Fold=i, returnNonZeroLabels=True).tolist())
            train_mvaValues  = array('f',self.get_TrainPredictions(ColumnID=ColumnID,Fold=i, returnNonZeroLabels=True).tolist())
            test_mvaWeights  = array('f',self.get_TestWeights(Fold=i, returnNonZeroLabels=True).values.tolist())
            train_mvaWeights = array('f',self.get_TrainWeights(Fold=i, returnNonZeroLabels=True).values.tolist())
            train_fpr, train_tpr, _ = roc_curve(train_mvaTargets, train_mvaValues)
            test_fpr, test_tpr, _ = roc_curve(test_mvaTargets, test_mvaValues)
            train_auc = auc(train_fpr, train_tpr)
            test_auc = auc(test_fpr, test_tpr)
            sampling = int(len(train_fpr)/100)
            TrainCurve = TGraph(len(train_fpr[::sampling]),array('f',train_fpr[::sampling]),array('f',train_tpr[::sampling]))
            TestCurve = TGraph(len(test_fpr[::sampling]),array('f',test_fpr[::sampling]),array('f',test_tpr[::sampling]))
            TrainGraphs.append(copy.deepcopy(TrainCurve))
            TestGraphs.append(copy.deepcopy(TestCurve))
            # Now we calculate the AUC using 100 points as the granularity instead of 30 to be more precise
            TrainAUCs.append(train_auc)
            TestAUCs.append(test_auc)
            Legend.AddEntry(TrainGraphs[i],"Training %d (AUC=%.3f)"%(i+1,TrainAUCs[i]),"L")
            Legend.AddEntry(TestGraphs[i],"Testing %d (AUC=%.3f)"%(i+1,TestAUCs[i]),"L")
            TrainGraphs[i].SetLineColor(kBlue-10+i*2) #blue(ish), we subtract 10 to use the "full" colour wheel
            TestGraphs[i].SetLineColor(kRed-10+i*2) #red(ish), we subtract 10 to use the "full" colour wheel

            mg.Add(TrainGraphs[i])
            mg.Add(TestGraphs[i])

        # We need some space for the labels
        mg.GetYaxis().SetRangeUser(0.,1.7)
        mg.GetXaxis().SetRangeUser(0.,1.)
        # Setting axis titles
        mg.GetXaxis().SetTitle("True Positive Rate")
        mg.GetYaxis().SetTitle("False Positive Rate")
        mg.Draw("AL")
        Legend.Draw("Same")
        DrawLabels(self.ALabel, self.CMLabel, self.MyLabel, Option="MC")
        CustomLabel(0.2,0.70, ClassLabel)
        for file_extension in self.get_GeneralSettings().get_PlotFormat():
            Canvas.SaveAs(hf.ensure_trailing_slash(self.get_MVAPlotDirectory())+basename+"ROCCurve_Combined_"+ClassLabel+"."+file_extension)
        Canvas.Close()
        del Canvas

    def __do_AUCSummary(self, PredictionColumn=None, ClassLabel=None, ClassID=1):
        """
        Function to draw all ROC curves in one plot.

        Keyword arguments:
        PredictionColumn --- Name of the column holding the prediction
        ClassLabel       --- Name of the class itself
        ClassID          --- Label associated to the requested class
        """
        if PredictionColumn is None:
            PredictionColumn=self._DataHandler__PREDICTIONNAME
        basename = self.Model.get_Name()+"_"+self.Model.get_Type()

        Canvas = TCanvas("AUCSummary","AUCSummary",800,600)
        gStyle.SetPaintTextFormat(".4f")

        # get rid of data
        P = self.get(PredictionColumn).values[self.get_Sample_Types().values!="Data"]
        W = self.get_Weights().values[self.get_Sample_Types().values!="Data"]
        if self.Model.isClassification():
            N = self.get_Sample_Names().values[self.get_Sample_Types().values!="Data"]
            Y = self.get_Labels().values[self.get_Sample_Types().values!="Data"]
            T = self.get_Sample_Types().values[self.get_Sample_Types().values!="Data"]
        if self.Model.is3LZReconstruction() or self.Model.is4LZReconstruction():
            N = self.get_Labels().values[self.get_Sample_Types().values!="Data"]
            Y = self.get_Labels().values[self.get_Sample_Types().values!="Data"]
        if ClassLabel is None:
            NT_dict = dict(zip(N,T))
            Background = [n for n,t in NT_dict.items() if t=="Background"]
            Y[Y!=1] = 0
            AUC = []
            for bkg in Background:
                mvaValues = array('f', P[(N==bkg) | (T=="Signal")])
                mvaTargets = array('b', Y[(N==bkg) | (T=="Signal")])
                mvaWeights = array('f', W[(N==bkg) | (T=="Signal")])

                AUC.append(TMVA.ROCCurve(   ClassificationPlotHandler.array_to_cpp_vector("float", mvaValues),
                                            ClassificationPlotHandler.array_to_cpp_vector("bool",  mvaTargets),
                                            ClassificationPlotHandler.array_to_cpp_vector("float", mvaWeights).GetROCIntegral()))
            PostProcessorMessage("Plotting AUC summary.")
        else:
            if self.Model.isClassification():
                NY_dict = dict(zip(N,Y))
                NT_dict = dict(zip(N,T))
                Background = [n for n,y in NY_dict.items() if y!=ClassID and NT_dict[n]!="Data"]
            elif self.Model.is3LZReconstruction() or self.Model.is4LZReconstruction():
                Name_dict = {Class:ClassLabel for Class, ClassLabel in enumerate(self.Model.get_ClassLabels())}
                Name_dict[-2] = "None"
                Background = np.unique([y for y in Y if y!=ClassID])
            Y[Y!=ClassID] = -99
            Y[Y==ClassID] = 1
            Y[Y==-99] = 0
            AUC = []
            for bkg in Background:
                mvaValues = array('f', P[(N==bkg) | (Y==1)])
                mvaTargets = array('b', Y[(N==bkg) | (Y==1)])
                mvaWeights = array('f', W[(N==bkg) | (Y==1)])
                AUC.append(TMVA.ROCCurve(   ClassificationPlotHandler.array_to_cpp_vector("float",mvaValues),
                                            ClassificationPlotHandler.array_to_cpp_vector("bool", mvaTargets),
                                            ClassificationPlotHandler.array_to_cpp_vector("float",mvaWeights)).GetROCIntegral())
            PostProcessorMessage("Plotting AUC summary for "+ClassLabel+" classifier.")
        # Sort the AUC list and keep track of indece
        indece = sorted(range(len(AUC)), key=lambda k: AUC[k], reverse=True)
        AUC_sorted = np.array(AUC)[indece]
        Background_sorted = np.array(Background)[indece]

        # We use a TH1D histogram which we will plot as a hbar chart
        Histogram = TH1D("Histogram","",int(len(AUC_sorted)),0,len(AUC_sorted))
        Histogram.SetFillColor(4)
        Histogram.SetBarWidth(0.5)
        Histogram.SetBarOffset(0.25) # Since the bar 0.25+0.5+0.25 -> bar is centered with width 0.5
        for bin_ID,mean in enumerate(AUC_sorted):
            Histogram.SetBinContent(bin_ID+1,mean)
        Histogram.Draw("hist Text")
        DrawLabels(self.ALabel, self.CMLabel, self.MyLabel, Option="MC", align="left")
        for bin_ID in range(len(Background_sorted)):
            if self.Model.isClassification():
                Histogram.GetXaxis().SetBinLabel(bin_ID+1,str(Background_sorted[bin_ID]))
            if self.Model.is3LZReconstruction() or self.Model.is4LZReconstruction():
                Histogram.GetXaxis().SetBinLabel(bin_ID+1,Name_dict[Background_sorted[bin_ID]])
        Histogram.GetYaxis().SetTitle("AUC")
        Histogram.GetXaxis().SetTitle("Background")
        Histogram.SetMaximum(1.3)
        Histogram.SetMinimum(0.5)
        CustomLabel(0.2,0.7, ClassLabel+" Classifier") # To clarify the classifier in multi-class cases
        for file_extension in self.get_GeneralSettings().get_PlotFormat():
            Canvas.SaveAs(hf.ensure_trailing_slash(self.get_MVAPlotDirectory())+basename+"_AUCSummary_"+ClassLabel+"."+file_extension)
        del Canvas

    def __do_WeightedAccuracy(self, PredictionColumn = None, ClassLabel = None, ClassID = None):
        if PredictionColumn is None:
            PredictionColumn=self._DataHandler__PREDICTIONNAME
        Categories = self.get_Sample_Names()
        Pred = self.get_Predictions(self.get_ClassLabels()).values
        Labels = self.get_Labels().values
        Weights = self.get_Weights().values
        basename = self.Model.get_Name()+"_"+self.Model.get_Type()
        PostProcessorMessage("Plotting Accuracy plot for "+ClassLabel+" classifier.")
        accuracy = {}
        precision = {}
        recall = {}
        for Category in np.unique(Categories):
            Y = Pred[Categories == Category]
            N = Labels[Categories == Category]
            W = Weights[Categories == Category]
            TP = FP = FN = TN = noclass = 0
            for y,n,w in zip(Y,N,W):
                class_pred = np.argmax(y)
                if class_pred == ClassID and n == ClassID:
                    TP += w
                elif class_pred == ClassID and n != ClassID:
                    FP += w
                elif class_pred != ClassID and n == ClassID:
                    FN += w
                elif class_pred != ClassID and n != ClassID and n != -2:
                    TN += w
                elif n == -2:
                    noclass += w
            accuracy[Category] = (TP + TN)/(noclass + TP + TN + FP + FN)
            if TP != 0 or FP != 0:
                precision[Category] = TP/(TP + FP)
            else: precision[Category] = 0
            if TP != 0 or FN != 0:
                recall[Category] = TP/(TP + FN)
            else: recall[Category] = 0
        Hist_Acc = TH1D("HistAcc" + ClassLabel, "", len(accuracy), 0, len(accuracy))
        Hist_Prec = TH1D("HistPrec" + ClassLabel, "", len(accuracy), 0, len(accuracy))
        Hist_Rec = TH1D("HistRec" + ClassLabel, "", len(accuracy), 0, len(accuracy))
        Hist_Acc.SetFillColor(4) #blue
        Hist_Prec.SetFillColor(2) #red
        Hist_Rec.SetFillColor(3) #green
        Canvas = TCanvas("Accuracy","Accuracy",800,600)
        gStyle.SetPaintTextFormat(".4f")
        for binID, Category in zip(range(1,len(accuracy)+1),accuracy.keys()):
            Hist_Acc.SetBinContent(binID, accuracy[Category])
            Hist_Prec.SetBinContent(binID, precision[Category])
            Hist_Rec.SetBinContent(binID, recall[Category])
        Stack = THStack()
        Stack.Add(Hist_Acc)
        Stack.Add(Hist_Prec)
        Stack.Add(Hist_Rec)
        Stack.Draw("NOSTACKB TEXT")
        for binID, Category in zip(range(1,Hist_Acc.GetNbinsX()+1),accuracy.keys()):
            Stack.GetXaxis().SetBinLabel(binID, Category)
        Stack.SetMaximum(1.5)
        Stack.SetMinimum(0)
        Stack.GetXaxis().SetTitle("Sample")
        Stack.GetYaxis().SetTitle("Accuracy/Precision/Recall")
        CustomLabel(0.2,0.8, ClassLabel + " Classifier")
        for file_extension in self.get_GeneralSettings().get_PlotFormat():
            Canvas.SaveAs(hf.ensure_trailing_slash(self.get_MVAPlotDirectory())+basename+"_Accuracy_"+ClassLabel+"."+file_extension)
        del Canvas
        del Hist_Acc
        del Hist_Prec
        del Hist_Rec

    def __do_YieldsPlot(self):
        W = self.get_Weights().values
        N_evts = np.ones(len(W))
        basename = self.Model.get_Name()+"_"+self.Model.get_Type()
        PostProcessorMessage("Plotting class distribution plot.")
        Canvas = TCanvas("ClassDistr", "ClassDistr", 800, 600)
        if self.Model.is3LZReconstruction() or self.Model.is4LZReconstruction():
            Y = self.get_Labels().values
            N = self.get_ClassLabels()
            Yields_Classes = {}
            N_evts_Classes = {}
            for y,n in enumerate(N):
                W_Class = W[Y==y]
                N_evts_Class = N_evts[Y==y]
                Yields_Classes[n] = sum(W_Class)
                N_evts_Classes[n] = sum(N_evts_Class)
            Hist_ClassTotalYields = TH1D("Hist_Total_class", "", len(Yields_Classes), 0, len(Yields_Classes))
            Hist_ClassTotalYields_unw = TH1D("Hist_Total_class_unw", "", len(Yields_Classes), 0, len(Yields_Classes))
            Hist_ClassRelYields = TH1D("Hist_Rel_class", "", len(Yields_Classes), 0, len(Yields_Classes))
            hf.FillYieldHist(Hist_ClassTotalYields, Yields_Classes)
            hf.FillYieldHist(Hist_ClassRelYields, Yields_Classes, total_number_events = sum(W[Y!=-2]))
            hf.FillYieldHist(Hist_ClassTotalYields_unw, N_evts_Classes)
            Hist_ClassTotalYields.GetXaxis().SetTitle("Classes")
            Hist_ClassRelYields.GetXaxis().SetTitle("Classes")
            Hist_ClassTotalYields_unw.GetXaxis().SetTitle("Classes")
            Hist_ClassTotalYields_unw.GetYaxis().SetTitleOffset(1.6)
            Hist_ClassTotalYields.SetMarkerSize(2)
            Hist_ClassTotalYields_unw.SetMarkerSize(2)
            Hist_ClassRelYields.SetMarkerSize(2)
            gStyle.SetPaintTextFormat(".4f")
            Hist_ClassTotalYields.Draw("HIST TEXT")
            for file_extension in self.get_GeneralSettings().get_PlotFormat():
                Canvas.SaveAs(hf.ensure_trailing_slash(self.get_MVAPlotDirectory())+basename+"_Classes_Yields."+file_extension)
            Hist_ClassRelYields.Draw("HIST TEXT")
            for file_extension in self.get_GeneralSettings().get_PlotFormat():
                Canvas.SaveAs(hf.ensure_trailing_slash(self.get_MVAPlotDirectory())+basename+"_Classes_Relative_Yields."+file_extension)
            gStyle.SetPaintTextFormat(".0f")
            Hist_ClassTotalYields_unw.Draw("HIST TEXT")
            for file_extension in self.get_GeneralSettings().get_PlotFormat():
                Canvas.SaveAs(hf.ensure_trailing_slash(self.get_MVAPlotDirectory())+basename+"_Classes_Yields_unw."+file_extension)
            del Hist_ClassTotalYields
            del Hist_ClassRelYields
            del Hist_ClassTotalYields_unw
        if len(np.unique(self.get_Sample_Names())) > 1:
            S = self.get_Sample_Names().values
            N = np.unique(self.get_Sample_Names())
            Yields_Samples = {}
            N_evts_Samples = {}
            for n in N:
                W_Sample = W[S==n]
                N_evts_Sample = N_evts[S==n]
                Yields_Samples[n] = sum(W_Sample)
                N_evts_Samples[n] = sum(N_evts_Sample)
            Hist_SampleTotalYields = TH1D("Hist_Total_sample", "", len(Yields_Samples), 0, len(Yields_Samples))
            Hist_SampleTotalYields_unw = TH1D("Hist_Total_sample_unw", "", len(N_evts_Samples), 0, len(N_evts_Samples))
            Hist_SampleRelYields = TH1D("Hist_Rel_sample", "", len(Yields_Samples), 0, len(Yields_Samples))
            hf.FillYieldHist(Hist_SampleTotalYields, Yields_Samples)
            hf.FillYieldHist(Hist_SampleTotalYields_unw, N_evts_Samples)
            hf.FillYieldHist(Hist_SampleRelYields, Yields_Samples, total_number_events = sum(W))
            Hist_SampleTotalYields.GetXaxis().SetTitle("Samples")
            Hist_SampleRelYields.GetXaxis().SetTitle("Samples")
            Hist_SampleTotalYields_unw.GetXaxis().SetTitle("Samples")
            Hist_SampleTotalYields_unw.GetYaxis().SetTitleOffset(1.6)
            Hist_SampleTotalYields.SetMarkerSize(2)
            Hist_SampleTotalYields_unw.SetMarkerSize(2)
            Hist_SampleRelYields.SetMarkerSize(2)
            gStyle.SetPaintTextFormat(".4f")
            Hist_SampleTotalYields.Draw("HIST TEXT")
            for file_extension in self.get_GeneralSettings().get_PlotFormat():
                Canvas.SaveAs(hf.ensure_trailing_slash(self.get_MVAPlotDirectory())+basename+"_Samples_Yields."+file_extension)
            Hist_SampleRelYields.Draw("HIST TEXT")
            for file_extension in self.get_GeneralSettings().get_PlotFormat():
                Canvas.SaveAs(hf.ensure_trailing_slash(self.get_MVAPlotDirectory())+basename+"_Samples_Relative_Yields."+file_extension)
            gStyle.SetPaintTextFormat(".0f")
            Hist_SampleTotalYields_unw.Draw("HIST TEXT")
            for file_extension in self.get_GeneralSettings().get_PlotFormat():
                Canvas.SaveAs(hf.ensure_trailing_slash(self.get_MVAPlotDirectory())+basename+"_Samples_Yields_unw."+file_extension)
            del Hist_SampleTotalYields
            del Hist_SampleRelYields
            del Hist_SampleTotalYields_unw
        del Canvas

    def __do_EfficiencyPlot(self):
        basename = self.Model.get_Name()+"_"+self.Model.get_Type()
        PostProcessorMessage("Plotting efficiency plot")
        efficiency = {}
        if self.isBinary:
            PredictionColumn = self._DataHandler__PREDICTIONNAME
        else:
            PredictionColumn = self.get_ClassLabels()
        for Sample in np.unique(self.get_Sample_Names()):
            Y = self.get_Predictions(PredictionColumn).values[self.get_Sample_Names() == Sample]
            N = self.get_Labels().values[self.get_Sample_Names() == Sample]
            W = self.get_Weights().values[self.get_Sample_Names() == Sample]
            correct = incorrect = noclass = 0
            for y,n,w in zip(Y,N,W):
                class_pred = np.argmax(y)
                if class_pred == n:
                    correct += w
                elif class_pred != n and n != -2:
                    incorrect += w
            efficiency[Sample] = (correct)/(correct + incorrect)
        Hist_Eff = TH1D("HistEff", "", len(efficiency), 0, len(efficiency))
        Hist_Eff.SetFillColor(4) #blue
        Canvas = TCanvas("Efficiency","Efficiency",800,600)
        gStyle.SetPaintTextFormat(".4f")
        for binID, Sample in zip(range(1,len(efficiency)+1),efficiency.keys()):
            Hist_Eff.SetBinContent(binID, efficiency[Sample])
        Hist_Eff.Draw("HIST TEXT")
        for binID, Sample in zip(range(1,Hist_Eff.GetNbinsX()+1),efficiency.keys()):
            Hist_Eff.GetXaxis().SetBinLabel(binID, Sample)
        Hist_Eff.SetMaximum(1.5)
        Hist_Eff.SetMinimum(0)
        Hist_Eff.SetMarkerSize(2)
        Hist_Eff.GetXaxis().SetTitle("Sample")
        Hist_Eff.GetYaxis().SetTitle("Efficiency")
        for file_extension in self.get_GeneralSettings().get_PlotFormat():
            Canvas.SaveAs(hf.ensure_trailing_slash(self.get_MVAPlotDirectory())+basename+"_Efficiencies."+file_extension)
        del Canvas
        del Hist_Eff

    def __do_ConfusionMatrix(self):
        """
        Produces a confusion matrix plot
        """
        PostProcessorMessage("Plotting Confusion matrix")
        gStyle.SetPadRightMargin(0.17)
        if self.isBinary:
            Y_pred = [round(pred) for pred in self.get_Predictions(self._DataHandler__PREDICTIONNAME)[self.get_Labels().values >= 0]]
            Y_true = self.get_Labels().values[self.get_Labels().values >= 0]
            Classes = ["Background", "Signal"]
            W = self.get_Weights()[self.get_Labels().values >= 0]
        else:
            Probs = self.get_Predictions(self.get_ClassLabels()).values
            Y_pred = [np.argmax(prob) for prob in Probs[self.get_Labels().values >= 0]]
            Y_true = self.get_Labels().values[self.get_Labels().values >= 0]
            Classes =  self.get_ClassLabels()
            W = self.get_Weights()[self.get_Labels().values >= 0]
        ConfMatrix = confusion_matrix(Y_true, Y_pred, normalize = "true", sample_weight = W)
        ConfMatrix_Hist  = TH2D("ConfMat", "", len(Classes), 0, len(Classes), len(Classes), 0,len(Classes))
        for binIDx, _ in enumerate(Classes):
            ConfMatrix_Hist.GetXaxis().SetBinLabel(binIDx+1, Classes[binIDx])
            ConfMatrix_Hist.GetYaxis().SetBinLabel(binIDx+1, Classes[binIDx])
            for binIDy in range(len(Classes)):
                ConfMatrix_Hist.SetBinContent(binIDx+1, binIDy+1, ConfMatrix[binIDy][binIDx])
        ConfMatrix_Hist.GetXaxis().SetTitle("Prediction")
        ConfMatrix_Hist.GetYaxis().SetTitle("True Label")
        ConfMatrix_Hist.GetZaxis().SetTitle("Fraction of Events")
        ConfMatrix_Hist.SetMarkerSize(2)
        Canvas = TCanvas("ConfMatrix","ConfMatrix",1000,600)
        gStyle.SetPaintTextFormat(".4f")
        ConfMatrix_Hist.Draw("colz text")
        DrawLabels(self.ALabel, self.CMLabel, self.MyLabel, Option="MC", align="top_outside")
        for file_extension in self.get_GeneralSettings().get_PlotFormat():
            Canvas.SaveAs(hf.ensure_trailing_slash(self.get_MVAPlotDirectory())+self.Model.get_Name()+"_"+self.Model.get_Type()+"_Confusion_matrix."+file_extension)
        del Canvas
        del ConfMatrix_Hist

"""
Module handling control plots
"""

from ROOT import TH1D, TH2D, TCanvas, THStack, TLegend, TLine, gStyle, gPad, gROOT, TPad, kBlue, kRed
import numpy as np
import pandas as pd
import HelperModules.HelperFunctions as hf
import HelperModules.AtlasStyle
from HelperModules.AtlasLabel import ATLASLabel, CustomLabel, DrawLabels
from HelperModules.DataHandler import DataHandler
from HelperModules.MessageHandler import ConverterMessage

class CtrlPlotHandler(DataHandler):
    """
    Class handling control plots
    """
    def __init__(self,Settings):
        super().__init__(Settings)
        self.__ALabel  = self._DataHandler__GeneralSettings.get_ATLASLabel()
        self.__CMLabel = self._DataHandler__GeneralSettings.get_CMLabel()
        self.__MyLabel = self._DataHandler__GeneralSettings.get_CustomLabel()
        self.__CorCut  = self._DataHandler__GeneralSettings.get_LargeCorrelation()

        self.__SCALEFACTOR          = 1.4
        self.__SCALEFACTOR_DATAHIST = 1.7
        self.__RATIOPLOTMAX         = 1.55
        self.__RATIOPLOTMIN         = 0.45
        gROOT.SetBatch()

    ###########################################################################
    ############################ Wrapper functions ############################
    ###########################################################################
    def do_CtrlPlots(self):
        """
        Wrapper function to produce all available plots in one go.
        """
        self.do_TrainingStats()
        self.do_DataMCPlots()
        self.do_TrainTestPlots()
        self.do_CorrelationMatrix_plot()
        if len(self._DataHandler__Samples)!=1:
            self.do_Separation2D()

    def do_DataMCPlots(self):
        """
        Wrapper function to produce all Data/MC plots in one go.
        """
        for variable in self.get_VarObjects():
            self.do_DataMCPlot(variable)

    def do_TrainTestPlots(self):
        """
        Wrapper function to produce all Train/Test for all variables plots in one go.
        """
        for variable in self.get_VarObjects():
            self.do_TrainTestPlot(variable)

    ###########################################################################
    ########################### Plotting functions ############################
    ###########################################################################
    def do_CorrelationMatrix_plot(self):
        """
        Function to produce a 2D correlation matrix plot between the input variables.
        """
        ConverterMessage("Plotting correlation matrices and exporting correlations to tables for the "+ self.get_ModelSettings().get_Type() + " input variables.")
        correlations = [[] for i in range(len(self.get_VarNames()))]
        gStyle.SetPaintTextFormat(".2f")
        Canvas = TCanvas("c1","c1",1000,1000)
        correlations = self.get(self.get_VarNames()).corr()
        correlations.to_csv(hf.ensure_trailing_slash(self.get_CtrlPlotDirectory())+"CorrelationMatrix.csv")
        Large_correlations_vars = [var for var in self.get_VarNames() if any([True for x in correlations[var].values if (x>self.__CorCut)&(x!=1)])]
        Large_correlations = correlations[Large_correlations_vars].corr()
        Large_correlations.to_csv(hf.ensure_trailing_slash(self.get_CtrlPlotDirectory())+"LargeCorrelationsMatrix.csv")
        hist_correlations = TH2D("Correlations2Dhist","",len(correlations),0,len(correlations),len(correlations),0,len(correlations))
        hist_large_correlations = TH2D("LargeCorrelations2Dhist","",len(Large_correlations),0,len(Large_correlations),len(Large_correlations),0,len(Large_correlations))
        for xbin, var1 in enumerate(self.get_VarNames()):
            for ybin, var2 in enumerate(self.get_VarNames()):
                hist_correlations.SetBinContent(xbin+1,ybin+1,correlations[var1][var2])
        for xbin, var1 in enumerate(Large_correlations.keys()):
            for ybin, var2 in enumerate(Large_correlations.keys()):
                hist_large_correlations.SetBinContent(xbin+1,ybin+1,Large_correlations[var1][var2])
        ### Drawing all correlations ###
        hist_correlations.Draw("colz text45")
        hist_correlations.SetMarkerSize(0.4*30/len(self.get_VarNames()))
        hist_correlations.GetXaxis().SetLabelSize(0.01*40/len(self.get_VarNames()))
        hist_correlations.GetYaxis().SetLabelSize(0.01*40/len(self.get_VarNames()))
        hist_correlations.GetZaxis().SetLabelSize(0.04)
        Yaxis = hist_correlations.GetYaxis()
        Xaxis = hist_correlations.GetXaxis()
        for i,var in enumerate(self.get_VarObjects()):
            Xaxis.SetBinLabel(i+1,var.get_Label().replace("[GeV]",""))
            Yaxis.SetBinLabel(i+1,var.get_Label().replace("[GeV]",""))
        Xaxis.LabelsOption("v")
        hist_correlations.SetMaximum(1.0)
        hist_correlations.SetMinimum(-1.0)
        Canvas.SetRightMargin(0.15)
        Canvas.SetBottomMargin(0.30)
        Canvas.SetLeftMargin(0.30)
        hist_correlations.GetZaxis().SetTitle("Correlation")
        DrawLabels(self.__ALabel, self.__CMLabel, self.__MyLabel, Option="MC", align="top_outside")
        for file_extension in self.get_GeneralSettings().get_PlotFormat():
            Canvas.SaveAs(hf.ensure_trailing_slash(self.get_CtrlPlotDirectory())+"CorrelationMatrix."+file_extension)
        del Canvas
        ### Drawing only large correlations ###
        Canvas = TCanvas("c1","c1",1000,1000)
        hist_large_correlations.Draw("colz text45")
        if len(Large_correlations.keys())>0:
            hist_large_correlations.SetMarkerSize(0.4*40/len(Large_correlations.keys()))
            hist_large_correlations.GetXaxis().SetLabelSize(0.01*40/len(Large_correlations.keys()))
            hist_large_correlations.GetYaxis().SetLabelSize(0.01*40/len(Large_correlations.keys()))
            hist_large_correlations.GetZaxis().SetLabelSize(0.04)
            Yaxis = hist_large_correlations.GetYaxis()
            Xaxis = hist_large_correlations.GetXaxis()
            for i,variable_label in enumerate(Large_correlations.keys()):
                Xaxis.SetBinLabel(i+1,variable_label.replace("[GeV]",""))
                Yaxis.SetBinLabel(i+1,variable_label.replace("[GeV]",""))
            Xaxis.LabelsOption("v")
            hist_large_correlations.SetMaximum(1.0)
            hist_large_correlations.SetMinimum(-1.0)
            Canvas.SetRightMargin(0.15)
            Canvas.SetBottomMargin(0.30)
            Canvas.SetLeftMargin(0.30)
            hist_large_correlations.GetZaxis().SetTitle("Correlation")
            DrawLabels(self.__ALabel, self.__CMLabel, self.__MyLabel, Option="MC", align="top_outside")
            for file_extension in self.get_GeneralSettings().get_PlotFormat():
                Canvas.SaveAs(hf.ensure_trailing_slash(self.get_CtrlPlotDirectory())+"LargeCorrelationsMatrix."+file_extension)
            del Canvas

    def do_TrainingStats(self):
        """
        Function to produce a plot with training statistics
        """
        unique, counts = np.unique(self.get_Sample_Names(returnNominalOnly=False).values[self.get_Sample_Types(returnNominalOnly=False).values!="Data"],return_counts=True)
        # Sort the list and keep track of indece
        indece = sorted(range(len(counts)), key=lambda k: counts[k], reverse=True)
        counts_sorted = np.array(counts)[indece]
        unique_sorted = np.array(unique)[indece]
        Canvas = TCanvas("TrainingStats","TrainingStats",1150,600)
        Histogram = TH1D("Histogram","",int(len(counts_sorted)),0,int(len(counts_sorted)))
        Histogram.SetFillColor(4)
        Histogram.SetBarWidth(0.5)
        Histogram.SetBarOffset(0.25) # Since the bar 0.25+0.5+0.25 -> bar is centered with width 0.5
        for bin_ID,count in enumerate(counts_sorted):
            Histogram.SetBinContent(bin_ID+1,count)
        Histogram.Draw("hbar")
        for bin_ID,name in enumerate(unique_sorted):
            Histogram.GetXaxis().SetBinLabel(bin_ID+1,"#splitline{"+name+"}{"+"Nr. of events:"+str(counts_sorted[bin_ID])+"}")
        DrawLabels(self.__ALabel, self.__CMLabel, self.__MyLabel, Option="MC", align="right")
        gPad.SetLogx()
        gPad.SetGridx(1)
        Histogram.GetYaxis().SetTitle("Number of MC events")
        for file_extension in self.get_GeneralSettings().get_PlotFormat():
            Canvas.SaveAs(hf.ensure_trailing_slash(self.get_CtrlPlotDirectory())+"MC_Stats."+file_extension)
        del Canvas

    def do_DataMCPlot(self, variable_object):
        """
        Function to plot a Stacked data/MC plot.
        """
        Blinding = self._DataHandler__GeneralSettings.get_Blinding()
        variable_name = variable_object.get_Name()
        variable_binning = variable_object.get_VarBinning()
        Canvas = TCanvas("DataMCPlot","DataMCPlot",800,600)
        ConverterMessage("Plotting Stacked Data/MC plot for %s."%variable_name)
        hists = hf.DefineAndFill(variable_binning,
                                 Samples=self.get_NominalSamples(),
                                 xvals=self.get(variable_name).values,
                                 xval_identifiers=self.get_Sample_Names().values,
                                 weights=self.get_Weights().values,
                                 binningoptimisation="None")
        Stack = THStack()
        if len(hists)>=6:
            Legend = TLegend(.50,.75-len(hists)/2.*0.025,.90,.90)
            Legend.SetNColumns(2)
        else:
            Legend = TLegend(.50,.75-len(hists)*0.025,.90,.90)
        Legend.SetBorderSize(0)
        Legend.SetTextFont(42)
        Legend.SetTextSize(0.035)
        Legend.SetFillStyle(0)
        data_hist = None
        for key,value in hists.items():
            if value[1]["Type"]!="Data":
                Stack.Add(value[0])
                value[0].SetFillColor(value[1]["FillColor"])
                if not value[1]["FillColor"]==0:
                    value[0].SetLineWidth(0)
                if self._DataHandler__GeneralSettings.do_Yields():
                    Legend.AddEntry(value[0],"{}  {:.1f}".format(value[1]["Name"],value[0].Integral()),"f")
                else:
                    Legend.AddEntry(value[0],key,"f")
            if value[1]["Type"]=="Data":
                data_hist = value[0]
                if self._DataHandler__GeneralSettings.do_Yields():
                    Legend.AddEntry(value[0],"{}  {:.1f}".format(value[1]["Name"],value[0].Integral()),"p")
                else:
                    Legend.AddEntry(value[0],key,"p")
        if data_hist is not None: # In case we do have a data hist we create a ratio plot
            pad1 = TPad("pad1", "pad1", 0, 0.3, 1, 1.0)
            pad1.SetBottomMargin(0)  # joins upper and lower plot
            pad1.Draw()
            # Lower ratio plot is pad2
            Canvas.cd()  # returns to main canvas before defining pad2
            pad2 = TPad("pad2", "pad2", 0, 0.05, 1, 0.3)
            pad2.SetTopMargin(0)  # joins upper and lower plot
            pad2.SetBottomMargin(0.3)
            pad2.Draw()
            d_hist = data_hist.Clone("d_hist")
            MC_hist = Stack.GetStack().Last().Clone("MC_hist")
            ratio = hf.createRatio(d_hist, MC_hist)
            ratio.SetMaximum(self.__RATIOPLOTMAX)
            ratio.SetMinimum(self.__RATIOPLOTMIN)

            # Define blue unc. band and draw it on top of Stack and create legend entry for it
            UpperPad_errband = Stack.GetStack().Last().Clone("UpperPad_errband")
            UpperPad_errband.SetFillColor(kBlue)
            UpperPad_errband.SetFillStyle(3018)
            UpperPad_errband.SetMarkerSize(0)
            Legend.AddEntry(UpperPad_errband,"Uncertainty","f")
            # Define blue unc. band for the lower pad
            LowerPad_errband = Stack.GetStack().Last().Clone("LowerPad_errband")
            for i in range(LowerPad_errband.GetNbinsX()+1):
                if LowerPad_errband.GetBinContent(i)==0:
                    LowerPad_errband.SetBinError(i,0)
                else:
                    LowerPad_errband.SetBinError(i,LowerPad_errband.GetBinError(i)/LowerPad_errband.GetBinContent(i))
                    LowerPad_errband.SetBinContent(i,1)
            LowerPad_errband.SetFillColor(kBlue)
            LowerPad_errband.SetFillStyle(3018)
            LowerPad_errband.SetMarkerSize(0)

            # Apply blinding
            if Blinding!=1:
                x_low = data_hist.GetXaxis().FindBin(Blinding)
                for i in range(x_low,LowerPad_errband.GetNbinsX()+1):
                    ratio.SetBinContent(i,-9999)
                    data_hist.SetBinContent(i,-9999)

            # draw everything in first pad
            pad1.cd()
            Stack.Draw("hist")
            Stack.GetYaxis().SetTitle("Events")
            UpperPad_errband.Draw("e2 SAME")
            data_hist.Draw("ep SAME")
            # to avoid clipping the bottom zero, redraw a small axis
            axis = Stack.GetYaxis()
            axis.ChangeLabel(1, -1, -1, -1, -1, -1, " ")

            # draw everything in second pad
            pad2.cd()
            ratio.Draw("ep")
            ratio.GetXaxis().SetTitle(variable_object.get_Label())
            LowerPad_errband.Draw("e2 SAME")

            # switch back to first pad so legend is drawn correctly
            pad1.cd()
        else:
            Stack.Draw("hist")

        # Let's make sure we have enough space for the labels
        Stack.SetMaximum(Stack.GetMaximum()*self.__SCALEFACTOR)
        Legend.Draw()
        DrawLabels(self.__ALabel, self.__CMLabel, self.__MyLabel, Option="data")

        # Now we export our plots
        varPathName = hf.filter_VarPathName(variable_name)
        for file_extension in self.get_GeneralSettings().get_PlotFormat():
            Canvas.SaveAs(hf.ensure_trailing_slash(self.get_CtrlPlotDirectory())+varPathName+"."+file_extension)

        # Clean up
        for key in hists.keys():
            gROOT.FindObject(key).Delete()
        Canvas.Close()
        del Canvas

    def do_Separation2D(self, basename="2DSeparation"):
        """
        Function to plot the separation for each indivdual sample and calculate a summary

        Keywords arguments:
        basename    --- the base name of the plot e.g. '2DSeparation'. The code will save as .pdf, .png and .eps.
        """
        #First we need to get rid of data
        df_X = self.get_DataFrame()
        df_X = df_X[df_X[self._DataHandler__SAMPLETYPE]!="Data"]
        N = np.unique(df_X[self._DataHandler__SAMPLENAME].values)
        norm = {Sample_Name: np.sum(df_X[df_X[self._DataHandler__SAMPLENAME]==Sample_Name][self._DataHandler__WEIGHTNAME].values) for Sample_Name in N}
        Separation_dict = {}
        for variable_object in self.get_VarObjects():
            variable_name = variable_object.get_Name()
            varPathName = hf.filter_VarPathName(variable_name)
            Separation_dict[variable_name]={}
            Canvas = TCanvas("c1"+variable_name,"c1",800,600)
            ConverterMessage("Plotting Separation for %s"%variable_name)
            variable_binning = variable_object.get_VarBinning()
            Hist2D = TH2D(variable_name+"_hist","",int(len(variable_binning)-1),variable_binning,len(N),0,len(N))
            for i,Sample_Name in enumerate(N):
                for value, weight in zip(df_X[df_X["Sample_Name"]==Sample_Name][variable_name].values, df_X[df_X["Sample_Name"]==Sample_Name]["Weight"].values):
                    Hist2D.Fill(value,i,weight/norm[Sample_Name])
            Yaxis = Hist2D.GetYaxis()
            for i,Sample_Name in enumerate(N):
                Yaxis.SetBinLabel(i+1,Sample_Name)
            Hist2D.Draw("colz")
            Hist2D.GetXaxis().SetTitle(variable_object.get_Label())
            Canvas.SetRightMargin(0.15)
            Hist2D.GetZaxis().SetTitle("Fraction of Events")
            DrawLabels(self.__ALabel, self.__CMLabel, self.__MyLabel, Option="MC")
            for file_extension in self.get_GeneralSettings().get_PlotFormat():
                Canvas.SaveAs(hf.ensure_trailing_slash(self.get_CtrlPlotDirectory())+basename+"_"+varPathName+"."+file_extension)
            Canvas.Clear()

            ConverterMessage("Plotting separation summary for %s"%variable_name)
            gStyle.SetPaintTextFormat(".2f")
            Separation_Hist = TH2D("Separation_Hist","",len(N),0,len(N),len(N),0,len(N))
            for i, Sample_Name_i in enumerate(N):
                for j, Sample_Name_j in enumerate(N):
                    if i<=j:
                        Separation_Hist.SetBinContent(i+1,j+1,0)
                    else:
                        hist_a = Hist2D.ProjectionX("Hist_"+Sample_Name_i,i+1,i+1)
                        hist_b = Hist2D.ProjectionX("Hist_"+Sample_Name_j,j+1,j+1)
                        s = hf.Separation(hist_a, hist_b)*100
                        Separation_Hist.SetBinContent(i+1,j+1,s)
                        Separation_dict[variable_name][Sample_Name_i+"_vs_"+Sample_Name_j]=s
                        gROOT.FindObject("Hist_"+Sample_Name_i).Delete()
                        gROOT.FindObject("Hist_"+Sample_Name_j).Delete()
            Yaxis = Separation_Hist.GetYaxis()
            Xaxis = Separation_Hist.GetXaxis()
            for i,Sample_Name in enumerate(N):
                Yaxis.SetBinLabel(i+1,Sample_Name)
                Xaxis.SetBinLabel(i+1,Sample_Name)
            Separation_Hist.Draw("colz text")
            Separation_Hist.SetMaximum(50)
            Separation_Hist.SetMinimum(0)
            #Here we are not using DrawLabels() because we need one more label
            if self.__ALabel.lower()!="none":
                ATLASLabel(0.2,0.85, self.__ALabel)
            CustomLabel(0.2,0.80, self.__CMLabel)
            if self.__MyLabel!="":
                CustomLabel(0.2,0.75, self.__MyLabel)
            CustomLabel(0.2,0.70, variable_object.get_Label().replace("[GeV]",""))
            Separation_Hist.GetZaxis().SetTitle("S [%]")
            for file_extension in self.get_GeneralSettings().get_PlotFormat():
                Canvas.SaveAs(hf.ensure_trailing_slash(self.get_CtrlPlotDirectory())+basename+"_Summary_"+varPathName+"."+file_extension)
            gROOT.FindObject("Separation_Hist").Delete()
            Canvas.Close()
            del Canvas

            df = pd.DataFrame(data=Separation_dict)
            df.to_latex(buf=hf.ensure_trailing_slash(self.get_CtrlPlotDirectory())+basename+"_Summary.tex", index=True, float_format="%.2f")
            df.to_csv(path_or_buf=hf.ensure_trailing_slash(self.get_CtrlPlotDirectory())+basename+"_Summary.csv", index=True, float_format="%.2f")

    def do_TrainTestPlot(self, variable_object):
        """
        Generate a plot with superimposed test and train variables for signal and background.
        This function is very similar to the do_TrainTestPlot from the ClassificationPlotHandler.

        Keyword arguments:
        variable_object --- An object of type variable
        """
        variable_binning = variable_object.get_VarBinning()
        variable_name = variable_object.get_Name()
        basename = "TrainTest_"+variable_name+"_"
        for i in range(self.get_nFolds()):
            ConverterMessage("Plotting K-Fold comparison for %s classifier, Fold=%d"%(variable_name,i))
            Canvas = TCanvas("c1","c1",800,600)
            Train_Hist = TH1D("Train_Hist"+str(i),"",int(len(variable_binning)-1),variable_binning)
            Test_Hist = TH1D("Test_Hist"+str(i),"",int(len(variable_binning)-1),variable_binning)

            Legend = TLegend(.63,.80-4*0.025,.85,.90)
            Legend.SetBorderSize(0)
            Legend.SetTextFont(42)
            Legend.SetTextSize(0.035)
            Legend.SetFillStyle(0)

            x_train = self.get_TrainInputs_prescaled(Fold=i, returnNonZeroLabels=False)[variable_name].values
            w_train = self.get_TrainWeights(Fold=i, returnNonZeroLabels=False).values
            x_test  = self.get_TestInputs_prescaled(Fold=i, returnNonZeroLabels=False)[variable_name].values
            w_test  = self.get_TestWeights(Fold=i, returnNonZeroLabels=False).values

            hf.FillHist(Train_Hist, x_train, w_train)
            hf.FillHist(Test_Hist, x_test, w_test)
            Legend.AddEntry(Train_Hist,"Train","p")
            Legend.AddEntry(Test_Hist,"Test","f")

            Train_Hist.Scale(1./Train_Hist.Integral())
            Test_Hist.Scale(1./Test_Hist.Integral())
            KS = Train_Hist.KolmogorovTest(Test_Hist)
            maximum = max(Train_Hist.GetMaximum(),Test_Hist.GetMaximum())

            Train_Hist.SetLineColor(2) # red
            Test_Hist.SetLineColor(2) # red

            Test_Hist.SetMarkerSize(0)
            Train_Hist.SetMarkerColor(2)
            Train_Hist.SetMarkerColor(4)

            pad1 = TPad("pad1", "pad1", 0, 0.3, 1, 1.0)
            pad1.SetBottomMargin(0)  # joins upper and lower plot
            pad1.Draw()
            # Lower ratio plot is pad2
            Canvas.cd()  # returns to main canvas before defining pad2
            pad2 = TPad("pad2", "pad2", 0, 0.05, 1, 0.3)
            pad2.SetTopMargin(0)  # joins upper and lower plot
            pad2.SetBottomMargin(0.3)
            pad2.Draw()
            ratio = hf.createRatio(Train_Hist, Test_Hist)
            ratio_errband = ratio.Clone("ratio_errband")
            maxerr = 0.55
            for binID in range(ratio_errband.GetNbinsX()+1):
                if ratio_errband.GetBinContent(binID)==0:
                    ratio_errband.SetBinContent(binID,1)
                    ratio_errband.SetBinError(binID,maxerr) # completely fill the ratio plot
                else:
                    ratio_errband.SetBinError(binID,ratio_errband.GetBinError(binID)/ratio_errband.GetBinContent(binID))
            ratio_errband.SetFillColor(kRed)
            ratio_errband.SetFillStyle(3005)
            ratio_errband.SetMarkerSize(0)
            ratio.SetMaximum(1+maxerr)
            ratio.SetMinimum(1-maxerr)
            ratio.SetMarkerSize(0)
            # draw everything in first pad
            pad1.cd()
            Test_Hist.Draw("hist e")
            Test_Hist.GetYaxis().SetTitle("Fraction of Events")
            Test_Hist.GetXaxis().SetTitle(variable_object.get_Label())
            Test_Hist.SetMaximum(maximum*1.5)
            Train_Hist.Draw("e1X0 SAME")
            if self.__ALabel.lower()!="none":
                ATLASLabel(0.175,0.85, self.__ALabel)
            CustomLabel(0.175,0.8, self.__CMLabel)
            CustomLabel(0.175,0.75, "KS Test: P=%.3f"%(KS))
            if self.__MyLabel!="":
                CustomLabel(0.2,0.70, self.__MyLabel)
            Legend.Draw("SAME")

            # to avoid clipping the bottom zero, redraw a small axis
            axis = Test_Hist.GetYaxis()
            axis.ChangeLabel(1, -1, -1, -1, -1, -1, " ")

            # draw everything in second pad
            pad2.cd()
            ratio.Draw("hist ")
            ratio_errband.Draw("e2 Same")
            line = TLine(ratio.GetXaxis().GetXmin(),1,ratio.GetXaxis().GetXmax(),1)
            line.SetLineStyle(2) # dashed
            line.Draw("Same")
            ratio.GetXaxis().SetTitle(variable_object.get_Label())
            ratio.GetYaxis().SetTitle("Train/Test")

            # switch back to first pad so legend is drawn correctly
            pad1.cd()
            for file_extension in self.get_GeneralSettings().get_PlotFormat():
                Canvas.SaveAs(hf.ensure_trailing_slash(self.get_CtrlPlotDirectory())+basename+str(i)+"."+file_extension)
            del Train_Hist, Test_Hist
            Canvas.Close()
            del Canvas

import HelperModules.HelperFunctions as hf
from tensorflow.keras.models import load_model
from HelperModules.AtlasLabel import DrawLabels
from HelperModules.DataHandler import DataHandler
from HelperModules.Metrics import Metrics

from ROOT import TH1D, TCanvas, THStack, TLegend, TGraph, TMultiGraph, gPad, gROOT
import json
import numpy as np
import pandas as pd
from pickle import load

class WGANPlotHandler(DataHandler):
    """
    Class providing plotting functionalities for WGAN models.
    """
    def __init__(self,Settings):
        super().__init__(Settings)
        self.__ALabel  = self._DataHandler__GeneralSettings.get_ATLASLabel()  # ATLAS label
        self.__CMLabel = self._DataHandler__GeneralSettings.get_CMLabel()     # Centre of mass label
        self.__MyLabel = self._DataHandler__GeneralSettings.get_CustomLabel() # Custom label
        gROOT.SetBatch()

        ###################################################################
        ######################## Model Information ########################
        ###################################################################
        if(self.get_nFolds()!=1):
            Folds = [str(i) for i in range(self.get_nFolds())]
        else:
            Folds = ["0"]

        self.__ModelNames = [hf.ensure_trailing_slash(hf.ensure_trailing_slash(self.get_ModelDirectory()))+Settings.get_Model().get_Name()+"_"+Settings.get_Model().get_Type()+Fold+".h5" for Fold in Folds]
        self.__GeneratorNames = [hf.ensure_trailing_slash(hf.ensure_trailing_slash(self.get_ModelDirectory()))+Settings.get_Model().get_Name()+"_"+Settings.get_Model().get_Type()+"_Generator"+Fold+".h5" for Fold in Folds]
        self.__HistoryNames = [hf.ensure_trailing_slash(hf.ensure_trailing_slash(self.get_ModelDirectory()))+Settings.get_Model().get_Name()+"_"+Settings.get_Model().get_Type()+"_history"+Fold+".json" for Fold in Folds]
        self.__KerasModels = [load_model(modelname, compile=False) for modelname in self.__ModelNames]
        self.__GeneratorModels = [load_model(modelname, compile=False) for modelname in self.__GeneratorNames]
        self.__Scaler = load(open(hf.ensure_trailing_slash(self.get_ModelDirectory())+"scaler.pkl",'rb'))

    ###########################################################################
    ############################# misc functions ##############################
    ###########################################################################
    def get_HistoryNames(self):
        return self.__HistoryNames

    ###########################################################################
    ############################ Wrapper functions ############################
    ###########################################################################
    def do_Plots(self):
        """
        Wrapper function to produce all WGAN plots.
        """
        self.do_PlotFakeReal()
        self.do_HistoryPlot()

    ###########################################################################
    ########################### Plotting functions ############################
    ###########################################################################
    def do_PlotFakeReal(self,nFakes=10000):
        """
        Plotting function to produce comparison plots of fake/real data.

        Keyword arguments:
        nFakes --- number of fake events to be generated
        """
        X_Real = self.get_sc(self.get_Variables()).sample(n=nFakes)
        noise  = np.random.uniform(0,1,size=[nFakes, self.get_ModelSettings().get_LatentDim()])
        X_Fake = pd.DataFrame(data=self.__GeneratorModels[0].predict(noise), columns=self.get_Variables())
        for var in self.get_Variables():
            H_Real = TH1D("H_Real","H_Real",100,-1,1)
            H_Fake = TH1D("H_Fake","H_Fake",100,-1,1)
            H_Real.SetLineColor(2) # red
            H_Fake.SetLineColor(4) # red
            for x_r, x_f in zip(X_Real[var].values,X_Fake[var].values):
                H_Real.Fill(x_r)
                H_Fake.Fill(x_f)
            Canvas = TCanvas("PerformancePlot","PerformancePlot",800,600)
            Stack = THStack()
            Stack.Add(H_Real)
            Stack.Add(H_Fake)
            Stack.Draw("hist nostack")
            # gPad.SetLogy()
            Canvas.SaveAs(hf.ensure_trailing_slash(self.get_MVAPlotDirectory())+"RealFake_"+var+".pdf")
            Canvas.SaveAs(hf.ensure_trailing_slash(self.get_MVAPlotDirectory())+"RealFake_"+var+".png")
            Canvas.SaveAs(hf.ensure_trailing_slash(self.get_MVAPlotDirectory())+"RealFake_"+var+".eps")
            gROOT.FindObject("H_Real").Delete()
            gROOT.FindObject("H_Fake").Delete()
            Canvas.Close()
            del Canvas
        
    def do_HistoryPlot(self):
        """
        Function to produce a history plot containing generator and critic loss.
        """
        for i,historyname in enumerate(self.get_HistoryNames()):
            basename = self.get_ModelSettings().get_Name()+"_"+str(i)
            with open(historyname) as f:
                History = json.load(f)
                Canvas = TCanvas("PerformancePlot","PerformancePlot",800,600)
                keys = History.keys()
                mg = TMultiGraph()
                
                y_critic = np.array(list(History["Critic Loss"].values()))
                y_generator = np.array(list(History["Generator Loss"].values()))
                x = np.arange(1,len(y_critic)+1,1)
                gr_critic = TGraph(x.size,x.astype(np.double),y_critic.astype(np.double))
                gr_generator = TGraph(x.size,x.astype(np.double),y_generator.astype(np.double))
                gr_critic.SetLineColor(2)
                gr_critic.SetLineWidth(1)
                gr_generator.SetLineColor(4)
                gr_generator.SetLineWidth(1)
                mg.Add(gr_generator)
                mg.Add(gr_critic)

                Legend = TLegend(.73,.75,.85,.85)
                Legend.SetBorderSize(0)
                Legend.SetTextFont(42)
                Legend.SetTextSize(0.035)
                Legend.AddEntry(gr_critic,"Critic Loss","L")
                Legend.AddEntry(gr_generator,"Generator Loss","L")
                mg.Draw("A")
                Legend.Draw("SAME")
                # gPad.SetLogx()
                mg.SetMaximum(mg.GetHistogram().GetMaximum()*1.3)
                mg.GetYaxis().SetTitle("Loss")
                mg.GetXaxis().SetTitle("Epoch")
                DrawLabels(self.__ALabel, self.__CMLabel, self.__MyLabel, Option="MC")
                Canvas.SaveAs(hf.ensure_trailing_slash(self.get_MVAPlotDirectory())+basename+"_History.pdf")
                Canvas.SaveAs(hf.ensure_trailing_slash(self.get_MVAPlotDirectory())+basename+"_History.png")
                Canvas.SaveAs(hf.ensure_trailing_slash(self.get_MVAPlotDirectory())+basename+"_History.eps")
                Canvas.Close()
                del Canvas

import HelperModules.HelperFunctions as hf
from HelperModules.AtlasLabel import ATLASLabel, CustomLabel, DrawLabels
from HelperModules.MessageHandler import PostProcessorMessage
from PlotterModules.BasicPlotHandler import BasicPlotHandler
from ROOT import gROOT, TCanvas, THStack, TH1F, TH2F, TLegend, TLine
import numpy as np
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt

class RegressionPlotHandler(BasicPlotHandler):
    def __init__(self,Settings):
        super().__init__(Settings)

    def get_RegressionTargetLabel(self):
        print(self.Model.get_RegressionTarget())
    ###########################################################################
    ############################ Wrapper functions ############################
    ###########################################################################
    def do_Plots(self):
        """
        Wrapper function to produce all available plots in one go.
        """
        self.do_PerformancePlot()
        # self.do_ArchitecturePlot()
        self.do_StackPlot(ClassLabel="Regression")
        self.do_RegressionVersusTruthPlot()
        self.do_RegressionVersusTruthPlot2D()
        self.do_ErrorPlot1d()
        self._do_ReweightingPlot()

    def do_RegressionVersusTruthPlot(self):
        PostProcessorMessage("Plotting regressed variable versus truth variable.")
        Canvas = TCanvas("c1","c1",800,600)
        basename = self.Model.get_Name()+"_"+self.Model.get_Type()
        Binning = self.ModelVariables["Regression"].get_VarBinning()
        TruthDistribution = TH1F("TruthDistribution", "TruthDistribution",int(len(Binning)-1),Binning)
        PredDistribution  = TH1F("PredDistribution", "PredDistribution",int(len(Binning)-1),Binning)
        hf.FillHist(TruthDistribution, values=self.get_Labels(), weights=self.get_Weights())
        hf.FillHist(PredDistribution, values=self.get_Predictions(self._DataHandler__PREDICTIONNAME), weights=self.get_Weights())
        TruthDistribution.SetLineColor(2) # red
        PredDistribution.SetLineColor(4) # blue
        HistStack = THStack()
        HistStack.Add(TruthDistribution)
        HistStack.Add(PredDistribution)
        HistStack.Draw("hist nostack")
        HistStack.GetYaxis().SetTitle("Events")
        HistStack.GetXaxis().SetTitle(self.Model.get_RegressionTargetLabel())
        HistStack.SetMaximum(HistStack.GetMaximum()*1.2)
        Legend = TLegend(.75,.75,.90,.90)
        Legend.SetBorderSize(0)
        Legend.SetTextFont(42)
        Legend.SetTextSize(0.035)
        Legend.SetFillStyle(0)
        Legend.AddEntry(TruthDistribution,"Truth","L")
        Legend.AddEntry(PredDistribution,"Prediction","L")
        DrawLabels(self.ALabel, self.CMLabel, self.MyLabel, Option="MC")
        Legend.Draw()
        for file_extension in self.get_GeneralSettings().get_PlotFormat():
            Canvas.SaveAs(hf.ensure_trailing_slash(self.get_MVAPlotDirectory())+basename+"_Truth_versus_Prediction."+file_extension)
        gROOT.FindObject("TruthDistribution").Delete()
        gROOT.FindObject("PredDistribution").Delete()
        Canvas.Close()
        del Canvas

    def do_RegressionVersusTruthPlot2D(self):
        PostProcessorMessage("Plotting Regression/Truth Matrix.")
        Canvas = TCanvas("c1","c1",800,600)
        basename = self.Model.get_Name()+"_"+self.Model.get_Type()
        Binning = self.ModelVariables["Regression"].get_VarBinning()
        Distribution2D = TH2F("Distribution2D", "Distribution2D",int(len(Binning)-1),Binning,int(len(Binning)-1),Binning)
        hf.FillHist2D(Distribution2D, values_1=self.get_Predictions(self._DataHandler__PREDICTIONNAME), values_2=self.get_Labels(), weights=self.get_Weights()/np.sum(self.get_Weights())*100)
        Distribution2D.Draw("colz")
        Distribution2D.SetMinimum(0)
        Diagonal_Line = TLine(0,0,Binning[-1],Binning[-1])
        Diagonal_Line.SetLineColor(1)
        Diagonal_Line.SetLineWidth(2)
        Canvas.SetRightMargin(0.15)
        Canvas.SetTopMargin(0.1)
        Distribution2D.GetZaxis().SetTitle("Percentage of Events [%]")
        DrawLabels(self.ALabel, self.CMLabel, self.MyLabel, Option="MC",align="top_outside")
        Distribution2D.GetXaxis().SetTitle(self.Model.get_RegressionTargetLabel()+" (Pred.)")
        Distribution2D.GetYaxis().SetTitle(self.Model.get_RegressionTargetLabel()+" (Truth)")
        Diagonal_Line.Draw("Same")
        for file_extension in self.get_GeneralSettings().get_PlotFormat():
            Canvas.SaveAs(hf.ensure_trailing_slash(self.get_MVAPlotDirectory())+basename+"_Truth_versus_Prediction2D."+file_extension)
        gROOT.FindObject("Distribution2D").Delete()
        Canvas.Close()
        del Canvas

    def do_ErrorPlot1d(self):
        PostProcessorMessage("Plotting error distribution.")
        Canvas = TCanvas("c1","c1",800,600)
        basename = self.Model.get_Name()+"_"+self.Model.get_Type()
        ErrorDistribution = TH1F("ErrorDistribution", "ErrorDistribution",100,0.5,1.5)
        hf.FillHist(ErrorDistribution, values=(self.get_Predictions(self._DataHandler__PREDICTIONNAME)/self.get_Labels()), weights=self.get_Weights())
        ErrorDistribution.Draw("hist")
        ErrorDistribution.GetYaxis().SetTitle("Events")
        ErrorDistribution.GetXaxis().SetTitle(self.Model.get_RegressionTargetLabel()+"(Prediction/Truth)")
        DrawLabels(self.ALabel, self.CMLabel, self.MyLabel, Option="MC")
        for file_extension in self.get_GeneralSettings().get_PlotFormat():
            Canvas.SaveAs(hf.ensure_trailing_slash(self.get_MVAPlotDirectory())+basename+"_ErrorDistribution."+file_extension)
        gROOT.FindObject("ErrorDistribution").Delete()
        Canvas.Close()
        del Canvas

    def _do_ReweightingPlot(self):
        PostProcessorMessage("Plotting Reweighted distribution.")
        Canvas = TCanvas("c1","c1",800,600)
        ReweightedDistribution = TH1F("ReweightedDistribution", "ReweightedDistribution",50,0,600)
        hf.FillHist(ReweightedDistribution, values=(self.get_Labels()), weights=self.get_Weights_sc())
        ReweightedDistribution.Draw("hist")
        ReweightedDistribution.GetYaxis().SetTitle("Events")
        ReweightedDistribution.GetXaxis().SetTitle(self.Model.get_RegressionTargetLabel()+"(Prediction/Truth)")
        DrawLabels(self.ALabel, self.CMLabel, self.MyLabel, Option="MC")
        for file_extension in self.get_GeneralSettings().get_PlotFormat():
            Canvas.SaveAs(hf.ensure_trailing_slash(self.get_MVAPlotDirectory())+self.Model.get_Name()+"_"+self.Model.get_Type()+"_Reweighting."+file_extension)
        gROOT.FindObject("ReweightedDistribution").Delete()
        Canvas.Close()
        del Canvas
        

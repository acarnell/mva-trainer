"""
Module handling basic plotting functionalities
"""
import HelperModules.AtlasStyle
import HelperModules.CustomFunctions
import HelperModules.HelperFunctions as hf
from ROOT import TCanvas, THStack, TLegend, TGraph, TMultiGraph, gPad, gROOT, TPad, kBlue
import json,itertools
import numpy as np
import pandas as pd

from pickle import load
from HelperModules.AtlasLabel import DrawLabels
from HelperModules.DataHandler import DataHandler
from HelperModules.Metrics import Metrics
from HelperModules.MessageHandler import PostProcessorMessage, WarningMessage
from HelperModules.Variable import Variable
from tensorflow.keras.models import load_model
from tensorflow.keras.utils import plot_model

import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt

class BasicPlotHandler(DataHandler):
    """
    Class handling basic plotting functionalities
    """
    def __init__(self,Settings):
        super().__init__(Settings)
        self.ALabel  = self._DataHandler__GeneralSettings.get_ATLASLabel()
        self.CMLabel = self._DataHandler__GeneralSettings.get_CMLabel()
        self.MyLabel = self._DataHandler__GeneralSettings.get_CustomLabel()
        self.Model   = self.get_ModelSettings()
        if self.Model.get_ClassColors() is not None:
            self.ClassColorDict   = dict(zip(np.unique(self.get_DataFrame()[self._DataHandler__LABELNAME]),self.Model.get_ClassColors()))
        else:
            self.ClassColorDict = None
        self.isBinary             = (len(self.get_ClassLabels()) == 1 or len(self.get_ClassLabels()) == 0) and self.Model.isClassification()
        self.ModelVariables = {class_label: Variable(name=class_label,
                                                     label=class_label,
                                                     binning=model_binning) for class_label, model_binning in zip(self.get_ClassLabels(),self.Model.get_ModelBinning())}
        self.SCALEFACTOR          = 1.8
        self.SCALEFACTOR_DATAHIST = 1.7
        self.RATIOPLOTMAX         = self.get_GeneralSettings().get_RatioMax() # Maximum value in ratio (default 1.55)
        self.RATIOPLOTMIN         = self.get_GeneralSettings().get_RatioMin() # Minimum value in ratio (default 0.45)
        gROOT.SetBatch()
        ###################################################################
        ######################## Model Information ########################
        ###################################################################
        if self.get_nFolds()!=1:
            Folds = ["_Fold"+str(i) for i in range(self.get_nFolds())]
        else:
            Folds = [""]
        if self.Model.isClassificationDNN() or self.Model.isReconstruction() or self.Model.isRegressionDNN():
            self.ModelNames = [hf.ensure_trailing_slash(hf.ensure_trailing_slash(self.get_ModelDirectory()))+Settings.get_Model().get_Name()+"_"+Settings.get_Model().get_Type()+Fold+".h5" for Fold in Folds]
            self.__HistoryNames = [hf.ensure_trailing_slash(hf.ensure_trailing_slash(self.get_ModelDirectory()))+Settings.get_Model().get_Name()+"_"+Settings.get_Model().get_Type()+"_history"+Fold+".json" for Fold in Folds]
            self.ModelObjects = [load_model(modelname, compile=False) for modelname in self.ModelNames]
        if self.Model.isClassificationBDT() or self.Model.isRegressionBDT() or self.Model.isClassificationXgBDT():
            self.ModelNames = [hf.ensure_trailing_slash(hf.ensure_trailing_slash(self.get_ModelDirectory()))+Settings.get_Model().get_Name()+"_"+Settings.get_Model().get_Type()+Fold+".pkl" for Fold in Folds]
            self.ModelObjects = []
            for modelname in self.ModelNames:
                with open(modelname, 'rb') as f:
                    self.ModelObjects.append(load(f))
        self.__Scaler = load(open(hf.ensure_trailing_slash(self.get_ModelDirectory())+"scaler.pkl",'rb'))
        ###################################################################
        ########################### Predictions ###########################
        ###################################################################
        F_tmp = [self.get_DataFrame_sc()[self.get_DataFrame_sc()[self._DataHandler__FOLDIDENTIFIER]%self.get_nFolds()==i]["Index_tmp"].values for i in range(self.get_nFolds())]
        X_tmp = [(self.get_DataFrame_sc()[self.get_DataFrame_sc()[self._DataHandler__FOLDIDENTIFIER]%self.get_nFolds()==i][self.get_VarNames()]).values for i in range(self.get_nFolds())]
        if self.Model.isClassificationDNN() or self.Model.isReconstruction() or self.Model.isRegressionDNN():
            P_tmp = [model.predict(x) for x,model in zip(X_tmp,self.ModelObjects)]
        if self.Model.isClassificationBDT():
            if self.isBinary:
                P_tmp = [model.predict_proba(x)[:,1] for x,model in zip(X_tmp,self.ModelObjects)] # scikit-learn provides both [1-P,P]. We only want P.
            else:
                P_tmp = [model.predict_proba(x) for x,model in zip(X_tmp,self.ModelObjects)]
        if self.Model.isClassificationXgBDT():
            if self.isBinary:
                P_tmp = [(model.predict_proba(x))[:,1] for x,model in zip(X_tmp,self.ModelObjects)] # scikit-learn provides both [1-P,P]. We only want P.
                #P_tmp = [np.array(P_wip[0].tolist()),np.array(P_wip[1].tolist())] 
            else:
                P_tmp = [model.predict_proba(x) for x,model in zip(X_tmp,self.ModelObjects)]
        if self.Model.isRegressionBDT():
            P_tmp=[model.predict(x) for x,model in zip(X_tmp,self.ModelObjects)]
        if self.get_nFolds()!=1:
            P_tmp = np.array(list(itertools.chain.from_iterable(P_tmp)))
            F_tmp = np.array(list(itertools.chain.from_iterable(F_tmp)))
        if self.isBinary or self.Model.isRegression():
            P_df = pd.DataFrame(data=F_tmp, columns=["Index_tmp"])
            P_df[self._DataHandler__PREDICTIONNAME] = P_tmp
        else:
            P_df = pd.DataFrame(data=F_tmp, columns=["Index_tmp"])
            for i, ClassLabel in enumerate(self.get_ClassLabels()):
                P_df[ClassLabel] = P_tmp[:,i]
        self._DataHandler__DataFrame    = pd.merge(self.get_DataFrame(), P_df, how="inner", on=["Index_tmp"])
        self._DataHandler__DataFrame_sc = pd.merge(self.get_DataFrame_sc(), P_df, how="inner", on=["Index_tmp"])
        self._DataHandler__DataFrame    = self._DataHandler__DataFrame.drop(["Index_tmp"],axis=1)
        self._DataHandler__DataFrame_sc = self._DataHandler__DataFrame_sc.drop(["Index_tmp"],axis=1)
    ###########################################################################
    ############################# misc functions ##############################
    ###########################################################################
    def get_Predictions(self, PredictionColumn):
        """
        Getter method to return the 'PredictionColumn' from a pandas DataFrame object

        Keyword arguments:
        PredictionColumn --- String defining the column of the pandas DataFrame object to be returned.
        """
        return self.get_DataFrame()[PredictionColumn]

    def get_ModelObjects(self):
        """
        Getter method to return all model objects.
        """
        return self.ModelObjects

    def get_TrainPredictions(self,ColumnID=0, Fold=0, returnNonZeroLabels=False):
        """
        Getter method to return the predictions using the training set

        Keyword arguments:
        ColumnID --- Integer defining the column of the array. I.e. 0 for first column in a binary case
        Fold --- Integer defining the fold the training data belonged to
        returnNonZeroLabels --- Boolean, can be True or False. If set to True also events that where not trained on are returned (i.e. labels <0)
        """
        if self.Model.isClassificationBDT() or self.Model.isClassificationXgBDT():
            if self.isBinary:
                ColumnID = 1
            return self.ModelObjects[Fold].predict_proba(self.get_TrainInputs(Fold=Fold, returnNonZeroLabels=returnNonZeroLabels).values)[:,ColumnID]      
        return self.ModelObjects[Fold].predict(self.get_TrainInputs(Fold=Fold, returnNonZeroLabels=returnNonZeroLabels).values)[:,ColumnID]
    
    def get_TestPredictions(self,ColumnID=0, Fold=0, returnNonZeroLabels=False):
        """
        Getter method to return the predictions using the testing set

        Keyword arguments:
        ColumnID --- Integer defining the column of the array. I.e. 0 for first column in a binary case
        Fold --- Integer defining the fold the testing data belonged to
        returnNonZeroLabels --- Boolean, can be True or False. If set to True also events that where not trained on are returned (i.e. labels <0)
        """
        if self.Model.isClassificationBDT() or self.Model.isClassificationXgBDT():
            if self.isBinary:
                ColumnID = 1
            return self.ModelObjects[Fold].predict_proba(self.get_TestInputs(Fold=Fold, returnNonZeroLabels=returnNonZeroLabels).values)[:,ColumnID]
        return self.ModelObjects[Fold].predict(self.get_TestInputs(Fold=Fold, returnNonZeroLabels=returnNonZeroLabels).values)[:,ColumnID]

    def get_HistoryNames(self):
        """
        Getter method to return the names of the histories
        """
        return self.__HistoryNames

    ###########################################################################
    ########################### Plotting functions ############################
    ###########################################################################
    def do_StackPlot(self, PredictionColumn=None, ClassLabel=None, ClassID=1):
        """
        Function to plot a Stacked data/MC plot.

        Keyword arguments:
        PredictionColumn --- Name of the column holding the prediction
        ClassLabel       --- Name of the class itself
        """
        if PredictionColumn is None:
            PredictionColumn=self._DataHandler__PREDICTIONNAME
        basename = self.Model.get_Name()+"_"+self.Model.get_Type()
        Blinding = self._DataHandler__GeneralSettings.get_Blinding()

        Canvas = TCanvas("StackPlot","StackPlot",800,600)
        PostProcessorMessage("Plotting Stacked Data/MC plot for "+ClassLabel+".")
        Binning = self.ModelVariables[ClassLabel].get_VarBinning()
        if self.Model.isClassification() or self.Model.isRegression():
            hists = hf.DefineAndFill(Binning,
                                     Samples=self._DataHandler__Samples,
                                     xvals=self.get_Predictions(PredictionColumn).values,
                                     xval_identifiers=self._DataHandler__DataFrame[self._DataHandler__SAMPLENAME].values,
                                     weights=self._DataHandler__DataFrame[self._DataHandler__WEIGHTNAME].values,
                                     ClassID=ClassID,
                                     binningoptimisation=self.Model.get_BinningOptimisation())
        elif self.Model.is3LZReconstruction() or self.Model.is4LZReconstruction():
            hists = hf.DefineAndFill(Binning,
                                     Samples=self._DataHandler__ClassLabels,
                                     xvals=self.get_Predictions(PredictionColumn).values,
                                     xval_identifiers=self.get_Labels().values,
                                     weights=self._DataHandler__DataFrame[self._DataHandler__WEIGHTNAME].values,
                                     ClassID=ClassID,
                                     SamplesAreClasses=True,
                                     binningoptimisation=self.Model.get_BinningOptimisation())
        Stack = THStack()
        if len(hists)>=6:
            Legend = TLegend(.50,.75-len(hists)/2.*0.025,.90,.90)
            Legend.SetNColumns(2)
        else:
            Legend = TLegend(.50,.75-len(hists)*0.025,.90,.90)
        Legend.SetBorderSize(0)
        Legend.SetTextFont(42)
        Legend.SetTextSize(0.035)
        Legend.SetFillStyle(0)
        data_hist = None

        for value in hists.values():
            if value[1]["Type"]!="Data":
                value[0].SetFillColor(value[1]["FillColor"])
                if not value[1]["FillColor"]==0:
                    value[0].SetLineWidth(0)
                Stack.Add(value[0])
                if value[1]["Type"]=="Signal" and value[1]["ScaleToBkg"]:
                    scaled_hist = value[0].Clone(value[1]["Name"]+"_scaled")
                    scaled_hist.Scale(sum([v[0].Integral() for v in hists.values() if v[1]["Type"]=="Background"])/scaled_hist.Integral())
                    scaled_hist.SetFillColor(0)
                    scaled_hist.SetLineColor(value[1]["FillColor"])
                    scaled_hist.SetLineStyle(2)
                    scaled_hist.SetLineWidth(1)
                    Stack.Add(scaled_hist)
                    if self._DataHandler__GeneralSettings.do_Yields():
                        Legend.AddEntry(scaled_hist,"{}  {:.1f}".format(value[1]["Name"]+" scaled",scaled_hist.Integral()),"f")
                    else:
                        Legend.AddEntry(scaled_hist,value[1]["Name"]+" scaled","f")
                if self.Model.isClassification():
                    if self._DataHandler__GeneralSettings.do_Yields():
                        Legend.AddEntry(value[0],"{}  {:.1f}".format(value[1]["Name"],value[0].Integral()),"f")
                    else:
                        Legend.AddEntry(value[0],value[1]["Name"],"f")
                elif self.Model.is3LZReconstruction() or self.Model.is4LZReconstruction():
                    Legend.AddEntry(value[0], value[1]["Name"], "f")
            if value[1]["Type"]=="Data":
                data_hist = value[0]
                if self.Model.isClassification():
                    if self._DataHandler__GeneralSettings.do_Yields():
                        Legend.AddEntry(value[0],"{}  {:.1f}".format(value[1]["Name"],value[0].Integral()),"p")
                    else:
                        Legend.AddEntry(value[0],value[1]["Name"],"p")
                elif self.__Model.isReconstruction():
                    Legend.AddEntry(value[0], value[1][1], "p")
        if data_hist is not None: # In case we do have a data hist we create a ratio plot
            pad1 = TPad("pad1", "pad1", 0, 0.3, 1, 1.0)
            pad1.SetBottomMargin(0)  # joins upper and lower plot
            pad1.Draw()
            # Lower ratio plot is pad2
            Canvas.cd()  # returns to main canvas before defining pad2
            pad2 = TPad("pad2", "pad2", 0, 0.05, 1, 0.3)
            pad2.SetTopMargin(0)  # joins upper and lower plot
            pad2.SetBottomMargin(0.3)
            pad2.Draw()
            d_hist = data_hist.Clone("d_hist")
            MC_hist = Stack.GetStack().Last().Clone("MC_hist")
            ratio = hf.createRatio(d_hist, MC_hist)
            ratio.SetMaximum(self.RATIOPLOTMAX)
            ratio.SetMinimum(self.RATIOPLOTMIN)

            # Define blue unc. band and draw it on top of Stack and create legend entry for it
            UpperPad_errband = Stack.GetStack().Last().Clone("UpperPad_errband")
            UpperPad_errband.SetFillColor(kBlue)
            UpperPad_errband.SetFillStyle(3018)
            UpperPad_errband.SetMarkerSize(0)
            Legend.AddEntry(UpperPad_errband,"Uncertainty","f")
            # Define blue unc. band for the lower pad
            LowerPad_errband = Stack.GetStack().Last().Clone("LowerPad_errband")
            for i in range(LowerPad_errband.GetNbinsX()+1):
                if LowerPad_errband.GetBinContent(i)==0:
                    LowerPad_errband.SetBinError(i,0)
                else:
                    LowerPad_errband.SetBinError(i,LowerPad_errband.GetBinError(i)/LowerPad_errband.GetBinContent(i))
                    LowerPad_errband.SetBinContent(i,1)
            LowerPad_errband.SetFillColor(kBlue)
            LowerPad_errband.SetFillStyle(3018)
            LowerPad_errband.SetMarkerSize(0)

            # Apply blinding
            if Blinding!=1:
                x_low = data_hist.GetXaxis().FindBin(Blinding)
                for i in range(x_low,LowerPad_errband.GetNbinsX()+1):
                    ratio.SetBinContent(i,-9999)
                    data_hist.SetBinContent(i,-9999)

            # draw everything in first pad
            pad1.cd()
            Stack.Draw("hist")
            Stack.SetMaximum(Stack.GetMaximum()*1.3)
            UpperPad_errband.Draw("e2 SAME")
            data_hist.Draw("ep SAME")
            # to avoid clipping the bottom zero, redraw a small axis
            axis = Stack.GetYaxis()
            axis.ChangeLabel(1, -1, -1, -1, -1, -1, " ")

            # draw everything in second pad
            pad2.cd()
            ratio.Draw("ep")
            ratio.GetXaxis().SetTitle(ClassLabel+" Classifier")
            LowerPad_errband.Draw("e2 SAME")

            # switch back to first pad so legend is drawn correctly
            pad1.cd()
        else:
            Stack.Draw("hist")
            Stack.GetXaxis().SetTitle(ClassLabel+" Classifier")
        Stack.GetYaxis().SetTitle("Events")
        # Let's make sure we have enough space for the labels
        Stack.SetMaximum(Stack.GetMaximum()*self.SCALEFACTOR)
        Legend.Draw()
        DrawLabels(self.ALabel, self.CMLabel, self.MyLabel, Option="data")
        # logscale or linear scale
        if self.get_GeneralSettings().get_StackPlotScale()=="Log":
            gPad.SetLogy()
        # Now we export our plots
        for file_extension in self.get_GeneralSettings().get_PlotFormat():
            Canvas.SaveAs(hf.ensure_trailing_slash(self.get_MVAPlotDirectory())+basename+"_"+ClassLabel+"."+file_extension)
        # Clean up
        for key in hists.keys():
            gROOT.FindObject(key).Delete()
        Canvas.Close()
        del Canvas

    def do_PerformancePlot(self):
        """
        Plot a typical performance plot using a keras history object.
        The method will check whether the requested metric is indeed available.
        """
        metrics = self.Model.get_Metrics()
        if metrics is None:
            metrics = []
        metrics.append("loss")
        Metric_Labels = Metrics()
        for i,historyname in enumerate(self.get_HistoryNames()):
            with open(historyname) as f:
                History = json.load(f)
                for metric in metrics:
                    Canvas = TCanvas("PerformancePlot","PerformancePlot",800,600)
                    mg = TMultiGraph()
                    if metric in History.keys():
                        PostProcessorMessage("Plotting training history for metric "+metric+" and fold "+str(i)+".")
                        y = np.array(list(History[metric].values()))
                        x = np.arange(1,len(y)+1,1)
                        gr = TGraph(x.size,x.astype(np.double),y.astype(np.double))
                        gr.SetLineColor(2)
                        gr.SetLineWidth(2)
                        mg.Add(gr)
                        Legend = TLegend(.73,.75,.85,.85)
                        Legend.SetBorderSize(0)
                        Legend.SetTextFont(42)
                        Legend.SetTextSize(0.035)
                        Legend.AddEntry(gr,"Train","L")
                        if "val_"+metric in History.keys():
                            y = np.array(list(History["val_"+metric].values()))
                            x = np.arange(1,len(y)+1,1)
                            gr_val = TGraph(x.size,x.astype(np.double),y.astype(np.double))
                            gr_val.SetLineColor(4)
                            gr_val.SetLineWidth(2)
                            mg.Add(gr_val)
                            Legend.AddEntry(gr_val,"Validation","L")
                        mg.Draw("A")
                        Legend.Draw("SAME")
                        gPad.SetLogx()
                        mg.GetYaxis().SetTitle(Metric_Labels.get_Label(metric))
                        mg.GetXaxis().SetTitle("Epoch")
                        DrawLabels(self.ALabel, self.CMLabel, self.MyLabel, Option="MC")
                        for file_extension in self.get_GeneralSettings().get_PlotFormat():
                            Canvas.SaveAs(hf.ensure_trailing_slash(self.get_MVAPlotDirectory())+self.Model.get_Name()+"_"+str(i)+"_"+metric+"."+file_extension)
                    else:
                        WarningMessage("Metric "+metric+" does not exist. No history plot can be created!")
                    Canvas.Close()
                    del Canvas

    def do_ArchitecturePlot(self, left=0.02, right=0.98, bottom=0.02, top=0.98):
        """
        Function to create simple architecture plots of the model.

        Keyword arguments:
        left        --- Left border.
        right       --- Right border.
        bottom      --- Bottom border.
        top         --- Top border.
        """
        PostProcessorMessage("Plotting model architecture.")
        for file_extension in self.get_GeneralSettings().get_PlotFormat():
            plot_model(self.get_ModelObjects()[0], to_file=hf.ensure_trailing_slash(self.get_MVAPlotDirectory())+"Model."+file_extension)
        basename = self.Model.get_Name()+"_"
        if self.Model.get_PredesignedModel() is None:
            layer_sizes = [len(self.get_VarNames())]
            layer_sizes.extend(self.Model.get_Nodes())
            layer_sizes.append(self.Model.get_OutputSize())
            weight_array = []
            for i in range(len(layer_sizes)-1):
                weight_array.append(np.ones((layer_sizes[i],layer_sizes[i+1])))
            fig = plt.figure()
            ax = fig.gca()
            ax.axis('off')
            v_spacing = (top - bottom)/float(max(layer_sizes))
            h_spacing = (right - left)/float(len(layer_sizes) - 1)
            # Nodes
            for n, layer_size in enumerate(layer_sizes):
                layer_top = v_spacing*(layer_size - 1)/2. + (top + bottom)/2.
                for m in range(layer_size):
                    circle = plt.Circle((n*h_spacing + left, layer_top - m*v_spacing), v_spacing/2.,color='w', ec='k', zorder=4)
                    ax.add_artist(circle)
            # Edges
            for n, (layer_size_a, layer_size_b) in enumerate(zip(layer_sizes[:-1], layer_sizes[1:])):
                layer_top_a = v_spacing*(layer_size_a - 1)/2. + (top + bottom)/2.
                layer_top_b = v_spacing*(layer_size_b - 1)/2. + (top + bottom)/2.
                for m in range(layer_size_a):
                    for o in range(layer_size_b):
                        line = plt.Line2D([n*h_spacing + left, (n + 1)*h_spacing + left],[layer_top_a - m*v_spacing, layer_top_b - o*v_spacing],c="black", linewidth=0.05, zorder=1)
                        ax.add_artist(line)
            for file_extension in self.get_GeneralSettings().get_PlotFormat():
                fig.savefig(hf.ensure_trailing_slash(self.get_MVAPlotDirectory())+basename+"Model_Graph."+file_extension)
            plt.close()

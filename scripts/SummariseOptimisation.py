#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import pandas as pd
import numpy as np
import argparse, glob, json
from itertools import groupby
from HelperModules.Settings import Settings

if __name__ == "__main__":
    parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument("-j", "--JOBpath", help="Input path of the JOB directory", required=True)
    parser.add_argument("-c", "--CFGpath", help="Input path of the JOB directory", required=True)
    args = parser.parse_args()
    ##################
    ## Reading cfgs ##
    ##################
    ConfigFiles  = glob.glob(args.CFGpath+"/*.cfg")
    Layers = []
    ModelNames = []
    FirstLayerNodes = []
    DropoutLen = []
    BatchNormLen = []
    for File in ConfigFiles:
        cfg_settings = Settings(File)
        Layers.append(len(cfg_settings.get_Model().get_Nodes()))
        FirstLayerNodes.append(cfg_settings.get_Model().get_Nodes()[0])
        if -1 in cfg_settings.get_Model().get_DropoutIndece():
            DropoutLen.append(0)
        else:
            DropoutLen.append(len(cfg_settings.get_Model().get_DropoutIndece()))
        if -1 in cfg_settings.get_Model().get_BatchNormIndece():
            BatchNormLen.append(0)
        else:
            BatchNormLen.append(len(cfg_settings.get_Model().get_BatchNormIndece()))
        ModelNames.append(cfg_settings.get_Model().get_Name()+"_"+cfg_settings.get_Model().get_Type())
    cfg_df = pd.DataFrame(data=list(zip(ModelNames, Layers,FirstLayerNodes,DropoutLen,BatchNormLen)), columns=["Model","Layers","Nodes in 1st Layer", "Number of Dropout Layers","Number of BatchNorm Layers"])
    ##################
    ## Reading loss ##
    ##################
    HistoryFiles = glob.glob(args.JOBpath+"/Model/*history*")
    dictionary = {File.split("_history")[0].replace(args.JOBpath+"/Model/",""): [] for File in HistoryFiles}
    for File in HistoryFiles:
        dictionary[File.split("_history")[0].replace(args.JOBpath+"/Model/","")].append(File)
    Fnames        = []
    Mean_Val_Loss = []
    Std_Val_Loss  = []
    for key,Files in dictionary.items():
        Losses = [pd.read_json(File)["val_loss"].values[-1] for File in Files]
        Fnames.append(key)
        Mean_Val_Loss.append(np.mean(Losses))
        Std_Val_Loss.append(np.std(Losses))
    pd.options.display.float_format = "{:,.4f}".format
    res_df = pd.DataFrame(data=np.array([Fnames,Mean_Val_Loss,Std_Val_Loss]).T, columns=["Model","Validation Loss","Validation Loss unc."])
    #################
    #### Merging ####
    #################
    merged = res_df.merge(cfg_df,on="Model")
    merged_sorted = merged.sort_values(by=["Validation Loss"])
    merged_sorted = merged_sorted.reset_index(drop=True)
    merged_sorted = merged_sorted.astype({"Validation Loss": "float32", "Validation Loss unc.": "float32"})
    
    print(merged_sorted)
    print("Writing DataFrame to %s"%args.JOBpath+"/HO_Results.csv")
    merged_sorted.to_csv(args.JOBpath+"/HO_Results.csv", index=True)
    print("Writing DataFrame to %s"%args.JOBpath+"/HO_Results.tex")
    merged_sorted.to_latex(args.JOBpath+"/HO_Results.tex", index=True)

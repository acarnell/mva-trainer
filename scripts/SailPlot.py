#!/usr/bin/env python3

import argparse, ctypes, copy
from ROOT import TH2D, TCanvas, TGraph, TLine, TMultiGraph, TLegend, gROOT
from HelperModules import AtlasStyle
from HelperModules.HelperFunctions import ensure_trailing_slash
from HelperModules.AtlasLabel import ATLASLabel, CustomLabel, DrawLabels
from root_numpy import array2hist
import numpy as np
import pandas as pd

def PlotHisto(df, var="ttZ_SR", classifier="ttZ_multi_class_Classifier_v1", label="Events", zmin=None, zmax=None):
    '''
    Function to plot a 2D histogram/contour plot for different variables.
    
    Keyword arguments:
    var        --- variable to be plotted
    classifier --- Classifier name
    label      --- Label for the colour bar
    '''
    ttZ_array = df[df["Classifier"]==classifier].sort_values(["tZ Cut", "Diboson Cut"], ascending=[True,True])[var].values.reshape(100,100)
    ttZ_Hist = TH2D("ttZ_Hist", "ttZ_Hist",100,0,1,100,0,1)
    Canvas = TCanvas("Canvas", "Canvas",800,600)
    array2hist(ttZ_array,ttZ_Hist)
    ttZ_Hist.Draw("CONT1Z")
    ttZ_Hist.GetYaxis().SetTitle("Diboson Cut")
    ttZ_Hist.GetXaxis().SetTitle("tZ Cut")
    ttZ_Hist.GetZaxis().SetTitle(label)
    if zmax == None:
        zmax = ttZ_Hist.GetMaximum()
    if zmin == None:
        zmin = ttZ_Hist.GetMinimum()
    ttZ_Hist.SetMaximum(zmax)
    ttZ_Hist.SetMinimum(zmin)
    DrawLabels("Internal", "13 TeV", "t#bar{t}Z 3L", Option="MC", align="left")
    Canvas.SetRightMargin(0.15)
    Canvas.SaveAs(ensure_trailing_slash(args.outputpath)+classifier+"_"+var+".pdf")
    del Canvas

def PlotScatter(df, var_1="SsqrtB_SR",var_2="ttZ_SR", classifier="ttZ_multi_class_Classifier_v1", xlabel="S/#sqrt{B}", ylabel="Signal Events", Xinclusive=0, Yinclusive=0, Xdifferential=0, Ydifferential=0, Legend_pos="left"):
    '''
    Function to plot a 2D scatter plot for different variables.
    
    Keyword arguments:
    var_1      --- variable for the first axis
    var_2      --- variable for the second axis
    classifier --- Classifier name
    xlabel     --- Label for the x-axis
    ylabel     --- Label for the y-axis
    Xinclusive --- value for colour-coding better/worse regarding variable on first axis for inclusive measurement
    Yinclusive --- value for colour-coding better/worse regarding variable on first axis for inclusive measurement
    Xdifferential --- value for colour-coding better/worse regarding variable on first axis for differential measurement
    Ydifferential --- value for colour-coding better/worse regarding variable on first axis for differential measurement
    Legend_post --- Position of the legend. Can be 'left' or 'right'
    '''

    is_better_inclusive    = ((df[var_1]>=Xinclusive)&(df[var_2]>=Yinclusive)).values
    is_better_differential = ((df[var_1]>=Xdifferential)&(df[var_2]>=Ydifferential)).values
    df["better inclusive"]    = is_better_inclusive    # Define a True/False column for configurations that are better with respect to Xinclusive & Yinclusive
    df["better differential"] = is_better_differential # Define a True/False column for configurations that are better with respect to Xdifferential & Ydifferential
    
    # Combine logic from above into 4 cases:
    better_inclusive_and_differential = df[(df["Classifier"]==classifier)&(df["better inclusive"]==True)&(df["better differential"]==True)]
    better_inclusive                  = df[(df["Classifier"]==classifier)&(df["better inclusive"]==True)&(df["better differential"]==False)]
    better_differential               = df[(df["Classifier"]==classifier)&(df["better inclusive"]==False)&(df["better differential"]==True)]
    worse_inclusive_and_differential  = df[(df["Classifier"]==classifier)&(df["better inclusive"]==False)&(df["better differential"]==False)]

    Graphs = {}
    if(len(better_inclusive_and_differential)>0):
        Graphs["g_better_inclusive_and_differential"] = TGraph(len(better_inclusive_and_differential),better_inclusive_and_differential[var_1].values,better_inclusive_and_differential[var_2].values)
    if(len(better_inclusive)>0):
        Graphs["g_better_inclusive"] = TGraph(len(better_inclusive),better_inclusive[var_1].values,better_inclusive[var_2].values)
    if(len(better_differential)>0):
        Graphs["g_better_differential"] = TGraph(len(better_differential),better_differential[var_1].values,better_differential[var_2].values)
    if(len(worse_inclusive_and_differential)>0):
        Graphs["g_worse_inclusive_and_differential"] = TGraph(len(worse_inclusive_and_differential),worse_inclusive_and_differential[var_1].values,worse_inclusive_and_differential[var_2].values)

    for key,graph in Graphs.items():
        graph.SetMarkerSize(0.5)
        if(key == "g_better_inclusive_and_differential"):
            graph.SetMarkerColor(3)
        if(key == "g_better_inclusive"):
            graph.SetMarkerColor(6)
        if(key == "g_better_differential"):
            graph.SetMarkerColor(4)
        if(key == "g_worse_inclusive_and_differential"):
            graph.SetMarkerColor(2)

    Canvas = TCanvas("Canvas", "Canvas",800,600)
    if Legend_pos=="left":
        Legend = TLegend(.2,.60,.3,.70)
    if Legend_pos=="right":
        Legend = TLegend(0.6,0.75,0.9,0.85)
    Legend.SetBorderSize(0)
    Legend.SetTextFont(42)
    Legend.SetTextSize(0.035)
    Legend.SetFillStyle(0)
    mg = TMultiGraph()
    for key, graph in Graphs.items():
        mg.Add(graph)
        if(key == "g_better_inclusive_and_differential"):
            Legend.AddEntry(graph,"Better incl. and diff.","p")
        if(key == "g_better_inclusive"):
            Legend.AddEntry(graph,"Better incl.","p")
        if(key == "g_better_differential"):
            Legend.AddEntry(graph,"Better diff.","p")
        if(key == "g_worse_inclusive_and_differential"):
            Legend.AddEntry(graph,"Worse incl. and diff.","p")
    mg.Draw("ap")
    Legend.Draw("Same")
    DrawLabels("Internal", "13 TeV", "t#bar{t}Z 3L", Option="MC", align="left")
    Canvas.SaveAs(ensure_trailing_slash(args.outputpath)+classifier+"_"+var_1+"_versus_"+var_2+".pdf")
    del Canvas

    # Here we do the same things again but this time we plot the good/bad configurations in the original 2D plane
    Graphs = {}
    if(len(better_inclusive_and_differential)>0):
        Graphs["g_better_inclusive_and_differential"] = TGraph(len(better_inclusive_and_differential),better_inclusive_and_differential["tZ Cut"].values,better_inclusive_and_differential["Diboson Cut"].values)
    if(len(better_inclusive)>0):
        Graphs["g_better_inclusive"] = TGraph(len(better_inclusive),better_inclusive["tZ Cut"].values,better_inclusive["Diboson Cut"].values)
    if(len(better_differential)>0):
        Graphs["g_better_differential"] = TGraph(len(better_differential),better_differential["tZ Cut"].values,better_differential["Diboson Cut"].values)
    if(len(worse_inclusive_and_differential)>0):
        Graphs["g_worse_inclusive_and_differential"] = TGraph(len(worse_inclusive_and_differential),worse_inclusive_and_differential["tZ Cut"].values,worse_inclusive_and_differential["Diboson Cut"].values)

    for key,graph in Graphs.items():
        graph.SetMarkerSize(0.5)
        if(key == "g_better_inclusive_and_differential"):
            graph.SetMarkerColor(3)
        if(key == "g_better_inclusive"):
            graph.SetMarkerColor(6)
        if(key == "g_better_differential"):
            graph.SetMarkerColor(4)
        if(key == "g_worse_inclusive_and_differential"):
            graph.SetMarkerColor(2)

    Canvas2 = TCanvas("Canvas2", "Canvas2",800,600)
    if Legend_pos=="left":
        Legend = TLegend(.2,.60,.3,.70)
    if Legend_pos=="right":
        Legend = TLegend(0.6,0.75,0.9,0.85)
    Legend.SetBorderSize(0)
    Legend.SetTextFont(42)
    Legend.SetTextSize(0.035)
    Legend.SetFillStyle(0)
    mg = TMultiGraph()
    for key, graph in Graphs.items():
        mg.Add(graph)
        if(key == "g_better_inclusive_and_differential"):
            Legend.AddEntry(graph,"Better incl. and diff.","p")
        if(key == "g_better_inclusive"):
            Legend.AddEntry(graph,"Better incl.","p")
        if(key == "g_better_differential"):
            Legend.AddEntry(graph,"Better diff.","p")
        if(key == "g_worse_inclusive_and_differential"):
            Legend.AddEntry(graph,"Worse incl. and diff.","p")
    mg.Draw("ap")
    Legend.Draw("Same")
    DrawLabels("Internal", "13 TeV", "t#bar{t}Z 3L", Option="MC", align="left")
    Canvas2.SaveAs(ensure_trailing_slash(args.outputpath)+classifier+"_"+"tZ Cut"+"_versus_"+"Diboson Cut_"+var_1+"_versus_"+var_2+".pdf")
    del Canvas2

if __name__ == "__main__":
    gROOT.SetBatch()
    # Change according to your needs
    
    parser = argparse.ArgumentParser(description='Process Inputs.')
    parser.add_argument("-i", "--inputfile", help="Input file", required=True)
    parser.add_argument("-o", "--outputpath", help="Output path", required=True)
    parser.add_argument("-c", "--classifiers", help="Classifier names", required=True, nargs='+')
    args = parser.parse_args()

    ###########################################
    ################ Constants ################
    ###########################################
    x_inclusive = 27.2119         # S/sqrt(B) in inclusive measurement
    y_inclusive = 205.673+162.474 # Signal yield in inclusive measurement
    x_differential = 24.530       # S/sqrt(B) in differential measurement
    y_differential = 337.048      # Signal yield in differential measurement
    x_inclusive_SB = 2.011        # S/B in inclusive measurement
    x_differential_SB = 1.785     # S/B in differential measurement
    
    ###############################################
    ################ some DF magic ################
    ###############################################
    df = pd.read_csv(args.inputfile, delimiter = "\t") # reading the input file
    df["SB_SR"] = df["ttZ_SR"]/(df["ttW_SR"]+df["ttH_SR"]+df["tZ_SR"]+df["tWZ_SR"]+df["WZb_SR"]+df["WZc_SR"]+df["WZl_SR"]+df["ZZb_SR"]+df["ZZc_SR"]+df["ZZl_SR"]+df["Fakes_SR"]+df["Other_SR"])
    df["SsqrtB_SR"] = df["ttZ_SR"]/np.sqrt(df["ttW_SR"]+df["ttH_SR"]+df["tZ_SR"]+df["tWZ_SR"]+df["WZb_SR"]+df["WZc_SR"]+df["WZl_SR"]+df["ZZb_SR"]+df["ZZc_SR"]+df["ZZl_SR"]+df["Fakes_SR"]+df["Other_SR"])
    df["SBSsqrtB_SR"] = 2/(1/df["SB_SR"]+1/df["SsqrtB_SR"])
    df["WZb_CR_Purity"] = df["WZb_WZ_CR"]/np.sqrt(df["ttZ_WZ_CR"]+df["tZ_WZ_CR"])
    df["WZb_CR_Purity_Unc"] = np.sqrt(df["WZb_WZ_CR"]/(df["ttZ_WZ_CR"]+df["tZ_WZ_CR"]) + 0.25*df["WZb_WZ_CR"]/(df["ttZ_WZ_CR"]+df["tZ_WZ_CR"])**3 + 0.25*df["tZ_WZ_CR"]/(df["ttZ_WZ_CR"]+df["tZ_WZ_CR"])**3)
    df["tZ_CR_Purity"] = df["tZ_tZ_CR"]/np.sqrt(df["ttZ_tZ_CR"]+df["WZb_tZ_CR"])
    df["tZ_CR_Purity_Unc"] = np.sqrt(df["tZ_tZ_CR"]/(df["ttZ_tZ_CR"]+df["WZb_tZ_CR"]) + 0.25*df["WZb_tZ_CR"]/(df["ttZ_tZ_CR"]+df["WZb_tZ_CR"])**3 + 0.25*df["ttZ_tZ_CR"]/(df["ttZ_tZ_CR"]+df["WZb_tZ_CR"])**3)
    df["ttZ_SR_Purity"] = df["ttZ_SR"]/np.sqrt(df["WZb_SR"]+df["tZ_SR"])
    df["ttZ_SR_Purity_Unc"] = np.sqrt(df["ttZ_SR"]/(df["tZ_SR"]+df["WZb_SR"]) + 0.25*df["WZb_SR"]/(df["tZ_SR"]+df["WZb_SR"])**3 + 0.25*df["tZ_SR"]/(df["tZ_SR"]+df["WZb_SR"])**3)
    df["S/sqrt(S+Bs)_SR"] = df["ttZ_SR"]/np.sqrt(df["ttZ_SR"]+df["ttW_SR"]+df["ttH_SR"]+df["tZ_SR"]+df["tWZ_SR"]+df["WZb_SR"]+df["WZc_SR"]+df["WZl_SR"]+df["ZZb_SR"]+df["ZZc_SR"]+df["ZZl_SR"]+df["Fakes_SR"]+df["Other_SR"]+(0.5*df["WZb_SR"])**2+(0.1*df["tWZ_SR"])**2+(0.5*df["Fakes_SR"])**2)
    df["S/sqrt(S+B+F)_SR"] = df["ttZ_SR"]/np.sqrt(df["ttZ_SR"]+df["ttW_SR"]+df["ttH_SR"]+df["tZ_SR"]+df["tWZ_SR"]+df["WZb_SR"]+df["WZc_SR"]+df["WZl_SR"]+df["ZZb_SR"]+df["ZZc_SR"]+df["ZZl_SR"]+df["Fakes_SR"]+df["Other_SR"]+(0.5*df["Fakes_SR"])**2)
    
    df = df.replace([np.inf, -np.inf], np.nan) # sometimes we can have NAN...
    df = df.fillna(0)
    
    df.loc[(df["SsqrtB_SR"]>=x_inclusive)&(df["ttZ_SR"]>=y_inclusive),"SsqrtB_SR better inclusive"]          = True
    df.loc[(df["SsqrtB_SR"]>=x_differential)&(df["ttZ_SR"]>=y_differential),"SsqrtB_SR better differential"] = True
    df.loc[(df["SB_SR"]>=x_inclusive_SB)&(df["ttZ_SR"]>=y_inclusive),"SB_SR better inclusive"]               = True
    df.loc[(df["SB_SR"]>=x_differential_SB)&(df["ttZ_SR"]>=y_differential),"SB_SR better differential"]      = True

    # Some prints of useful configurations, change this to your needs
    for classifier in args.classifiers:
        print("--------------------")
        print(df[(df["Classifier"]==classifier)&(df["SsqrtB_SR better inclusive"]==True)&(df["SsqrtB_SR better differential"]==True)
                 &(df["SB_SR better inclusive"]==True)&(df["SB_SR better differential"]==True)].sort_values(["SsqrtB_SR"],ascending=[False])[["Diboson Cut", "tZ Cut", "SB_SR","SsqrtB_SR","ttZ_SR"]].head(1).to_string(index=False))
        print("--------------------")
        print(df[(df["Classifier"]==classifier)&(df["SsqrtB_SR better inclusive"]==True)&(df["SsqrtB_SR better differential"]==True)
                 &(df["SB_SR better inclusive"]==True)&(df["SB_SR better differential"]==True)].sort_values(["SB_SR"],ascending=[False])[["Diboson Cut", "tZ Cut", "SB_SR","SsqrtB_SR","ttZ_SR"]].head(1).to_string(index=False))
        best_choice_ttZ = df[(df["Classifier"]==classifier)&(df["SsqrtB_SR better inclusive"]==True)&(df["SsqrtB_SR better differential"]==True)
                 &(df["SB_SR better inclusive"]==True)&(df["SB_SR better differential"]==True)].sort_values(["SB_SR"],ascending=[False])[["Diboson Cut", "tZ Cut"]].head(1).values[0]
        print("--------------------")
        print(df[(df["Classifier"]==classifier)&(df["SsqrtB_SR better inclusive"]==True)&(df["SsqrtB_SR better differential"]==True)].sort_values(["ttZ_SR"],ascending=[False])[["Diboson Cut", "tZ Cut", "ttZ_SR"]].head(1).to_string(index=False))
        print("--------------------")
        print(df[(df["Classifier"]==classifier)&(df["Diboson Cut"]>=best_choice_ttZ[0])].sort_values(["WZb_CR_Purity"],ascending=[False])[["Diboson Cut", "tZ Cut", "WZb_CR_Purity"]].head(1).to_string(index=False))
        best_choice_WZb = df[(df["Classifier"]==classifier)&(df["Diboson Cut"]>=best_choice_ttZ[0])].sort_values(["WZb_CR_Purity"],ascending=[False])[["Diboson Cut", "tZ Cut"]].head(1).values[0]
        print("--------------------")
        print(df[(df["Classifier"]==classifier)&(df["Diboson Cut"]<=best_choice_WZb[0])&((df["tZ Cut"]>best_choice_ttZ[1]))].sort_values(["tZ_CR_Purity"],ascending=[False])[["Diboson Cut", "tZ Cut", "tZ_CR_Purity"]].head(1).to_string(index=False))
        best_choice_tZ = df[(df["Classifier"]==classifier)&(df["Diboson Cut"]<=best_choice_WZb[0])&((df["tZ Cut"]>=best_choice_ttZ[1]))].sort_values(["tZ_CR_Purity"],ascending=[False])[["Diboson Cut", "tZ Cut"]].head(1).values[0]
        print("--------------------")

    df_new = copy.deepcopy(df[(df["SB_SR"]<10)&(df["SB_SR"]>0)])
    for classifier in args.classifiers:
        # 2D Plots
        PlotHisto(df, var="SBSsqrtB_SR", classifier=classifier, label="S/#sqrt{B}", zmax=35, zmin=0)
        PlotHisto(df, var="SsqrtB_SR", classifier=classifier, label="S/#sqrt{B}", zmax=35, zmin=0)
        PlotHisto(df, var="SB_SR", classifier=classifier, label="S/B", zmax=5, zmin=0)
        PlotHisto(df, var="WZb_SR", classifier=classifier)
        PlotHisto(df, var="tZ_SR", classifier=classifier)
        PlotHisto(df, var="tWZ_SR", classifier=classifier)
        PlotHisto(df, var="ttZ_SR", classifier=classifier)
        # Scatter Plots
        PlotScatter(df, classifier=classifier, Xinclusive=x_inclusive, Yinclusive=y_inclusive, Xdifferential=x_differential, Ydifferential=y_differential)
        PlotScatter(df_new, classifier=classifier, var_1="SB_SR", xlabel="S/B", Xinclusive=x_inclusive_SB, Yinclusive=y_inclusive, Xdifferential=x_differential_SB, Ydifferential=y_differential, Legend_pos="right")

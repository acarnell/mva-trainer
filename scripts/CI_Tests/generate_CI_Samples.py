import uproot, os
import numpy as np

print("CI PREPARATION:\t creating Ntuple directory")
os.makedirs("CI_Test_Ntuples",exist_ok=True)

################
### Sample A ###
################
print("CI PREPARATION:\t creating sample_a.root")
with uproot.recreate("CI_Test_Ntuples/sample_a.root") as f_a:
    var_dict = {"Variable_1": np.random.normal(loc=0.0,scale=1.0, size=100000),
                "Variable_2": np.random.normal(loc=1.0,scale=1.0, size=100000),
                "Variable_3": np.random.normal(loc=2.0,scale=1.0, size=100000),
                "Variable_4": np.random.normal(loc=1.0,scale=2.0, size=100000),
                "Variable_5": np.random.normal(loc=2.0,scale=2.0, size=100000),
                "weight_1": np.random.normal(loc=1.0,scale=0.1, size=100000),
                "weight_2": np.random.normal(loc=1.0,scale=0.1, size=100000),
                "Selection_1": np.random.normal(loc=1.0,scale=1.0, size=100000),
                "Selection_2": np.random.normal(loc=1.0,scale=1.0, size=100000),
                "eventNumber": np.arange(100000)}
    f_a["test_tree"] = var_dict
    f_a["truth"] = {"Truth_Variable": var_dict["Variable_1"]+var_dict["Variable_2"]+var_dict["Variable_3"]+var_dict["Variable_4"]+10,
                    "eventNumber": np.arange(100000)}
f_a.close()
################
### Sample B ###
################
print("CI PREPARATION:\t creating sample_b.root")
with uproot.recreate("CI_Test_Ntuples/sample_b.root") as f_b:
    var_dict = {"Variable_1": np.random.normal(loc=3.0,scale=1.0, size=100000),
               "Variable_2": np.random.normal(loc=4.0,scale=1.0, size=100000),
               "Variable_3": np.random.normal(loc=5.0,scale=1.0, size=100000),
               "Variable_4": np.random.normal(loc=4.0,scale=2.0, size=100000),
               "Variable_5": np.random.normal(loc=5.0,scale=2.0, size=100000),
               "weight_1": np.random.normal(loc=1.0,scale=0.1, size=100000),
               "weight_2": np.random.normal(loc=1.0,scale=0.1, size=100000),
               "Selection_1": np.random.normal(loc=1.0,scale=1.0, size=100000),
               "Selection_2": np.random.normal(loc=1.0,scale=1.0, size=100000),
               "eventNumber": np.arange(100000)}
    f_b["test_tree"] = var_dict
    f_b["truth"] = {"Truth_Variable": var_dict["Variable_1"]+var_dict["Variable_2"]+var_dict["Variable_3"]+var_dict["Variable_4"]+10,
                    "eventNumber": np.arange(100000)}
f_b.close()
################
### Sample C ###
################
print("CI PREPARATION:\t creating sample_c.root")
with uproot.recreate("CI_Test_Ntuples/sample_c.root") as f_c:
    var_dict = {"Variable_1": np.random.normal(loc=6.0,scale=1.0, size=100000),
                "Variable_2": np.random.normal(loc=7.0,scale=1.0, size=100000),
                "Variable_3": np.random.normal(loc=8.0,scale=1.0, size=100000),
                "Variable_4": np.random.normal(loc=7.0,scale=2.0, size=100000),
                "Variable_5": np.random.normal(loc=8.0,scale=2.0, size=100000),
                "weight_1": np.random.normal(loc=1.0,scale=0.1, size=100000),
                "weight_2": np.random.normal(loc=1.0,scale=0.1, size=100000),
                "Selection_1": np.random.normal(loc=1.0,scale=1.0, size=100000),
                "Selection_2": np.random.normal(loc=1.0,scale=1.0, size=100000),
                "eventNumber": np.arange(100000)}
    f_c["test_tree"] = var_dict
    f_c["truth"] = {"Truth_Variable": var_dict["Variable_1"]+var_dict["Variable_2"]+var_dict["Variable_3"]+var_dict["Variable_4"]+10,
                    "eventNumber": np.arange(100000)}
    
f_c.close()
################
##### Data #####
################
print("CI PREPARATION:\t creating test_data.root")
with uproot.recreate("CI_Test_Ntuples/test_data.root") as f_data:
    var_dict = {"Variable_1": np.concatenate((np.random.normal(loc=0.0,scale=1.0, size=100000),
                                              np.random.normal(loc=3.0,scale=1.0, size=100000),
                                              np.random.normal(loc=6.0,scale=1.0, size=100000)),axis=None),
                "Variable_2": np.concatenate((np.random.normal(loc=1.0,scale=1.0, size=100000),
                                              np.random.normal(loc=4.0,scale=1.0, size=100000),
                                              np.random.normal(loc=7.0,scale=1.0, size=100000)),axis=None),
                "Variable_3": np.concatenate((np.random.normal(loc=2.0,scale=1.0, size=100000),
                                              np.random.normal(loc=5.0,scale=1.0, size=100000),
                                              np.random.normal(loc=8.0,scale=1.0, size=100000)),axis=None),
                "Variable_4": np.concatenate((np.random.normal(loc=1.0,scale=2.0, size=100000),
                                              np.random.normal(loc=4.0,scale=2.0, size=100000),
                                              np.random.normal(loc=7.0,scale=2.0, size=100000)),axis=None),
                "Variable_5": np.concatenate((np.random.normal(loc=2.0,scale=2.0, size=100000),
                                              np.random.normal(loc=5.0,scale=2.0, size=100000),
                                              np.random.normal(loc=8.0,scale=2.0, size=100000)),axis=None),
                "weight_1": np.random.normal(loc=1.0,scale=0.1, size=300000),
                "weight_2": np.random.normal(loc=1.0,scale=0.1, size=300000),
                "Selection_1": np.random.normal(loc=1.0,scale=1.0, size=300000),
                "Selection_2": np.random.normal(loc=1.0,scale=1.0, size=300000),
                "eventNumber": np.arange(300000)}
    f_data["test_tree"] = var_dict
f_data.close()
print("CI PREPARATION:\t Done!")

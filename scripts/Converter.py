#!/usr/bin/env python3
"""
Main steering script for conversion
"""
import argparse, sklearn
from HelperModules.ConversionHandler import ConversionHandler

if __name__ == "__main__":
    parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument("-c", "--configfile", help="Config file", required=True)
    parser.add_argument("-f", "--filter", dest="wildcard", help="additional string to filter input files", type=str, default='')
    parser.add_argument("--processes", help="the number of parallel processes to run", type=int, default=8)
    args = parser.parse_args()

    ConvHandler = ConversionHandler(args)
    ConvHandler.PerformConversion()

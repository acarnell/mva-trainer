#!/usr/bin/env python3
"""
Main steering script for training
"""
from HelperModules import Settings
from HelperModules.Directories import Directories
from HelperModules.DataHandler import DataHandler
from HelperModules.MessageHandler import WelcomeMessage, EnvironmentalCheck
import HelperModules.HelperFunctions as hf
from shutil import copyfile
import argparse, os

if __name__ == "__main__":
    parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument("-c", "--configfile", help="Config file", required=True)
    args = parser.parse_args()

    WelcomeMessage("Trainer")
    EnvironmentalCheck(check="env")

    # Here we create the necessary settings objects and read information from the config file
    cfg_settings = Settings.Settings(args.configfile, option="all")
    Dirs         = Directories(cfg_settings.get_General().get_Job())

    # Copy the config file to the job directory
    copyfile(args.configfile, hf.ensure_trailing_slash(Dirs.ConfigDir())+os.path.basename(args.configfile).replace(".cfg","_training_step.cfg"))
    
    cfg_settings.Print()

    DH = DataHandler(cfg_settings)
   
    DH.TrainAllFolds()
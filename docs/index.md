# Documentation for mva-trainer

`mva-trainer` is a framework for the training of deep neural networks and decision trees. The code can be found on [GitLab](https://gitlab.cern.ch/skorn/mva-trainer). This website collects documentation and tutorials for the `mva-trainer` framework. The navigation is performed using the panel on the left.

This framework builds MVA-based models such as deep neural networks or decision trees based on [Keras](https://keras.io/), [TensorFlow](https://www.tensorflow.org/), and [scikit-learn](https://www.google.com/search?channel=fs&client=ubuntu&q=scikitlearn).
Its intention is to allow for an easy introduction into potent MVA-based techniques for HEP analyses.

### Run an injection
If you want to work directly with your MVA outputs you should "re-inject" them into your Ntuples.
The idea of this process is that the MVA outputs are added as additional branches to the Ntuples. During this process a copy of the Ntuples is produced, including the newly added branches.

To inject NN predictions back into Ntuples you can do:
```sh
./scripts/MVAInjector.py -i <input_path> -o <output_path> -c <config_path> -m <model_path> --processes <n_processes> --filter <filter_string>
```
Hereby `<input_path>` describes the path to input Ntuples. The `<output_path>` argument describes the path to an output directory in which the injected Ntuples will be stored. The directory and the sub-directory structure will automatically be created for you. The config file is **again** passed via the `<config_path>` argument.
The trained model will **again** be taken from the `<model_path>`. You can pass multiple config files and multiple model paths to simultaneously inject multiple neural networks into Ntuples.
Using the *processes* argument you can specify a number of independent processes that are run simultaneously. Each process will inject the prediction into one root file and start a new process upon termination. Using the *filter* argument you can restrict your injection using a string as a wildcard.
Keep in mind that this can be very memory-consuming because the entire root-file is loaded. Restrict this to a lower number (e.g. 2-4) in case you have memory limitations.

### Prepare files for usage with lwtnn

If you want to include your model in your c++ code it is recommended to use lwtnn.

After the training of your model(s) is done you should see (among others) these files:
```
Model/
    ├── Your_Model_Weights_Fold0.h5
    ├── Your_Model_Architecture_Fold0.h5
    └── ...
lwtnn/
    └── Your_Model_Variables.json
```
Keep in mind that your prefix will look different depending on your chosen model name and type, *Your_Model* is just a placeholder here.
While the weights and architectures are split by fold, the variables file is not.
After installing [lwtnn](https://github.com/lwtnn/lwtnn) you will be able to run the [lwtnn converter](https://github.com/lwtnn/lwtnn/wiki/Keras-Converter) for each fold like this:
```
kerasfunc2json Your_Model_Architecture_Fold0.h5 Your_Model_Weights_Fold0.h5 Your_Model_Variables.json > Your_lwtnn_File_Fold0.json
```
You can find an example on how to run your lwtnn model [here](https://github.com/lwtnn/lwtnn/wiki/Running-on-CERN-AFS).
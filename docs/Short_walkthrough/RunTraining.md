# Run a training

After you successfully ran a conversion you can now run the training of your model.
The training can be started by doing:
```sh
./scripts/Trainer.py -c <config_path>
```
The `<config_path>` parameter again is the path to your config file.

The training script processes the previously during the conversion generated `.h5` files (strictly speaking the merged files).
Once the training starts you will see some output on your settings as well as on the training procedure itself. After the training subsequent files are written (e.g. history files or files holding model weights and biases).
You can find your model in the `Model` directory within the directory of the job.
# Run hyperparameter optimisation

Hyparameter optimisation is a key aspect for training a good neural network. 
At the moment the hyperparameter optimisation can be performed for deep neural networks.
To run hyperparameter optimisation you can use the `Optimise.py` script.
```sh
./scripts/Optimise.py -c <config_path> -cp <HTCondorConfig_path> --RunOption <RunOption> --NNetworks <NNetworks> --RandomSeed <RandomSeed>
```
This script will take the config file specified by `config_path` as a "seed config" and create `NNetworks` number of new randomised (control seed using `RandomSeed`) configs in the `HTCondorConfig_path` directory. Furthermore `.sub` (and `.dag`) files are created.
Three `RunOption`s are available to the user:

| Option | Effect |
| ------ | ------ |
| ConversionAndTraining | In this case a `.dag` file is created in the specified `HTCondorConfig_path` directory. In addition two `.sub` files are created, one for the conversion and one for the training. The files are created in a way that the conversion only runs once per optimisation. To start the optimisation (in this case a conversion followed by the training of all networks) simply submit the `.dag` file to the HTCondor deamon using the `condor_submit_dag` command.|
| Conversion | In this case a `.sub` file is created in the specified `HTCondorConfig_path` directory. To start the conversion (in this case only the conversion is run) simply submit the `.sub` file to the HTCondor deamon using the `condor_submit` command.|
| Training | In this case a `.sub` file is created in the specified `HTCondorConfig_path` directory. To start the training (in this case only the training is run) simply submit the `.sub` file to the HTCondor deamon using the `condor_submit` command. **For this job to work converted files must already exist.**|


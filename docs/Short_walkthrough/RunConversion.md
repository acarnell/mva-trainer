# Run a conversion

The conversion usually is the first things you have to do. The goal of the conversion is to convert `.root` files into `.hdf5` (.h5) files which can be easily handled by subsequent python libraries, especially [TensorFlow](https://www.tensorflow.org/) and [Keras](https://keras.io/).

The conversion is needed, because the subsequent scripts rely on the presence of `.hdf5` files to work.
Before you start with the conversion you should open the setup script and check whether the environmental variables

  - `NTUPLE_VERSION`
  - `NTUPLE_LOCATION`

are correct, i.e. `NTUPLE_LOCATION` should specify a directory holding `NTUPLE_VERSION`, i.e. the directory holding your Ntuples. Make sure that you pass the absolute path to your `NTUPLE_LOCATION`.
The introduction of multiple variables allows you to have one common ntuple directory with several version of the Ntuples.
An example of the file structure could be this:
```
<path_to_Ntuple_directory>
    ├── Ntuple_Version_1/
    ├── Ntuple_Version_2/
    └── Ntuple_Version_3/
```
resulting in these lines:
```
export NTUPLE_VERSION="Ntuple_Version_1"
export NTUPLE_LOCATION="path_to_Ntuple_directory"
```
In case you only want to change the version you can do that easily in the setup script by changing the path.
Afterwards you need to source the setup script again with:
```sh
source setup.sh
```
## Setting up a config file

Before you start converting files, you need to define a config file.
In the [config directory](configs) you find some example configs such as

  - ExampleClassification.cfg,
  - ExampleReconstruction.cfg,
  - and ExampleRegression.cfg.

Open `ExampleClassification.cfg`. The config file consists of several blocks. The GENERA block followed by several SAMPLE blocks and finally the MODEL block.
The GENERAL block holds general config options such as the (general) `Selection` and the (general) `MCWeight` definitions. Since the code uses *uproot* you need to pass logic strings in a python-conform way,
i.e.
```
(A)&(B)|(C)
```
instead of the usual c++ way:
```
(A)&&(B)||(C)
```
If you define new variables to be used, you need to check the Histograms.py file and add information on your new variables in the two respective dictionaries in the file to tell the code the labels and binnings you want to use.

## Performing the conversion

From the main directory you can then do:
```sh
./scripts/Converter.py -c <config_path>
```
Hereby `<config_path>` specifies the path to your config file.
Upon running the script a new directory is created based on the 'Job' option declared in the given config file.
The directory structure looks like this:
```
<JobName>
    ├── Configs/
    ├── Data/
    ├── Model/
    └── Plots/
```
`Configs` will contain copies of all used config files. `Data` will contain dataframes stored in `.h5` format. `Model` will contain all model related files and `Plots` will contain all control plots (and tables).

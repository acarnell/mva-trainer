# Run an evaluation

The most important step after a successfull training is the evaluation of the model. This step is vital for finding problems and improving on previous results.
During the evaluation several additional plots are produced and stored in the `MVAPlots` directory within the `Plots` directory.

To evaluate a training/model you can do:
```sh
./scripts/Evaluate.py -c <config_path>
```
Here `<config_path>` is again the path to your config file.
# Setting up mva-trainer

To obtain the code simply clone the repository using a protocol of your choice:
```sh
git clone ssh://git@gitlab.cern.ch:7999/skorn/mva-trainer.git
```
If this is your first time cloning the repository you need to verify that you have the necessary python modules installed.
For this purpose run the `setup.sh` script via:
```sh
source setup.sh
```
The script will check whether the necessary packages exist. If packages are missing an error message will be printed.
Please install any missing packages.

Each time you start a new shell you need to run the setup script again. It will set environmental variables that are picked up by the code.
The code itself is python based and hence does not need any compilation.
# Dockerising the code

A `Dockerfile` is attached so you can create your own docker image of the code.
The dockerimage is based on [rootproject/root:latest](https://hub.docker.com/u/rootproject). This way a running version of ROOT within the container is ensured.

Apart from ROOT several python modules will be installed such as:

  - numpy
  - uproot
  - pandas
  - scikit-learn
  - tensorflow
  - tables
  - matplotlib
  - pydot

To allow for code development inside the container images (this is surely not the intended use case) several editors as well as git are installed to.

For building call:
```sh
docker build -t my_image --rm .
```
For running call:
```sh
docker run -it --name my_my_trainer --rm my_image
```
This will generate a docker container called `my_image` and run a docjer container called `my_mva_trainer`. Upon calling the build command all dependencies are installed automatically for you. You can then simply commence with sourcing the setup script. For information how to save container data see [here](https://medium.com/swlh/dockerize-your-python-command-line-program-6a273f5c5544).

E.g. to gain access to your ntuple directory you shoud do:
```sh
docker run -it --name my_mva_trainer --rm -v <absolute_path_to_Ntuple_directory>:/home/mva_trainer_user/Ntuples/ my_image
```
where `<absolute_path_to_Ntuple_directory>` is the absolute path to your `ntuple directory`. This directory will then be mounted in the home directory of the docker container (here as *Ntuples*).

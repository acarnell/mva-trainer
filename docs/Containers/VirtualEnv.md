# Virtual environment on Lxplus

For a working virtual environment on lxplus you should perform the following steps.

## Creating the virtual environment

This you technically only need to do once. The following lines will create the virtual environment for you:

```sh
SetupATLAS
lsetup "ROOT 6.22.00-python3-x86_64-centos7-gcc8-opt"
python3 -m venv py3_venv
```
Here we chose the name `py3_venv`. Obviously you can also give your environment another name. If you do, make sure you use that name in the following steps instead of `py3_venv`. 

## Installing packages within the virtual environment

Source your virtual environment via:

```sh
source py3_venv/bin/activate
```

Your command line should have changed now and you should see `(py3_venv)` at its start.
Now we continue with the installation of packages. Do

```sh
pip3 install --upgrade pip
```
This will provide you with an up-to-date version of pip.
Continue with the following command:

```sh
pip3 install tensorflow==2.3.1 pandas uproot scikit-learn tables matplotlib pydot
```
This command will install the necessary packages that should work with the python intepreter you have previously set up.

Check if your installation was successful and if all needed packages are indeed there by sourcing the mva-trainer's setup script:

```sh
source setup.sh
```

If the scripts terminates without errors you made it. The installation was successful.
You can leave the virtual environment by typing:

```
deactivate
```

For future uses you now only need to source the virtual environment via the source command:

```sh
source py3_venv/bin/activate
```


# Settings for mva-trainer configs 



#### `GENERAL` block options
| **Option** | **Effect** |
| ------ | ------ |
| **Inputs** | |
| Job | Name of the `Job`. An output directory is automatically created and all scripts will store their output in it.|
| MCWeight | Python style weight string. Pass a string of (multiplied) `weights`: *weight_mc\*weight_pileup\*...*. |
| Selection | Python style selection string. Pass a string of (boolean) criteria: *(nJets>=2)&(nEl==2) ...* . |
| Treename | Name of ROOT `TTree` from which the data is taken in the conversion step. |
| Variables | List of variables that are read during the conversion step. Pass a comma-separated list: *pT_1jet,pT_2jet,pT_3jet,...* .|
| **Preprocessing** | |
| InputScaling | Determine procedure to scale inputs. Possible options are [minmax](https://scikit-learn.org/stable/modules/generated/sklearn.preprocessing.MinMaxScaler.html) minmax_symmetric or [standard](https://scikit-learn.org/stable/modules/generated/sklearn.preprocessing.StandardScaler.html) and `none`. If `minmax` is chosen inputs are scaled into a range between 0 and 1. If `standard` is chosen the input features are scaled by removing the mean and scaling to unit variance. If `none` is chosen no scaling is applied. Default is `minmax`. |
| RecalcScaler | Can be `True`(default) or `False`. If set to false the scaler (if present) is simply loaded and not re-calculated. This is useful if you want to run a different conversion but dont't want to change the scaling obtained from a previous configuration.|
| WeightScaling | Can be set to `True` of `False`. If set to `True` weights are scaled such that the sum over all labels is normalised.|
| TreatNegWeights | Can be set to `True` of `False`. If set to `True` negative weights are set to 0.|
| Folds | Integer number which determines how many folds (k-folding) should be performed during training.|
| **Cosmetics** | |
| ATLASLabel | Text that is printed behind the ATLAS label.|
| CMLabel | CM label for the plots. *#sqrt{s}* is automatically added in front of this string.|
| CustomLabel | Custom text that is printed below the ATLAS label.|
| PlotFormat | List of file endings to specify the file format of the produced plots. Allowed options are `pdf`, `png`, `eps`, `jpeg`, `gif` and `svg`.|
| StackPlotScale | Can be `Linear` or `Log`. Determines the Y-axis scale for stack plots. |
| SeparationPlotScale | Can be `Linear` or `Log`. Determines the Y-axis scale for (1D) separation plots. |
| ConfusionPlotScale | Can be `Linear` or `Log`. Determines the Y-axis scale for binary confusion plots. |
| KSPlotScale | Can be `Linear` or `Log`. Determines the Y-axis scale for Train-versus-Test (KS) plots. |
| LargeCorrelation | Float value above which a correlation between variables is considered large (default 0.5). Based on this value an extra correlation matrix is drawn only containing large correlations. |
| RatioMax | Float to define the maximum value in ratio plots |
| RatioMin | Float to define the minimum value in ratio plots |
| DoCtrlPlots | Boolean deciding whether conversion control plots are produced Can be `True` or `False`.|
| DoYields | Boolean deciding whether yields are drawn. Can be `True` or `False`.|
| Blinding | Option (float value) to blind data. Any datapoint beyond a given threshold (e.g. 0.5) will be blinded in the plot.|

#### `VARIABLE` block options
| **Option** | **Effect** |
| ------ | ------ |
| Name | Name of the variable. |
| Label | Label of the variable. |
| Binning | Default ROOT-style binning, i.e. 10,0,1 is 10 bins from 0 to 1 |
| CustomBinning | Custom binning, i.e. user-defined edges: e.g. 0.1,0.4,0.6,0.7,0.8,1.0 |

#### `SAMPLE` block options
| **Option** | **Effect** |
| ------ | ------ |
| Name | Name of the sample. |
| Type | Type of the sample. Can be `Signal`, `Background`, `Systematic`, or `Fake`|
| TrainLabel | Integer describing the label used during training.|
| NtupleFiles | Comma-separated list of ntuple files. Remember that the parent path is already given in the setup script.|
| TreeName| Name of the TTree to be used for this sample.|
| Selection | Sample specific Python style selection string. This selection is combined with the `GENERAL Selection`.|
| MCWeight | Sample specific Python style MCWeight string. This string is combined using `*` with the `GENERAL MCWeight`.|
| PenaltyFactor | Scalar value (float) that is multiplied to the training weight.|
| FillColor | Color value (integer) which is used to fill the corresponding Histogram in a stack plot. |
| Group | String to group together samples. These samples are now treated as "one". |
| ScaleToBkg | Can be `True` or `False`. Specifies whether the sample is scaled up/down w.r.t. the backgrounds. |

#### Model Options

| **Option** | **Effect** |
| ------ | ------ |
| **Generic options** | |
| Name | Custom name of the model. This name needs to be unique. I.e. if you plan to inject several models you need to make sure the names of the models are unambiguous.|
| Type | Type of the model. Currently supported: `Classification-DNN`, `Classification-BDT`, `Classification-Xg-BDT`, `3LZ-Reconstruction`, `4LZ-Reconstruction`, `Regression-DNN`, `Regression-BDT` and `Classification-WGAN` (WIP).|
| Loss | Loss function which is used in the training. Currently supported for `DNNMODEL`: `binary_crossentropy`, `categorical_crossentropy`, `huber_loss`, `mean_squared_error`, `mean_absolute_error`, `mean_absolute_percentage_error`, `mean_squared_logarithmic_error`, `cosine_similarity`, `huber_loss`, `log_cosh`, [`categorical_focal_loss`](https://arxiv.org/abs/1708.02002), [`binary_focal_loss`](https://arxiv.org/abs/1708.02002). For a `BDTMODEL` the following loss functions are supported: `deviance`, `exponential`.|
| Optimiser | String to defined the optimiser used during the training of a deep neural network. Allowed optimisers are: `Adadelta`, `Adagrad`, `Adam`, `Adamax`, `Ftrl, `Nadam`(Default), `RMSprop` and `SGD`.|
| HuberDelta | Float value representing the delta parameter in the huber loss function. Only works if `huber_loss` is chosen as loss function.|
| ValidationSize | Relative size of the validation set used during training. Default if 0.2.|
| Verbosity | Can be 0, 1 or 2. Defines the verbosity level of the training output. 0 represents the most silent option.|
| ClassLabels | Custom classifier labels. Pass a comma-separated list of labels for multi-class classification.|
| ModelBinning | Custom binning using a fixed bin with. Pass a comma-separated list of values following the formar `nbins`, `x_low`, `x_high`. Default is 20,0,1. This binning will be used for all appropriate plots. In case of a multi-class classifier simply append different binnings (e.g. 20,0,1,10,0,1,5,0,1 for fixed binning with 20,10 and 5 bins respectively).|
| BinningOptimisation | Can be `TransfoD_symmetric`, `TransfoD_flatS` or `TransfoD_flatB`. Applies TransfoD binning optimisation using the specified number of bins in `ModelBinning` and attempts to optimise the binning.
| **Architecture related options** | |
| Nodes | Comma-separated list of neurons for each layer.|
| MaxNodes | Integer defining the maximum number of nodes in any layer of a DNN for hyperparameter optimisation. Default 100.|
| MinNodes | Integer defining the minimum number of nodes in any layer of a DNN for hyperparameter optimisation. Default 10.|
| StepNodes | Integer defining the stepsize between `MaxNodes` and `MinNodes` of a DNN for hyperparameter optimisation. Default 10.|
| MaxLayers | Integer defining the maximum number of layers of a DNN for hyperparameter optimisation. Default 5.|
| MinLayers | Integer defining the minimum number of layers of a DNN for hyperparameter optimisation. Default 1.|
| nEstimators | Number of boosting stages to be performed during the training of a BDT.|
| MaxDepth | Maximum Depth of the individual estimators of a BDT.|
| Criterion | Function to measure the quality of a split in a BDT.| 
| DropoutIndece | Layer indece at which [Dropout](https://keras.io/api/layers/regularization_layers/dropout/) layers are added. Only supported in a DNN.|
| DropoutProb | Probability of dropout. Default is 0.1.|
| BatchNormIndece | Layer indece at which [BatchNormalisation](https://keras.io/api/layers/normalization_layers/batch_normalization/) layers are added. Only supported in a DNN.|
| OutputSize | Number of neurons in the output layer of a DNN.|
| KernelInitialiser | [KernelInitialiser](https://keras.io/api/layers/initializers/) in the hidden layers. Pass a string defining an initializers to define the way to set the initial random weights of Keras layers. Used in a DNN.|
| KernelRegulariser | [KernelRegulariser](https://keras.io/api/layers/regularizers/) in the hidden layers. Regulariser to apply a penalty on the hidden layer's kernel. Allowed options are `l1`, `l2` and `l1_l2`. UUsed in a DNN.|
| BiasRegulariser | [BiasRegulariser](https://keras.io/api/layers/regularizers/) in the hidden layers. Regulariser to apply a penalty on the hidden layer's bias. Allowed options are `l1`, `l2` and `l1_l2`. Used in a DNN.
| ActivitylRegulariser | [ActivityRegulariser](https://keras.io/api/layers/regularizers/) in the hidden layers. Regulariser to apply a penalty on the hidden layer's output. Allowed options are `l1`, `l2` and `l1_l2`. Used in a DNN.|
| SetActivationFunctions | List of activation functions from which possible activation functions for the layers for hyperparameter optimisation are randomly drawn. The default only contains `ReLU`.|
| ActivationFunctions | [Activation function](https://keras.io/api/layers/activations/) in the hidden layers. Pass a list of activation functions with a length that corresponds to the number of hidden layers defined in 'Nodes'. Used in a DNN.|
| OutputActivation | [Activation function](https://keras.io/api/layers/activations/) in the output layer of a DNN.|
| **Learning related options** | |
| Epochs | Number of training epochs for a DNN. Default is 100.|
| LearningRate | Initial learning rate for the training of a DNN or BDT. Default is 0.001.|
| BatchSize | Batch size used in training. Default is 32.|
| Patience | Number of epochs with no improvement after which training will be stopped.|
| MinDelta | Minimum change in the monitored quantity to qualify as an improvement.|
| Metrics | Comma-separated list of metrics to be evaluated during training. Supported for a DNNM`|
| SmoothingAlpha | Float between 0 and 0.5 to be applied to smooth labels according using Y=Y(1-alpha)+alpha/K see [arXiv:1512.00567](https://arxiv.org/abs/1512.00567) for an introduction to label smoothing.|
| UseTensorBoard | Can be True or False. If set to True [TensorBoard](https://www.tensorflow.org/tensorboard) log files are automatically written to the model directory. These can then be studied using TensorBoard. Used in `DNNMODEL`.|
| RegressionTarget | Name of the variable in your Ntuples to use as the target for regression. The variable is read from the truth tree by default.|
| RegressionTargetLabel | Custom label for the regression target variable to be used in the plots.|
| RegressionScale | Custom scale to be applied to the regression target variable on an event by event basis.|
| ReweightTargetDist | Option for a regression model. Can be True (default) or False. When set to True the target distribution is binned using 'ModelBinning'. Afterwards weights are calculated to flatten the distribution and hence provide a flat training distribution of the target variable.|

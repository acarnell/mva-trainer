FROM rootproject/root:latest
RUN useradd --create-home --shell /bin/bash mva_trainer_user
WORKDIR /home/mva_trainer_user
RUN apt update && \
    apt install -y python3-pip && \
    apt install -y graphviz && \
    apt install -y git && \
    apt install -y emacs && \
    apt install -y vim && \
    apt install -y nano
RUN pip3 install --no-cache-dir --upgrade numpy==1.18.5 && \
    pip3 install --no-cache-dir --upgrade uproot && \
    pip3 install --no-cache-dir --upgrade pandas && \
    pip3 install --no-cache-dir --upgrade scikit-learn && \
    pip3 install --no-cache-dir --upgrade tensorflow==2.3.1 && \
    pip3 install --no-cache-dir --upgrade tables && \
    pip3 install --no-cache-dir --upgrade matplotlib && \
    pip3 install --no-cache-dir --upgrade pydot
USER mva_trainer_user
COPY . .
CMD ["bash"]


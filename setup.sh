#!/bin/sh

# Some messages to the user
echo "************************************************************************"
echo -e "SETUP\t This is the MVA-Trainer setup script"
echo -e "SETUP\t "
echo -e "SETUP\t You can find more information at https://gitlab.cern.ch/skorn/mva-trainer"
echo ""

echo -e "SETUP\t Setting up environmental variables"

# Determine where this script is located (this is surprisingly difficult).
if [ "${BASH_SOURCE[0]}" != "" ]; then
    # This should work in bash.
    _src=${BASH_SOURCE[0]}
elif [ "${ZSH_NAME}" != "" ]; then
    # And this in zsh.
    _src=${(%):-%x}
elif [ "${1}" != "" ]; then
    # If none of the above works, we take it from the command line.
    _src="${1/setup.sh/}/setup.sh"
else
    echo -e "SETUP\t Failed to determine the MVA-Trainer base directory. Aborting ..."
    echo -e "SETUP\t Can you give the source script location as additional argument? E.g. with"
    echo -e "SETUP\t . ../foo/bar/scripts/setup.sh ../foo/bar/scripts"
    return 1
fi

# Set up MVA-Trainer environment variables that are picked up from the code.
export MVA_TRAINER_BASE_DIR="$(cd -P "$(dirname "${_src}")" && pwd)"

export NTUPLE_VERSION="SSML"
export NTUPLE_LOCATION="/sps/atlas/a/acarnell/libraries/"

# Disable informational tensorflow output
export TF_CPP_MIN_LOG_LEVEL="2"

chmod +x $MVA_TRAINER_BASE_DIR/scripts/Trainer.py
chmod +x $MVA_TRAINER_BASE_DIR/scripts/Evaluate.py
chmod +x $MVA_TRAINER_BASE_DIR/scripts/Converter.py
chmod +x $MVA_TRAINER_BASE_DIR/scripts/MVAInjector.py
chmod +x $MVA_TRAINER_BASE_DIR/scripts/Optimise.py
chmod +x $MVA_TRAINER_BASE_DIR/scripts/SummariseOptimisation.py


# Determine whether Python3 is installed
echo -e "SETUP\t Checking Python3 Version..."
if ! hash python3; then
    echo -e '\e[31mERROR\t Python3 is not installed or can not be found.\e[0m'
    return 1
fi

# Ok, now that we now that we have Python3 let's check whether we also have the minimal version required (3.8)
ver=$(python3 -V 2>&1 | sed 's/.* \([0-9]\).\([0-9]\).*/\1\2/')
if [ "$ver" -lt "36" ]; then
    echo -e "\e[31mERROR\t Python3 3.8 or greater required!\e[0m"
    return 1
fi

# Let's make sure we print the version...
pyv="$(python3 -V 2>&1)"
echo -e "SETUP\t Detected Python3 version is $pyv"

# Determine whether requiered packages are installed. Cumbersome but necessary...
echo ""
echo -e "SETUP\t Checking python packages..."
# Checking pandas
echo -e "SETUP\t Searching pandas..."
if python3 -c 'import pkgutil; exit(not pkgutil.find_loader("pandas"))'; then
    echo -e 'SETUP\t pandas found'
else
    echo -e '\e[31mERROR\t pandas not found\e[0m'
    return 1 
fi
# Checking uproot
echo -e "SETUP\t Searching uproot..."
if python3 -c 'import pkgutil; exit(not pkgutil.find_loader("uproot"))'; then
    echo -e 'SETUP\t uproot found'
else
    echo -e '\e[31mERROR\t uproot not found\e[0m'
    return 1
fi

# Checking scikit-learn
echo -e "SETUP\t Searching scikit-learn..."
if python3 -c 'import pkgutil; exit(not pkgutil.find_loader("sklearn"))'; then
    echo -e 'SETUP\t scikit-learn found'
else
    echo -e '\e[31mERROR\t scikit-learn not found\e[0m'
    return 1 
fi
# Checking xgboost
echo -e "SETUP\t Searching xgboost..."
if python3 -c 'import pkgutil; exit(not pkgutil.find_loader("xgboost"))'; then
    echo -e 'SETUP\t xgboost found'
else
    echo -e '\e[31mERROR\t xgboost not found\e[0m'
    return 1 
fi
# Checking tensorflow
echo -e "SETUP\t Searching tensorflow..."
if python3 -c 'import pkgutil; exit(not pkgutil.find_loader("tensorflow"))'; then
    echo -e 'SETUP\t tensorflow found'
else
    echo -e '\e[31mERROR\t tensorflow not found\e[0m'
    return 1 
fi
# Checking pytables
echo -e "SETUP\t Searching tables..."
if python3 -c 'import pkgutil; exit(not pkgutil.find_loader("tables"))'; then
    echo -e 'SETUP\t tables found'
else
    echo -e '\e[31mERROR\t tables not found\e[0m'
    return 1 
fi
# Checking matplotlib
echo -e "SETUP\t Searching matplotlib..."
if python3 -c 'import pkgutil; exit(not pkgutil.find_loader("matplotlib"))'; then
    echo -e 'SETUP\t matplotlib found'
else
    echo -e '\e[31mERROR\t matplotlib not found\e[0m'
    return 1 
fi
# Checking pydot
echo -e "SETUP\t Searching pydot..."
if python3 -c 'import pkgutil; exit(not pkgutil.find_loader("pydot"))'; then
    echo -e 'SETUP\t pydot found'
else
    echo -e '\e[31mERROR\t pydot not found\e[0m'
    return 1 
fi

# Disable informational tensorflow output
export TF_CPP_MIN_LOG_LEVEL="2"

echo "************************************************************************"
echo -e "SETUP\t Configuration finished!"
echo -e "SETUP\t "
